#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/ns3.17-scratch-simulator-debug', 'build/scratch/cacLteWlan/ns3.17-cacLteWlan-debug', 'build/scratch/ns3.17-bateria-eventos-teste-debug', 'build/scratch/subdir/ns3.17-subdir-debug', 'build/scratch/bruno2/ns3.17-bruno2-debug', 'build/scratch/ns3.17-teste-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

