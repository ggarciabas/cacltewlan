/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:   EnbNode.h
 * Author: Giovanna Garcia
 * 
 *      Evolved NodeB. (3GPP TS 36.300 V11.0.0)
 *      E-UTRAN Node B, also known as Evolved Node B, is the element in E-UTRA of LTE that is the
 *  evolution of the element Node B in UTRA of UMTS.
 *      OFDMA to downlink --- (UMTS - WCDMA to downlink)
 *      SC-FDMA to uplink --- (UMTS - TD_SCDMA to uplink)
 *      eNB embeds its own control functionality, rather tha using an RNC (Radio Network Controller) as does a Node B (UMTS).
 *
 *      Protocol:
 *              S1-AP protocol on the S1-MME interface with the MME for control plane traffic.
 *              GTP-U protocol on the S1-U interface with the S-GW for user plane traffic.
 *                      S1-AP and S1-U represents the interface from eNB to the EPC.
 *              X2-AP protocol on the X2 interface with other eNB elements.
 * 
 * LINK: http://www.quintillion.co.jp/3GPP/Specs/36300-b40.pdf
 * Created on May 26, 2013, 12:45 PM
 */

#ifndef EVOLVEDNODEB_H
#define	EVOLVEDNODEB_H

#include "ns3/node.h"
#include "../pk_ue/UserEquipmentNode.h"
#include <vector>

using namespace ns3;
using namespace std;

class EvolvedNodeBNode : public Node {
public:
    EvolvedNodeBNode();
    long double getMaxBandwidthDl ();
    long double getMaxBandwidthUl ();
    long double setMaxBandwidthDl (long double);
    long double setMaxBandwidthUl (long double);
    int getNUeVector ();
    UserEquipmentNode getUeVector (int);
    
private:
    // Maximum bandwidth to downlink bits
    long double m_maxBandwidthDl;
    // Maximum bandwidth to uplink bits
    long double m_maxBandwidthUl;
    // User Equipments connected 
    vector<UserEquipmentNode> ueVector;
};

#endif	/* EVOLVEDNODEB_H */

