/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:  EvolvedNodeBNode.cc
 * Author: Giovanna Garcia
 * 
 * Created on May 26, 2013, 3:34 PM
 */

#include "../pk_enb/EvolvedNodeBNode.h"

EvolvedNodeBNode::EvolvedNodeBNode() {
}

long double EvolvedNodeBNode::setMaxBandwidthDl(long double t_maxBandwidthDl) {
    this->m_maxBandwidthDl = t_maxBandwidthDl;
}

long double EvolvedNodeBNode::setMaxBandwidthUl(long double t_maxBandwidthUl) {
    this->m_maxBandwidthUl = t_maxBandwidthUl;
}

long double EvolvedNodeBNode::getMaxBandwidthDl() {
    return this->m_maxBandwidthDl;
}

long double EvolvedNodeBNode::getMaxBandwidthUl() {
    return this->m_maxBandwidthUl;
}

int EvolvedNodeBNode::getNUeVector() {
    return this->ueVector.size();
}

UserEquipmentNode EvolvedNodeBNode::getUeVector(int pos) {
    return this->ueVector.at(pos);
}