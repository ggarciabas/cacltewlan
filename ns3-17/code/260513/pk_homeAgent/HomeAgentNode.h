/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:   HomeAgentNode.h
 * Author: Giovanna Garcia
 *
 * Created on May 26, 2013, 3:23 PM
 */

#ifndef HOMEAGENTNODE_H
#define	HOMEAGENTNODE_H

#include "ns3/node.h"

using namespace ns3;

/*
 *    This class HA will control the CAC of the LTE and WLAN,
 *  to do this, it will check what kind of application the UE
 *  wants to send and how much resource this app need, will
 *  ask to the enb if this UE can use the LTE to send, if not
 *  will verify if is possible to send by WLAN, otherwise it
 *  will reject the calling of the UE.
 *
 */
class HomeAgentNode : public Node {
public:
    HomeAgentNode();
    
private:

};

#endif	/* HOMEAGENTNODE_H */

