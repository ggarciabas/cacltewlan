/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:  AccessPointNode.h
 * Author: Giovanna Garcia
 * 
 * Created on May 26, 2013, 3:34 PM
 */

#ifndef ACCESSPOINTNODE_H
#define	ACCESSPOINTNODE_H

#include "ns3/node.h"
#include "../pk_ue/UserEquipmentNode.cc"
#include <vector>

using namespace ns3;
using namespace std;

class AccessPointNode : public Node {
public:
    AccessPointNode(long double, long double);
    long double getMaxBandwidthDl ();
    long double getMaxBandwidthUl ();
    int getNUeVector ();
    UserEquipmentNode getUeVector (int);
    
private:
    // Maximum bandwidth to downlink - bits
    long double m_maxBandwidthDl;
    // Maximum bandwidth to uplink - bits
    long double m_maxBandwidthUl;
    // User Equipments connected 
    vector<UserEquipmentNode> ueVector;
};


/*
 *    Now we will no use the Voip and Video to the Ap, but
 *  hereafter we will need because if is not possible to connect the Ue with a VOIP application
 *  in the enb we will try to connect with WLAN if possible.
 *
 */


#endif	/* ACCESSPOINTNODE_H */

