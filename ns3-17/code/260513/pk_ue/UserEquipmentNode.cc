/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:   UserEquipmentNode.cc
 * Author: Giovanna Garcia
 * 
 * Created on May 26, 2013, 2:07 PM
 */

#include "UserEquipmentNode.h"

UserEquipmentNode::UserEquipmentNode() {
    this->m_voipApp = false;
}

int UserEquipmentNode::getPosAppVoip() {
    return this->m_posAppVoip;
}

void UserEquipmentNode::setPosAppVoip(int t_posAppVoip) {
    this->m_posAppVoip = t_posAppVoip;
}

bool UserEquipmentNode::getVoipApp () {
    return this->m_voipApp;
}

void UserEquipmentNode::setVoipApp(bool t_voipApp) {
    this->m_voipApp = t_voipApp;
}

//Ipv4Address UserEquipmentNode::getIpv4AddressLte() {
//    
//}
//
//Ipv4Address UserEquipmentNode::getIpv4AddressWlan() {
//    
//}