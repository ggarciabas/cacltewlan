/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * /author Giovanna Garcia
 */ 
#include <vector>
#include "applicationSending.h"

// This structure describe the Ap connected to the HA
typedef struct accessPoint {
	// The pointer to the AP connected
	ns3::Ptr<ns3::Node> m_accessPoint;
	// Maximum bandwidth to downlink - bits
	long double m_maxBandwidthDl;
	// Maximum bandwidth to uplink - bits
	long double m_maxBandwidthUl;
        // Applications Dl occupying the WLAN
        std::vector<APPSENDING> appWlanOnlineDl; 
        // Applications Ul occupying the WLAN 
        std::vector<APPSENDING> appWlanOnlineUl; 
//        // Sum of the size package of the app
//        double sizeAppWanOnlineUl;
//        // Sum of the size package of the app
//        double sizeAppWanOnlineDl;
	// To calculate the available bandwidth we are using
	//		m_maxBandwidthDl - ( (m_amountVoipDl * 25165) + ( (m_amountVideoDl + m_amountWwwDl + m_amountFtpDl) * 134217.728 ) )
	//      m_maxBandwidthUl - ( (m_amountVoipUl * 25165) + ( (m_amountVideoUl + m_amountWwwUl + m_amountFtpUl) * 134217.728 ) )
} ACCESSPOINT;


/*
 *    Now we will no use the Voip and Video to the Ap, but
 *  hereafter we will need because if is not possible to connect the Ue with a VOIP application
 *  in the enb we will try to connect with WLAN if possible.
 *
 */
