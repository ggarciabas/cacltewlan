/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  /file Simulation LTE and WLAN
 * 	/brief
 * 	/author Giovanna Garcia
 *
 * 		The overall LTE architecture are described in TS 36.401 3GPP:
 * 			LINK http://www.3gpp.org/ftp/Specs/html-info/36401.htm
 *
 * 	OBS.:
 * 		- Fazer testes com os modelos 802.11 A e G. ( )
 */

////////////////////////////////////////////
//
//  Headers declarations
// 	- System c++ and c
// 	- Ns3 headers
//
///////////////////////////////////////////
#include <iostream>
#include <vector>
#include <algorithm>
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/config-store.h"
#include "ns3/epc-helper.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ipv4.h"
#include "ns3/network-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "enum/enum.h"
#include "ns3/trace-helper.h"
#include <cstdlib> // For functions rand and srand
#include "enbNode.h"
#include "accessPoint.h"
#include "ue.h"
#include "applicationSending.h"

/*
 *    This class HA will control the CAC of the LTE and WLAN,
 *  to do this, it will check what kind of application the UE
 *  wants to send and how much resource this app need, will
 *  ask to the enb if this UE can use the LTE to send, if not
 *  will verify if is possible to send by WLAN, otherwise it
 *  will reject the calling of the UE.
 *
 */
class HomeAgent {
public:
    // Constructor
    HomeAgent();
    // Number of application in the simulation
    void setNumberApps(int);
    // Number of applications per hour
    void setAppPerHour(int);
    // Configure the enb to the HA
    void insertEnb(ns3::Ptr<ns3::Node>);
    // Configure the AP to the HA
    void insertAccessPoint(ns3::Ptr<ns3::Node>);
    // Configure the UE to the HA - int parameter tells if the UE have a Wlan 802.11 a or g ( 0, 1)
    void insertUserEquipment(ns3::Ptr<ns3::Node>, int);
    // Set up the server HA
    void insertHa(ns3::Ptr<ns3::Node>);
    // function to order the vector of app
    bool funSortTime(APPSENDING, APPSENDING);

private:
    // Vector of all the Application created to the UE
    std::vector<APPSENDING> m_trafficApp;
    // Amount of apps per hour
    int m_appHour;
    // Counter to the application generated
    int m_counter;
    // Number of application generated to the simulation indicated by the user
    int m_numApp;
    // Vector of all the eNBs connected to the HA
    std::vector<ENB> m_enb;
    // Vector of all the Ap connected to the HA
    std::vector<ACCESSPOINT> m_accessPoint;
    // Number of accepted calls
    int m_accepted;
    // Number of denied calls
    int m_denied;
    // Vector of all UEs connected to the HA
    std::vector<UE> m_ue;
    // All the applications to the simulator execute it
    ns3::ApplicationContainer m_applications;
    // The server HA
    ns3::Ptr<ns3::Node> m_serverHa;

    // Private methods
    // Will create the random traffic app (vector)
    void createTrafficApp();
    // Will configure the UE
    void configureUe();
    // The CAC method - this method will manager all the connections
    int callAdmissionControll(App, int, int);
    // Verifica as chamadas anteriores e retorna o valor do montante, de acordo, com o tipo de aplicacao
    double verificarChamadasAnteriores(int, int, int);
};

HomeAgent::HomeAgent() {
    m_counter = 0;
    m_numApp = 0;
    m_appHour = 0;
    m_accepted = 0;
    m_denied = 0;
}

void HomeAgent::insertHa(ns3::Ptr<ns3::Node> ha) {
    m_serverHa = ha;
}

// public method - just create the vector  with the specified size and calls the method to random the applications

void HomeAgent::setNumberApps(int number) {
    m_numApp = number;
    createTrafficApp();
}

// private method - will randomize the values to the vector

void HomeAgent::createTrafficApp() {
    int random;
    uint16_t c;
    for (c = 0; c < m_numApp; ++c, ++m_counter) {
        // srand(time(NULL));
        random = rand() % (3 + 1); // generates the random number between 0 and 3, this numbers represents type of the App
        /*
         *  m_counter - returns the number of the application
         *  m_appHour - returns the number of apps per hour
         */
        APPSENDING nowApp; // application

        nowApp.m_timeStart = ns3::Time(m_counter * (3600 / (double) m_appHour)); // ns3::Time (ns3::Simulator::Now() + (m_counter * (3600 / m_appHour)));
        nowApp.m_timeStop = ns3::Time(nowApp.m_timeStart + 120);
        switch (random) {
            case 0:
            {
                //std::cout << "Generated the number 0 = VOIP" << std::endl;
                nowApp.m_app = VOIP;
                nowApp.m_packetSize = 50; // bits ?
                nowApp.m_dataRate = "0.024Mbps";
                nowApp.m_socketFactory = "ns3::UdpSocketFactory";
                nowApp.m_onTime = "ns3::ConstantRandomVariable[Constant=120]";
                nowApp.m_offTime = "ns3::ConstantRandomVariable[Constant=0]";
                nowApp.m_port = 5060;
            }
                break;
            case 1:
            {
                //std::cout << "Generated the number 1 = VIDEO" << std::endl;
                nowApp.m_app = VIDEO;
                nowApp.m_packetSize = 429; // bits ?
                nowApp.m_dataRate = "0.128Mbps";
                nowApp.m_socketFactory = "ns3::UdpSocketFactory";
                nowApp.m_onTime = "ns3::ConstantRandomVariable[Constant=120]";
                nowApp.m_offTime = "ns3::ConstantRandomVariable[Constant=0]";
                nowApp.m_port = 5060;
            }
                break;
            case 2:
            {
                //std::cout << "Generated the number 2 = WWW" << std::endl;
                nowApp.m_app = WWW;
                nowApp.m_packetSize = 429; // bits ?
                nowApp.m_dataRate = "0.128Mbps";
                nowApp.m_socketFactory = "ns3::TcpSocketFactory";
                nowApp.m_onTime = "ns3::ConstantRandomVariable[Constant=120]";
                nowApp.m_offTime = "ns3::ConstantRandomVariable[Constant=0.04]";
                nowApp.m_port = 8080;
            }
                break;
            case 3:
            {
                //std::cout << "Generated the number 3 = FTP" << std::endl;
                nowApp.m_app = FTP;
                nowApp.m_packetSize = 429; // bits ?
                nowApp.m_dataRate = "0.128Mbps";
                nowApp.m_socketFactory = "ns3::TcpSocketFactory";
                nowApp.m_onTime = "ns3::ConstantRandomVariable[Constant=120]";
                nowApp.m_offTime = "ns3::ConstantRandomVariable[Constant=0]";
                nowApp.m_port = 2121;
            }
                break;
        }

        m_trafficApp.push_back(nowApp);
        //        std::cout << "Application  (" << c << ")  " << nowApp.m_app
        //                << "  starttime=" << nowApp.m_timeStart
        //                << "  stopTime=" << nowApp.m_timeStop << std::endl;
    }
    configureUe(); // calls the configure UE to define which UE will receive or sent the package
}

// set the value to the app per hour

void HomeAgent::setAppPerHour(int appPerHour) {
    this->m_appHour = appPerHour;
}

// private method - configure the ue nodes to send the apps

void HomeAgent::configureUe() {
    std::cout << "Configuring the UEs" << std::endl;
    int randomUe, trafficUlDl, acceptedDenied = 1;
    // Will choose randomly the UE of the vector
    uint16_t i;
    for (i = 0; i < m_trafficApp.size(); ++i, acceptedDenied = 1) {
        do {
            //srand(time(NULL));
            randomUe = rand() % (m_ue.size()); // generates the random number between 0 and the max number of the UEs
            if (m_ue.at(randomUe).m_voipApp) {// If the Ue are sending a VOIP app
                // If the stop time was less than the start time of the next app
                if (m_trafficApp.at(m_ue.at(randomUe).m_posAppVoip).m_timeStop < m_trafficApp.at(i).m_timeStart)
                    m_ue.at(randomUe).m_voipApp = false; // The flag voipApp is set false.
            }
        } while (m_trafficApp.at(i).m_app == VOIP && m_ue.at(randomUe).m_voipApp); // For each app that has the same configuration, then the simulation will ask to another.

        std::cout << "\n\n-----------------------------------------------------------------------------------" << std::endl;
        std::cout << "#######   Ue selected (" << randomUe << ") -- " << m_trafficApp.at(i).m_app << std::endl;

        trafficUlDl = rand() % (2); // Uplink = 0 and downlink = 1
        m_trafficApp.at(i).m_ulDl = trafficUlDl; // If the app is Ul or Dl

        // Create the PacketSink, the tunel to the comunication with onoff
        /*
         *    Receive and consume traffic generated to an IP address and port.
         *    This application was written to complement OnOffApplication, but it is more general 
         * so a PacketSink name was selected. Functionally it is important to use in multicast 
         * situations, so that reception of the layer-2 multicast frames of interest are enabled, 
         * but it is also useful for unicast as an example of how you can write something simple to 
         * receive packets at the application layer. Also, if an IP stack generates ICMP Port 
         * Unreachable errors, receiving applications will be needed.
         *  The constructor specifies the Address (IP address and port) and the transport protocol 
         * to use. A virtual Receive () method is installed as a callback on the receiving socket. 
         * By default, when logging is enabled, it prints out the size of packets and their address, 
         * but we intend to also add a tracing source to Receive() at a later date.
         */
        ns3::PacketSinkHelper sinkHelper(m_trafficApp.at(i).m_socketFactory,
                ns3::InetSocketAddress(ns3::Ipv4Address::GetAny(),
                m_trafficApp.at(i).m_port));
        ns3::Ptr<ns3::Node> sender;

        // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html       ;
        ns3::OnOffHelper onoff(m_trafficApp.at(i).m_socketFactory,
                ns3::Address()); // Address() - creates an invalid address.
        std::cout << "On time: " << m_trafficApp.at(i).m_onTime;
        onoff.SetAttribute("OnTime",
                ns3::StringValue(m_trafficApp.at(i).m_onTime)); // On time
        onoff.SetAttribute("OffTime",
                ns3::StringValue(m_trafficApp.at(i).m_offTime)); // Off time
        // P.S.: offTime + DataRate/PacketSize = next packet time
        onoff.SetAttribute("DataRate",
                ns3::DataRateValue(
                ns3::DataRate(m_trafficApp.at(i).m_dataRate))); // Data Rate
        onoff.SetAttribute("PacketSize",
                ns3::UintegerValue(m_trafficApp.at(i).m_packetSize)); // Packet Size

        ns3::Ipv4Address receiverIpAddress; // Ipv4 Address of the receiver

        switch (callAdmissionControll(m_trafficApp.at(i).m_app, trafficUlDl, i)) {
            case 0:
            {
                acceptedDenied = 0; // just a flag to control if the connection was accepted or denied
            }
                break;
            case 1: // send the packet by LTE
            {
                /*
                 * If downlink traffic then the Ue will receive the call
                 * otherwise, the HA will receive the call
                 */
                if (trafficUlDl) { // UE
                    m_applications = sinkHelper.Install(m_ue.at(randomUe).m_ue); // who will receive the packet
                    receiverIpAddress = m_ue.at(randomUe).m_lteAddress;
                    sender = m_serverHa;
                } else { // HA
                    m_applications = sinkHelper.Install(m_serverHa); // who will receive the packet
                    receiverIpAddress =
                            m_serverHa->GetObject<ns3::Ipv4>()->GetAddress(1, 0).GetLocal(); // Lte
                    sender = m_ue.at(randomUe).m_ue;
                }
            }
                break;
            case 2: // 802.11 A
            {
                if (trafficUlDl) { // UE
                    m_applications = sinkHelper.Install(m_ue.at(randomUe).m_ue); // who will receive the packet
                    receiverIpAddress = m_ue.at(randomUe).m_wlanAddress;
                    sender = m_serverHa;
                } else { // HA
                    m_applications = sinkHelper.Install(m_serverHa); // who will receive the packet
                    receiverIpAddress =
                            m_serverHa->GetObject<ns3::Ipv4>()->GetAddress(2, 0).GetLocal(); // Wlan 802.11 A
                    sender = m_ue.at(randomUe).m_ue;
                }
            }
                break;
        }

        m_trafficApp.at(i).m_acceptedNot = false;
        if (acceptedDenied) { // if accepted the calling
            std::cout << "\naceptedDenied = true";
            m_trafficApp.at(i).m_acceptedNot = true;
            ns3::AddressValue receiverAddress(
                    ns3::InetSocketAddress(receiverIpAddress,
                    m_trafficApp.at(i).m_port));
            onoff.SetAttribute("Remote", receiverAddress);
            m_applications.Add(onoff.Install(sender));
        } else
            std::cout << "\naceptedDenied = false" << std::endl;
    }
}

// This class will create a struct with some configurations of each enb

void HomeAgent::insertEnb(ns3::Ptr<ns3::Node> enb) {
    std::cout << "Iniciando configuracao" << std::endl;
    ENB nowEnb;

    nowEnb.m_enb = enb;
    nowEnb.m_maxBandwidthDl = 52428800; // 50 Mbps
    std::cout << " maxDl = " << nowEnb.m_maxBandwidthDl;
    nowEnb.m_maxBandwidthUl = 52428800; // 50 Mbps
    std::cout << " maxUl = " << nowEnb.m_maxBandwidthUl << std::endl;
    // m_enbs->at(m_enbs->size()-1).m_maxBandwidthDl = enb->GetDevice(1)->Get; pegar o valor definido como UL e DL
    // http://www.nsnam.org/doxygen/lte-enb-net-device_8cc_source.html
    m_enb.push_back(nowEnb);
}

// This method will create a struct with some configurations of each accessPoint

void HomeAgent::insertAccessPoint(ns3::Ptr<ns3::Node> accessPoint) {
    ACCESSPOINT newAp;

    newAp.m_accessPoint = accessPoint;
    // Max
    newAp.m_maxBandwidthDl = 26214400; // 25 Mbps ?
    std::cout << " maxDl = " << newAp.m_maxBandwidthDl;
    newAp.m_maxBandwidthUl = 26214400; // 25 Mbps ?
    std::cout << " maxUl = " << newAp.m_maxBandwidthUl << std::endl;
    m_accessPoint.push_back(newAp);
}

// This method will create a struct with some configurations of each ue node
// _80211 - is to differentiate the 802.11 A e G respectively 0 and 1

void HomeAgent::insertUserEquipment(ns3::Ptr<ns3::Node> ue, int _80211) {
    UE newUe;

    newUe.m_ue = ue;
    newUe.m_voipApp = false;
    newUe.m_wlanAddress =
            ue->GetObject<ns3::Ipv4>()->GetAddress(1, 0).GetLocal();
    newUe.m_lteAddress =
            ue->GetObject<ns3::Ipv4>()->GetAddress(2, 0).GetLocal();
    newUe.m_80211 = _80211;

    m_ue.push_back(newUe);
}

// This method will check if the application can be sent by interface specified
// if not, this method will check if is possible to send the node by other interface
// will return then the number of the interface. 1 = LTE and 2 = WLAN

int HomeAgent::callAdmissionControll(App application, int dlUl, int posicao) {
    switch (application) {
        case VOIP:
        {
            std::cout << "\n\n- Solicitando = 25165.824 bits";
            if (dlUl) { // 1 = downlink  
                std::cout << " DL ";
                if (verificarChamadasAnteriores(1, 1, posicao) >= 25165.824) { // 1 = LTE -- 1 = DL
                    ++m_accepted;
                    return 1; // 1 = LTE
                } else {
                    // Here we will ask to the AP if is possible to send the VOIP (DL)
                }
            } else { // 0 = uplink
                std::cout << " UL ";
                if (verificarChamadasAnteriores(1, 0, posicao) >= 25165.824) { // 1 = LTE -- 0 = DL
                    ++m_accepted;
                    return 1; // 1 = LTE
                } else {
                    // Here we will ask to the AP if is possible to send the VOIP (UL)
                }
            }
        }
            break;
        case VIDEO:
        {
            std::cout << "\n\n- Solicitando = 134217.728 bits";
            if (dlUl) { // 1 = downlink
                std::cout << " DL ";
                if (verificarChamadasAnteriores(1, 1, posicao) >= 134217.728) { // 1 = LTE -- 1 = DL
                    ++m_accepted;
                    return 1; // 1 = LTE
                } else {
                    // Here we will ask to the AP if is possible to send the VIDEO
                }
            } else { // 0 = uplink
                std::cout << " UL ";
                if (verificarChamadasAnteriores(1, 0, posicao) >= 134217.728) { // 1 = LTE -- 0 = DL
                    ++m_accepted;
                    return 1; // 1 = LTE
                } else {
                    // Here we will ask to the AP if is possible to send the VIDEO
                }
            }
        }
            break;
        case WWW:
        {
            std::cout << "\n\n- Solicitando = 134217.728 bits";
            if (dlUl) { // 1 = downlink
                std::cout << " DL ";
                if (verificarChamadasAnteriores(0, 1, posicao) >= 134217.728) { // 0 = WLAN -- 1 = DL
                    ++m_accepted;
                    return 2; // 2 = 802.11A
                } else {
                    // Here we will ask to the eNB if is possible to send the WWW
                }
            } else { // 0 = uplink
                std::cout << " UL ";
                if (verificarChamadasAnteriores(0, 0, posicao) >= 134217.728) { // 0 = WLAN -- 0 = DL
                    ++m_accepted;
                    return 2; // 2 = 802.11A
                } else {
                    // Here we will ask to the eNB if is possible to send the WWW
                }
            }
        }
            break;
        case FTP:
        {
            std::cout << "\n\n- Solicitando = 134217.728 bits";
            if (dlUl) { // 1 = downlink
                std::cout << " DL ";
                if (verificarChamadasAnteriores(0, 1, posicao) >= 134217.728) { // 0 = WLAN -- 1 = DL
                    ++m_accepted;
                    return 2; // 2 = 802.11A
                } else {
                    // Here we will ask to the eNB if is possible to send the FTP
                }
            } else { // 0 = uplink
                std::cout << " UL ";
                if (verificarChamadasAnteriores(0, 0, posicao) >= 134217.728) { // 0 = WLAN -- 0 = DL
                    ++m_accepted;
                    return 2; // 2 = 802.11A
                } else {
                    // Here we will ask to the eNB if is possible to send the FTP
                }
            }
        }
            break;
    }
    std::cout << "\n\n --------> Nao entrei no Switch" << std::endl;
    ++m_denied;
    return 0; // Denied
}

/*
 *   Verifica o montante utilizado por cada aplicacao e retorna esse valor para que seja verificado a diponibilidade
 * de recursos.
 *   LteWlan - informa se e Lte = 1 ou WLAN = 0
 *   sentido - Ul = 0    Dlç = 1
 *   posicao - qual aplicacao do vetor esta solicitando recuso
 */
double HomeAgent::verificarChamadasAnteriores(int LteWlan, int sentido, int posicao) {
    long double total = 0;
    long double lte = 0;
    long double wlan = 0; // Rth 
    if (sentido) { // Download
        lte = m_enb.at(0).m_maxBandwidthDl;
        wlan = m_accessPoint.at(0).m_maxBandwidthDl;
    } else { // Upload
        lte = m_enb.at(0).m_maxBandwidthUl;
        wlan = m_accessPoint.at(0).m_maxBandwidthUl;
    }


    if (!LteWlan) {
        // Available Bandwith in the WLAN interface consists on monitoring the channel
        // occupation ratio, which determines the channel capacity for transmit:
        //      Hermes -- pg 56
        long double availableBandwith;

        // Verificar o menor tempo que influencia na nova aplicacao:
        //   timeVector -   1 2 3 5 8 10 15
        //   time app = 6
        // Entao eu sei que devo guardar a soma dos pacotes depois da posicao 4 do vetor de tempo.
        // Assim, eu irei pegar a posicao dele no vetor princiapl de aplicacoes da interface correspondente (appINTERFACEOnline)
        //  Cada aplicacao sabe a posicao que esta dentro deste vetor, entao eu nao preciso cuidar com a ordenacao de tempo,
        // ja terei esses valores.
        // 
        long double sumPacketSize = 0; // P - is the average packet size
        int qtAppOnline=0;
        double timeSuccess = 0; // T - is the average time for having success in transmission.
        std::vector<APPSENDING> orderByTimeWlan = (sentido) ? accessPoint.appWlanOnlineDl : accessPoint.appWlanOnlineUl;
        // Order the appWlanOnline by time.
        orderByTimeWlan = std::sort(orderByTimeWlan.begin(), orderByTimeWlan.end(), funSortTime);       
        std::vector<APPSENDING>::iterator iterator = orderByTimeWlan.begin();
        // verifica o vetor de tempo ate encontrar um valor maior ou igual ou encontrar o final do vetor de tempo
        while ((m_trafficApp.at(posicao).m_timeStart < *iterator) || iterator == orderByTimeWlan.end())
            ++iterator;        
        if (iterator != orderByTimeWlan.end()) { // se nao for o final do vetor de tempo
            // Efetuar o procedimento de soma dos tamanhos
            sumPacketSize += (*iterator).m_packetSize;
            timeSuccess += (*iterator).m_offTime + (*iterator).m_onTime;
            ++iterator;
            ++qtAppOnline;
        }
        
        availableBandwith = ((sumPacketSize/qtAppOnline) * (wlan - 134217.728)) / (timeSuccess/qtAppOnline);
        
        if (availableBandwith > 0) {
            m_trafficApp.at(posicao).m_acceptedNot = true; // a chamada foi aceita
        }
    }


    for (int c = posicao - 1; c >= 0; --c) {
        if (m_trafficApp.at(c).m_acceptedNot &&
                m_trafficApp.at(c).m_ulDl == sentido &&
                m_trafficApp.at(c).m_timeStop >= m_trafficApp.at(posicao).m_timeStart) {
            switch (m_trafficApp.at(c).m_app) {
                case VOIP:
                {
                    lte -= 25165.824;
                }
                    break;
                case VIDEO:
                {
                    lte -= 134217.728;
                }
                    break;
                case WWW:
                {
                    wlan -= 134217.728;
                }
                    break;
                case FTP:
                {
                    wlan -= 134217.728;
                }
                    break;
            }
        }
    }

    if (LteWlan == 1) {
        std::cout.precision(16);
        std::cout << " lteDisponivel = " << lte;
        return lte;
    } else {
        std::cout.precision(16);
        std::cout << " wlanDisponivel = " << wlan;
        return wlan;
    }

    return total;
}

bool HomeAgent::funSortTime(APPSENDING i, APPSENDING j) {
    return i.m_timeStart < j.m_timeStart;
}