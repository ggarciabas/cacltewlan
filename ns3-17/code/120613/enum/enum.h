/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  /description
 *  	This file define the ENUM of all the application possibilities.
 *  	Change the value to the name specified in the ENUM.
 *  /author Giovanna Garcia
 *  /date 23/03/13
 */

#include <iostream>



/////////////////////////////////////////////////////////////
//
//		ENUM Application
//
////////////////////////////////////////////////////////////

enum App {
	VOIP = 0, VIDEO = 1, WWW = 2, FTP = 3
};



/////////////////////////////////////////////////////////////
//
//	This function will change convert the value of ENUM
//		application to the tagged value.
//
////////////////////////////////////////////////////////////

std::ostream& operator<<(std::ostream &o, App n) {
	switch (n) {
	case VOIP:
		return o << "VoIP";
	case VIDEO:
		return o << "Video";
	case WWW:
		return o << "Www";
	case FTP:
			return o << "Ftp";
	default:
		return o << "(invalid value)";
	}
}
