/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:   UserEquipmentNode.h
 * Author: Giovanna Garcia
 *
 * Created on May 26, 2013, 2:07 PM
 */

#ifndef USEREQUIPMENTNODE_H
#define	USEREQUIPMENTNODE_H

#include "ns3/node.h"
#include "ns3/ipv4.h"

using namespace ns3;

class UserEquipmentNode : public Node {
public:
    UserEquipmentNode();
    int getPosAppVoip ();
    void setPosAppVoip (int);
    bool getVoipApp ();
    void setVoipApp (bool);
    Ipv4Address getIpv4AddressLte ();
    Ipv4Address getIpv4AddressWlan ();

    
private:
    // A bool verification if the ;ue are sending or receiving a VOIP application
    bool m_voipApp;
    // Inform the position of the voip app that the ue are sending
    int m_posAppVoip;
    // Inform if the node interface WLAN is A or G (model 802.11) respectively 0 and 1
    //int m_80211;
    
};

// After we will need to know which eNB the UE are connected.

#endif	/* USEREQUIPMENTNODE_H */

