/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:  AppSending.h
 * Author: Giovanna Garcia
 * 
 * Created on May 26, 2013, 3:34 PM
 */

#ifndef APPSENDING_H
#define	APPSENDING_H

#include "../pk_enumApp/enumApp.h"

class AppSending {
public:
    AppSending();
    
private:
    // Type of application
    EnumApp m_app;    
    
};

#endif	/* APPSENDING_H */

