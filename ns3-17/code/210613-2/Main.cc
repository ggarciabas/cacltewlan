/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * File:   Main.cc
 * Author: Giovanna Garcia
 *
 * Created on May 26, 2013, 12:49 PM
 */

#include <cstdlib>
#include "./pk_enb/EvolvedNodeBNode.cc"
#include "./pk_homeAgent/HomeAgentNode.cc"
#include "./pk_accessPoint/AccessPointNode.cc"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/config-store.h"
#include "ns3/epc-helper.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ipv4.h"
#include "ns3/network-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/trace-helper.h"

using namespace std;
using namespace ns3;

/*
        The main function
 */
int main(int argc, char *argv[]) {

    int qtEnb = 1; // Amount enb necessary to the simulation - default = 1
    int qtUe; // Amount ue necessary to the simulation
    int qtHa = 1; // Amount ha necessary to the simulation - default = 1
    int qtAp = 1; // Amount ap necessary to the simulation - default = 1

    //http://www.nsnam.org/doxygen-release/test-lte-rrc_8cc_source.html
    Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue(160));

    
    ////////////////////////////////////////////////////
    //
    //	Command Line
    //		Briefly summarize.
    //		Can inform the values of the variables on
    //	 the CMD (Command Line).
    //
    ///////////////////////////////////////////////////
    CommandLine cmd;
    cmd.AddValue("qtEnb", "Amount of ENB.", qtEnb);
    cmd.AddValue("qtUe", "Amount of UE.", qtUe);
    cmd.AddValue("qtHa", "Amount of HA.", qtHa);
    cmd.AddValue("qtAp", "Amount of AP.", qtAp);
    cmd.Parse(argc, argv);

    
    ////////////////////////////////////////////////////
    //
    // 	Log Component Enable
    //
    //
    //  obs.: Users may find it convenient to run on
    //	 explicit debugging for selected modules;
    //
    ////////////////////////////////////////////////////
    LogComponentEnable("TcpL4Protocol", LOG_LEVEL_ALL);
    LogComponentEnable("PacketSink", LOG_LEVEL_ALL);
    LogComponentEnable("OnOffApplication",
            LogLevel(LOG_LEVEL_ALL | LOG_PREFIX_FUNC | LOG_PREFIX_TIME));

    
    /////////////////////////////////////////////////////////////////////////////////
    //
    //	Lte Helper
    //    Creation and configuration of LTE entities.
    //		(*) This will instantiate some common
    //    objects and provide the methods to add eNBs
    //	  and UEs and configure them.
    //
    //	Epc Helper
    //   Helper class to handle the creation of the EPC entities and protocols.
    //   This Helper will create an EPC network topology comprising of a
    //   single node that implements both the SGW and PGW functionality, and
    //   is connected to all the eNBs in the simulation by means of the S1-U
    //   interface. 
    //
    //		(*) EPC implements both PGW and SGW functionality, and is connected to
    //    all the eNBs in the simulation.
    //			Evolved Packet Core is the IP-based core network by 3GPP inn release
    //	 8 for use by LTE and other access technologies. The goal of EPC is to provide
    //	 a simplified all-IP core network architecture to efficiently give access to
    // 	 various services such as the ones provided in IMS (IP Multimedia Subsystem).
    //	 EPC consist essentially of a Mobility Management Entity (MME) and access
    //	 agnostic Gateways for routing of user datagrams.
    //   @link http://lteuniversity.com/ask_the_expert/b/ltefaqs/archive/2008/11/03/what-is-epc.aspx
    //
    //	Pgw
    //			Creates a PGW node to communicating with
    // 		other technologies.
    //		(*) PGW is responsible to act an "anchor" of mobility between 3GPP and
    //	 non-3GPP technologies. PGW provides connectivity from the UE to external
    // 	 PDN by being the point of entry or exist of traffic for the UE.
    //	 The PGW manages policy enforcement, packet filtration for users, charging
    //	 support and LI. Possible to use non-3GPP technologies are: WiMAX, CDMA 1X
    //	 and EvDO.
    //	 @link http://www.lteandbeyond.com/2012/01/functions-of-main-lte-packet-core.html
    //
    //	 obs.: The EPC declared before implements both PGW and SGW, needs a EPC to
    //			create a PGW.
    //
    //	E-UTRAN
    //		The E-UTRAN logical nodes and interfaces between them, are defined as part of the Radio
    //	Network Layer. The E-UTRAN architecture consists of a set of eNBs connected to the EPC
    //	(Evolved Packet System) through the S1.
    //		The S1 interface is specified at the boundary between the EPC and the E-UTRAN.
    //
    //				                     ****** S1-MME
    //		----------------------	     *  -------------------------
    //		|     E-UTRAN        |       *  |          EPC          |
    //		|                    |       ---|----------             |
    //		|          ------    |     /    |     |MME|             |
    //		|          |eNB |----|----      |     -----             |
    //		|          ----- --- |          |        -----          |
    //		|            |      \|----      |       /|MME|          |
    //		|            |    ---|----\-----|------- -----          |
    //		|          ------/   |     -----|--                     |
    //		|          |eNB |    |          |  \--------            |
    //		|          ----------|----------|-- |S-GTW |            |
    //		|                    |     *    |  \--------            |
    //		----------------------	   *    -------------------------
    //				                   ****** S1-U
    //
    //		From the s1 perspective, the E-UTRAN access point is an eNB, and the EPC access point
    //	is either the control plane MME logical node or the user plane SAE GTW logical node. Two
    //	types of S1 interface are thus defined at the boundary depending on the EPC access point:
    //	S1-MME towards an MMe and S1-U towards an SAE GTW.
    //		S1 is a logical interface.
    //  The Intra-LTE handover function supports mobility for UEs in LTE_ACTIVE and comprises
    //	the preparation, execution and completion of handover via the X2 and S1 interfaces.
    //
    //
    //  TS 36.410 3GPP
    //		LINk http://www.quintillion.co.jp/3GPP/Specs/36410-800.pdf
    //
    ////////////////////////////////////////////////////////////////////////////////
    std::cout << "-----------> Creating Lte Helper, EPC Helper and PGW node"
            << std::endl;
    // LTE
    Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
    lteHelper->SetSchedulerType("ns3::RrFfMacScheduler");
    // lteHelper->setPathlossModelType("ns3::Cost231PropagationLossModel");
    lteHelper->SetAttribute("PathlossModel",
            StringValue("ns3::FriisPropagationLossModel"));
    // EPC
    Ptr<EpcHelper> epcHelper = CreateObject<EpcHelper>();
    lteHelper->SetEpcHelper(epcHelper);
    // PGW
    Ptr<ns3::Node> pgwNode = epcHelper->GetPgwNode();

    /*
     *  Description:
     *          Creating a NodeContainer to hold the Evolved NodeB.
     *  NodeContainer:
     *          Keep track of a set of node pointers. 
     */
    NodeContainer enbNodeContainer; // Holds all the EnbNode created.
    for (int c = 0; c < qtEnb; ++c) {
        enbNodeContainer.Add(CreateObject<EvolvedNodeBNode>());
        // enbNodeContainer.Get(c);
    }
    

    /*
     *  Description:
     *          Creating a NodeContainer to hold the User Equipment.
     *  NodeContainer:
     *          Keep track of a set of node pointers. 
     */
    NodeContainer ueNodeContainer; // Holds all the Ue created.
    for (int c = 0; c < qtUe; ++c)
        ueNodeContainer.Add(new UserEquipmentNode());

    /*
     *  Description:
     *          Creating a NodeContainer to hold the Home Agent.
     *  NodeContainer:
     *          Keep track of a set of node pointers. 
     */
    NodeContainer haNodeContainer;
    for (int c = 0; c < qtHa; ++c)
        haNodeContainer.Add(new HomeAgentNode());

    /*
     *  Description:
     *          Creating a NodeContainer to hold the Access Point.
     *  NodeContainer:
     *          Keep track of a set of node pointers. 
     */
    NodeContainer apNodeContainer;
    for (int c = 0; c < qtAp; ++c)
        apNodeContainer.Add(new AccessPointNode(26214400, 26214400));


    /////////////////////////////////////////////////////////////////////////////
    //
    // 	Setting the mobility to all the nodes created
    //			before
    // 	Mobility Model
    //		(*) Define mobility to the created node.
    //
    //	Nodes that use "ns3::RandomWalk2dMobilityModel"
    //	and are at APs the equation to calculate the
    // 	allowed area to walk:
    //		side = sqrt(((diameter)^2)/2)
    //	side is the side of the square.
    //
    // 	PGW is the center of the simulation struct
    //
    //	--> All the nodes position was defined in image/SimulationStruct.png
    //
    /////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up the Mobility" << std::endl;
    MobilityHelper mobilityHelper;
    // Packet Data Network Gateway
    Ptr<ListPositionAllocator> pgwPosition =
            CreateObject<ListPositionAllocator>();
    pgwPosition->Add(Vector(1768, 1200, 0));
    mobilityHelper.SetPositionAllocator(pgwPosition);
    mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install(pgwNode);
    // Enhanced node B
    Ptr<ListPositionAllocator> enbrPosition =
            CreateObject<ListPositionAllocator>();
    enbrPosition->Add(Vector(1100, 1346, 0));
    mobilityHelper.SetPositionAllocator(enbrPosition);
    mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install(enbNodeContainer.Get(0));
    // Server Node
    Ptr<ListPositionAllocator> serverPosition = CreateObject<
            ListPositionAllocator>();
    serverPosition->Add(Vector(1768, 1944, 0));
    mobilityHelper.SetPositionAllocator(serverPosition);
    mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install(haNodeContainer.Get(0));
    // User Equipment Lte Wlan
    mobilityHelper.SetPositionAllocator("ns3::RandomDiscPositionAllocator", "X",
            StringValue("2192.0"), "Y", StringValue("1530.0"), "Rho",
            StringValue("ns3::UniformRandomVariable[Min=1|Max=106.066]")); // 300 Meters, radius of the cell
    // here is used the side (calculated before) divided by 2, because is min and max to up and to down
    mobilityHelper.SetMobilityModel("ns3::RandomWalk2dMobilityModel", "Mode",
            StringValue("Time"), "Time", StringValue("0.05s"), "Speed",
            StringValue("ns3::ConstantRandomVariable[Constant=25.0]"), "Bounds",
            StringValue("2086|2298|1424|1636"));
    mobilityHelper.Install(ueNodeContainer);
    // Access point
    Ptr<ListPositionAllocator> accessPointPosition = CreateObject<
            ListPositionAllocator>();
    accessPointPosition->Add(Vector(1344, 1530, 0));
    mobilityHelper.SetPositionAllocator(accessPointPosition);
    mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install(apNodeContainer);


    //////////////////////////////////////////////////////////////////////////////////////////////
    //
    //		Configuring the LTE Nodes
    //
    //		Creating to each node the LTE net device and
    //	grouping these devices to a Net Device Container.
    //
    //	LINK http://www.nsnam.org/doxygen-release/classns3_1_1_net_device_container.html#details
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up Lte nodes" << std::endl;
    // eNB
    NetDeviceContainer enbLteDeviceContainer;
    enbLteDeviceContainer = lteHelper->InstallEnbDevice(enbNodeContainer);
    // Ue Lte Wlan
    std::cout << " here " << std::endl;
    NetDeviceContainer ueLteDeviceContainer;
    ueLteDeviceContainer = lteHelper->InstallUeDevice(ueNodeContainer);

    
    ///////////////////////////////////////////////////////////////////////
    //
    //	Installing the Internet Stack Helper
    //
    //	LINK http://www.nsnam.org/doxygen/group__internet_stack_model.html
    //
    //	The internet stack provides a number of trace
    //	sources in its various protocol implementations.
    //	These trace sources can be hooked using your own
    //	custom trace code, or you can use our helper
    //	functions in some cases to arrange for tracing
    //	to be enabled.
    //
    //////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up Internet Stack Helper" << std::endl;
    InternetStackHelper internetStackHelper;
    internetStackHelper.Install(haNodeContainer);
    internetStackHelper.Install(ueNodeContainer);
    internetStackHelper.Install(apNodeContainer);


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //		Creating the Wifi Helper
    //
    //		Helps to create WifiNetDevice objects
    //		This class can help to create a large set of similar WifiNetDevice objects and to configure a large set of their
    //		attributes during creation.
    //
    //		SetStandard ->
    //			http://www.nsnam.org/doxygen/yans-wifi-phy_8cc_source.html
    //
    //	LINK   http://www.nsnam.org/doxygen/classns3_1_1_wifi_helper.html#details
    //		   http://www.nsnam.org/docs/models/html/wifi.html
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    WifiHelper wifiHelper = WifiHelper::Default();
    wifiHelper.SetStandard(WIFI_PHY_STANDARD_80211g); // Define the standard 802.11G - http://standards.ieee.org/about/get/802/802.11.html
    wifiHelper.SetRemoteStationManager("ns3::ArfWifiManager");


    //////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Create a SSID (Service Set Identifier)
    //
    //	We configure the type of MAC, the SSID of the infrastructure network we want to setup and
    //	make sure that our stations don't perform active probing
    //  	The particular kind of MAC layer is specified by Attribute as being of the "ns3::StaWifiMac"
    //	type. he use of NqosWifiMacHelper will ensure that the
    //  “QosSupported” Attribute for created MAC objects is set false. The combination of these two
    //	configurations means that the MAC instance next created will
    //  be a non-QoS non-AP station (STA) in an infrastructure BSS (i.e., a BSS with an AP). Finally,
    //	the “ActiveProbing” Attribute is set to false. This means
    //  that probe requests will not be sent by MACs created by this helper.
    //  	In this case, the NqosWifiMacHelper is going to create MAC layers of the “ns3::ApWifiMac”,
    //	the latter specifying that a MAC instance configured as an
    //  AP should be created, with the helper type implying that the “QosSupported” Attribute should be
    //	set to false - disabling dlPort2.11e/WMM-style QoS support at
    //  created APs.
    //  	I set the “BeaconGeneration” Attribute to true and also set an interval between beacons of
    //	2.5 seconds.
    //  	The ActiveProbing is set up to false, because the probe request will not be sent by MACs
    //	created by this helper.
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Creating the SSID local" << std::endl;
    Ssid ssidAp = Ssid("WifiLocal");


    //////////////////////////////////////////////////////////////////////////////////////
    //
    // 		Non QoS Wifi Mac Helper
    //
    //	 LINK http://www.nsnam.org/doxygen/classns3_1_1_nqos_wifi_mac_helper.html#details
    //
    /////////////////////////////////////////////////////////////////////////////////////
    NqosWifiMacHelper nqosWifiMacHelper = NqosWifiMacHelper::Default();
    NqosWifiMacHelper accessPointMacHelper = NqosWifiMacHelper::Default();
    nqosWifiMacHelper.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssidAp));
    accessPointMacHelper.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssidAp));


    ////////////////////////////////////////////////////
    //
    //  Yans Wifi Channel Helper
    //
    //   Configure the channel to the wireless hosts and
    // access point.
    //
    //	LINK: http://cutebugs.net/files/wns2-yans.pdf
    //
    ////////////////////////////////////////////////////
    // Creating channel to the wireless connection
    std::cout << "\n-----------> Setting up the Channel to Wlan network"
            << std::endl;
    YansWifiChannelHelper wifiChannelHelper = YansWifiChannelHelper::Default();
    YansWifiPhyHelper accessPointPhyHelper = YansWifiPhyHelper::Default();
    accessPointPhyHelper.SetChannel(wifiChannelHelper.Create());
    // Ue wireless device
    NetDeviceContainer ueWirelessDeviceContainer;
    ueWirelessDeviceContainer = wifiHelper.Install(accessPointPhyHelper, nqosWifiMacHelper,
            ueNodeContainer);
    // Access Point wireless device
    NetDeviceContainer apWirelessDeviceContainer;
    apWirelessDeviceContainer = wifiHelper.Install(accessPointPhyHelper, accessPointMacHelper, apNodeContainer.Get(0));


    ///////////////////////////////////////////////////////////////////////////////////////////////
    //
    //	Creating channels
    //
    //	Point-to-point helper
    //	  Build a set of PointToPointNetDevice objects.
    //	  Normally we eschew multiple inheritance, however, the classes PcapUserHelperForDevice and
    //    AsciiTraceUserHelperForDevice are "mixins".
    //
    //	LINK http://www.nsnam.org/doxygen-release/classns3_1_1_point_to_point_helper.html#details
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up Point-to-Point channel"
            << std::endl;
    PointToPointHelper pppHelper; // Verificar parametros do PointToPoint
    pppHelper.SetDeviceAttribute("DataRate", DataRateValue(DataRate("100Mbps")));
    //pppHelper.SetDeviceAttribute("Mtu", UintegerValue(576));  // http://en.wikipedia.org/wiki/Maximum_transmission_unit
    //pppHelper.SetChannelAttribute("Delay", TimeValue(Seconds(0.01)));

    // Server to PGW - Device 1
    NetDeviceContainer pgwHaPppDevice = pppHelper.Install(
            NodeContainer(pgwNode, haNodeContainer));
    // Server to AP - Device 3
    NetDeviceContainer apHaPppDevice = pppHelper.Install(
            NodeContainer(apNodeContainer, haNodeContainer));

    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //	Ipv4 Interface Container
    //		We've got the "hardware" in place. Now we
    //	need to add IP addresses.
    //
    //		A helper class to make life easier while doing simple IPv4 address assignment in scripts.
    //		This class is a very simple IPv4 address generator. You can think of it as a simple local number incrementer.
    //		It has no notion that IP addresses are part of a global address space. If you have a complicated address
    //		assignment situation you may want to look at the Ipv4AddressGenerator which does recognize that IP address
    //		and network number generation is part of a global problem. Ipv4AddressHelper is a simple class to make simple
    //		problems easy to handle.
    //		We do call into the global address generator to make sure that there are no duplicate addresses generated.
    //
    //	LINK http://www.nsnam.org/doxygen-release/classns3_1_1_ipv4_address_helper.html#details
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up Ipv4 Interface Container"
            << std::endl;
    Ipv4AddressHelper ipv4Helper;
    // server and Pgw
    ipv4Helper.SetBase("192.168.1.0", "255.255.255.0"); // setting up the IP base, this simulation needs to be Ipv4 because the NS3 have no support to IPv6 yet.
    Ipv4InterfaceContainer pgwHaPppInterfaceContainer = ipv4Helper.Assign(pgwHaPppDevice);
    // Server and AP
    ipv4Helper.SetBase("192.168.2.0", "255.255.255.0");
    Ipv4InterfaceContainer apHaPppInterfaceContainer = ipv4Helper.Assign(apHaPppDevice);
    // Access Point 
    ipv4Helper.SetBase("11.0.0.0", "255.0.0.0");
    Ipv4InterfaceContainer apWirelessInterfaceContainer = ipv4Helper.Assign(apWirelessDeviceContainer);
    // User Equipment  
    Ipv4InterfaceContainer ueWirelessInterfaceContainer = ipv4Helper.Assign(ueWirelessDeviceContainer);
    Ipv4InterfaceContainer ueLteInterfaceContainer = epcHelper->AssignUeIpv4Address(ueLteDeviceContainer);

    
    /////////////////////////////////////////////////
    //
    //	Setting static route
    //
    //	Ipv4 Static Routing
    //	 	(*) Static routing protocol IP version
    //	 4 stacks.
    //			This class provides a basic set of
    //	 methods for inserting static unicast and
    //	 multicast routes into the ipv4 routing system.
    //			This particular protocol is designed
    //	 to be inserted into an Ipv4ListRouting
    //	 protocol but can be used also as a standalone
    //	 protocol.
    //
    /////////////////////////////////////////////////
    // Home agent routing configuration
    Ipv4StaticRoutingHelper ipv4StaticRoutingHelper;
    Ptr<Ipv4StaticRouting> serverStaticRouting = ipv4StaticRoutingHelper.GetStaticRouting(haNodeContainer.Get(0)->GetObject<Ipv4> ());
    // Network route to the PGW, the output is 1 because we created the pgwToServer before.
    serverStaticRouting->AddNetworkRouteTo(Ipv4Address("7.0.0.0"), Ipv4Mask("255.0.0.0"), 1);
    // Network route to the AccessPoint, the output is 2 because we created apToServer after.
    serverStaticRouting->AddNetworkRouteTo(Ipv4Address("11.0.0.0"), Ipv4Mask("255.0.0.0"), 2);

    // Access Point routing configuration
    Ptr<Ipv4StaticRouting> accessPointStaticRouting = ipv4StaticRoutingHelper.GetStaticRouting(apNodeContainer.Get(0)->GetObject<Ipv4>());
    // Network route to access the ues inside LTE
    accessPointStaticRouting->AddNetworkRouteTo(Ipv4Address("7.0.0.0"), Ipv4Mask("255.0.0.0"), Ipv4Address("192.168.2.2"), 2);
    // Network route to access the network between the HomeAgent and the PGW
    accessPointStaticRouting->AddNetworkRouteTo(Ipv4Address("192.168.1.0"), Ipv4Mask("255.255.255.0"), Ipv4Address("192.168.2.2"), 2);
    // Network route to connect with the Home Agent
    accessPointStaticRouting->AddNetworkRouteTo(Ipv4Address("192.168.2.0"), Ipv4Mask("255.255.255.0"), 2);

    for (uint16_t c = 0; c < ueNodeContainer.GetN(); ++c) {
        Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4StaticRoutingHelper.GetStaticRouting(ueNodeContainer.Get(c)->GetObject <Ipv4>());
        // Default route WLAN - 2 because we created the WLAN device after LTE.
        ueStaticRouting->SetDefaultRoute(Ipv4Address("11.0.0.1"), 2);
        // Default route LTE - 1 because we created the LTE device before WLAN.
        ueStaticRouting->SetDefaultRoute(epcHelper->GetUeDefaultGatewayAddress(), 1);
        // Network route to the server from WLAN
        ueStaticRouting->AddNetworkRouteTo(Ipv4Address("192.168.2.2"), Ipv4Mask("255.255.255.0"), Ipv4Address("11.0.0.1"), 2);
        // Network route to the server from LTE
        ueStaticRouting->AddNetworkRouteTo(Ipv4Address("192.168.2.2"), Ipv4Mask("255.255.255.0"), 1);
        // Network route to the PGW from WLAN
        ueStaticRouting->AddNetworkRouteTo(Ipv4Address("912.168.2.1"), Ipv4Mask("255.255.255.0"), Ipv4Address("193.168.2.2"), 2);
    }

    
    ///////////////////////////////////////////////////////
    //
    //	Atach UE to eNB
    //			(*) Attach the UEs to an eNB. THIs will
    //	 configure each UE according to the eNB configuration,
    //	 and create an RRC connection between them.
    //
    //////////////////////////////////////////////////////
    // ue lte wlan (802.11A)
    lteHelper->Attach(ueLteDeviceContainer, enbLteDeviceContainer.Get(0));


    //////////////////////////////////////////
    //
    //	Enabling components of the LTE
    //
    //////////////////////////////////////////
    lteHelper->EnableTraces();


    // ----- TEST
    for (int c = 0; c < qtEnb; ++c)
        cout << "Enb n " << enbNodeContainer.Get(c)->GetId() << endl;
    for (int c = 0; c < qtUe; ++c)
        cout << "Ue n " << ueNodeContainer.Get(c)->GetId() << endl;
    for (int c = 0; c < qtHa; ++c)
        cout << "Ha n " << haNodeContainer.Get(c)->GetId() << endl;
    for (int c = 0; c < qtAp; ++c)
        cout << "Ap n " << apNodeContainer.Get(c)->GetId() << endl;
    // -----

    
    //////////////////////////////////////////////////////////////
    //
    // 	Run Simulation
    //
    //	Simulator STOP
    //		(*) This is needed otherwise the simulation will last
    //   forever, because (among others) the start-of-subframe
    // 	 event is scheduled repeatedly, and the ns-3 simulator
    //   scheduler will hence never run out of events.
    //
    //	Simulator Schedule
    //		(*) Execute a defined function.
    //
    //	Simulator Run
    //		(*) Run the simulation.
    //
    //	Simulator Destroy
    //		(*) Cleanup and exit.
    //
    ////////////////////////////////////////////////////////////
    Simulator::Stop(Seconds(120.0));
    //Simulator::Schedule(Seconds(0.001), linkDescription);
    std::cout << "\n\n\nStarting the simulation...\n\n" << std::endl;
    Simulator::Run();
    Simulator::Destroy();
    return 0;
}

