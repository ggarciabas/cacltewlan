/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  /file Simulation LTE and WLAN
 * 	/brief
 * 	/author Giovanna Garcia
 *
 * 		The overall LTE architecture are described in TS 36.401 3GPP:
 * 			LINK http://www.3gpp.org/ftp/Specs/html-info/36401.htm
 *
 * 	OBS.:
 * 		- Fazer testes com os modelos 802.11 A e G. ( )
 */

////////////////////////////////////////////
//
//  Headers declarations
// 	- System c++ and c
// 	- Ns3 headers
//
///////////////////////////////////////////
#include <iostream>
#include <vector>
#include "HomeAgent.h"

using namespace ns3;

////////////////////////////////////////////////
//
//	Defining the log of the component
//
///////////////////////////////////////////////

NS_LOG_COMPONENT_DEFINE("CacLteWlan");


/////////////////////////////////////////////////////
//
//		The main function
//
////////////////////////////////////////////////////

int main(int argc, char *argv[]) {

    /*
     *   Srs Periodicity
     *          m_nUes < 25 = 40
     *          m_nUes < 60 = 80
     *          m_nUes < 120 = 160
     *          m_nUes >= 120 = 320
     * 
     *   http://www.nsnam.org/doxygen-release/test-lte-rrc_8cc_source.html
     */
    Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue(160));

    //////////////////////////////////////////////////
    //
    //	Local Variables
    //	-- dlPort - used to define the download port
    //	-- ulPort - used to define the upload port
    //	-- file - indicates the compilation version
    //	-- traffic - indicates the random applications
    //		to the simulation
    //
    ///////////////////////////////////////////////////
    uint32_t dlPort = 8080, ulPort = 8081;
    std::string file, date;
    std::vector<App> trafficApplications(10);

    ////////////////////////////////////////////////////
    //
    //	Command Line
    //		Briefly summarize.
    //		Can inform the values of the variables on
    //	 the CMD (Command Line).
    //
    ///////////////////////////////////////////////////
    CommandLine cmd;
    cmd.AddValue("file", "Animation file.", file);
    cmd.AddValue("date", "Date", date);
    cmd.Parse(argc, argv);

    ////////////////////////////////////////////////////
    //
    // 	Log Component Enable
    //
    //
    //  obs.: Users may find it convenient to run on
    //	 explicit debugging for selected modules;
    //
    ////////////////////////////////////////////////////
    LogComponentEnable(
            "Call_Admission_Control_on_Long_Term_Evolution_and_Wireless_Local_Area_Network",
            LOG_LEVEL_INFO);
    LogComponentEnable("TcpL4Protocol", LOG_LEVEL_ALL);
    LogComponentEnable("PacketSink", LOG_LEVEL_ALL);
    LogComponentEnable("OnOffApplication",
            LogLevel(LOG_LEVEL_ALL | LOG_PREFIX_FUNC | LOG_PREFIX_TIME));

    /////////////////////////////////////////////////////////////////////////////////
    //
    //	Lte Helper
    // 			Used to help the configuration of the
    // 		nodes LTE.
    //		(*) This will instantiate some common
    //    objects and provide the methods to add eNBs
    //	  and UEs and configure them.
    //
    //	Epc Helper
    //			Used to help the configuration of the
    // 		EPC node.
    //		(*) EPC implements both PGW and SGW functionality, and is connected to
    //    all the eNBs in the simulation.
    //			Evolved Packet Core is the IP-based core network by 3GPP inn release
    //	 8 for use by LTE and other access technologies. The goal of EPC is to provide
    //	 a simplified all-IP core network architecture to efficiently give access to
    // 	 various services such as the ones provided in IMS (IP Multimedia Subsystem).
    //	 EPC consist essentially of a Mobility Management Entity (MME) and access
    //	 agnostic Gateways for routing of user datagrams.
    //   @link http://lteuniversity.com/ask_the_expert/b/ltefaqs/archive/2008/11/03/what-is-epc.aspx
    //
    //	Pgw
    //			Creates a PGW node to communicating with
    // 		other technologies.
    //		(*) PGW is responsible to act an "anchor" of mobility between 3GPP and
    //	 non-3GPP technologies. PGW provides connectivity from the UE to external
    // 	 PDN by being the point of entry or exist of traffic for the UE.
    //	 The PGW manages policy enforcement, packet filtration for users, charging
    //	 support and LI. Possible to use non-3GPP technologies are: WiMAX, CDMA 1X
    //	 and EvDO.
    //	 @link http://www.lteandbeyond.com/2012/01/functions-of-main-lte-packet-core.html
    //
    //	 obs.: The EPC declared before implements both PGW and SGW, needs a EPC to
    //			create a PGW.
    //
    //	E-UTRAN
    //		The E-UTRAN logical nodes and interfaces between them, are defined as part of the Radio
    //	Network Layer. The E-UTRAN architecture consists of a set of eNBs connected to the EPC
    //	(Evolved Packet System) through the S1.
    //		The S1 interface is specified at the boundary between the EPC and the E-UTRAN.
    //
    //				                     ****** S1-MME
    //		----------------------	     *  -------------------------
    //		|     E-UTRAN        |       *  |          EPC          |
    //		|                    |       ---|----------             |
    //		|          ------    |     /    |     |MME|             |
    //		|          |eNB |----|----      |     -----             |
    //		|          ----- --- |          |        -----          |
    //		|            |      \|----      |       /|MME|          |
    //		|            |    ---|----\-----|------- -----          |
    //		|          ------/   |     -----|--                     |
    //		|          |eNB |    |          |  \--------            |
    //		|          ----------|----------|-- |S-GTW |            |
    //		|                    |     *    |  \--------            |
    //		----------------------	   *    -------------------------
    //				                   ****** S1-U
    //
    //		From the s1 perspective, the E-UTRAN access point is an eNB, and the EPC access point
    //	is either the control plane MME logical node or the user plane SAE GTW logical node. Two
    //	types of S1 interface are thus defined at the boundary depending on the EPC access point:
    //	S1-MME towards an MMe and S1-U towards an SAE GTW.
    //		S1 is a logical interface.
    //  The Intra-LTE handover function supports mobility for UEs in LTE_ACTIVE and comprises
    //	the preparation, execution and completion of handover via the X2 and S1 interfaces.
    //
    //
    //  TS 36.410 3GPP
    //		LINk http://www.quintillion.co.jp/3GPP/Specs/36410-800.pdf
    //
    ////////////////////////////////////////////////////////////////////////////////
    std::cout << "-----------> Creating Lte Helper, EPC Helper and PGW node"
            << std::endl;
    // LTE
    Ptr<LteHelper> lteHelper = CreateObject<LteHelper>();
    lteHelper->SetSchedulerType("ns3::RrFfMacScheduler");
    // lteHelper->setPathlossModelType("ns3::Cost231PropagationLossModel");
    lteHelper->SetAttribute("PathlossModel",
            StringValue("ns3::FriisPropagationLossModel"));
    // EPC
    Ptr<EpcHelper> epcHelper = CreateObject<EpcHelper>();
    lteHelper->SetEpcHelper(epcHelper);
    // PGW
    Ptr<ns3::Node> pgwNode = epcHelper->GetPgwNode();


    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //	Creating ENB nodes
    //		- 0 -> eNB left
    //		- 1 -> eNb right
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Creating a ENB node container" << std::endl;
    NodeContainer enbNodContainer;
    enbNodContainer.Create(1);

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //
    // 	Creating UE node(s)
    //
    //	The establishment of the overall initial UE context is initiated by the MME.
    //  The S1 UE context management function also supports the release of the context previously
    //  established in the eNB to enable the active-to-idle transition. The release of the context
    //  is triggered by the MME either directly or following a request received from the eNB.
    //	(TS 36.410)
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Creating Ues (LTE and Wlan) node container"
            << std::endl;
    NodeContainer ueLteWlan80211GNodContainer; // this Ue node will connect to the WLAN (802.11G) too.
    ueLteWlan80211GNodContainer.Create(10);
    NodeContainer ueLteWlan80211ANodContainer; // this Ue node will connect to the WLAN (802.11A) too.
    ueLteWlan80211ANodContainer.Create(25);

    ///////////////////////////////////////////////////
    //
    // 	Creating Host node(s)
    //
    //		The server nodes will be the Home Agent.
    //
    //////////////////////////////////////////////////
    std::cout << "\n-----------> Creating a Server node container" << std::endl;
    NodeContainer serverNodContainer;
    serverNodContainer.Create(1);

    ///////////////////////////////////////////////////
    //
    //  Creating a access point.
    // 	The access point will connect all wireless
    //		equipments.
    //
    //////////////////////////////////////////////////
    std::cout << "\n-----------> Creating a Access Point node" << std::endl;
    Ptr<ns3::Node> accessPointNode80211G = CreateObject<ns3::Node>();
    Ptr<ns3::Node> accessPointNode80211A = CreateObject<ns3::Node>();

    /////////////////////////////////////////////////////////////////////////////
    //
    // 	Setting the mobility to all the nodes created
    //			before
    // 	Mobility Model
    //		(*) Define mobility to the created node.
    //
    //	Nodes that use "ns3::RandomWalk2dMobilityModel"
    //	and are at APs the equation to calculate the
    // 	allowed area to walk:
    //		side = sqrt(((diameter)^2)/2)
    //	side is the side of the square.
    //
    // 	PGW is the center of the simulation struct
    //
    //	--> All the nodes position was defined in image/SimulationStruct.png
    //
    /////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up the Mobility" << std::endl;
    ns3::MobilityHelper mobilityHelper; // todo mobility
    // Packet Data Network Gateway
    Ptr<ListPositionAllocator> pgwPosition =
            CreateObject<ListPositionAllocator>();
    pgwPosition->Add(Vector(1768, 1200, 0));
    mobilityHelper.SetPositionAllocator(pgwPosition);
    mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install(pgwNode);
    // Enhanced node B - right
    Ptr<ListPositionAllocator> enbrPosition =
            CreateObject<ListPositionAllocator>();
    enbrPosition->Add(Vector(2438, 1346, 0));
    mobilityHelper.SetPositionAllocator(enbrPosition);
    mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install(enbNodContainer.Get(0));
    // Enhanced node B - left
    Ptr<ListPositionAllocator> enblPosition =
            CreateObject<ListPositionAllocator>();
    enblPosition->Add(Vector(1100, 1346, 0));
    mobilityHelper.SetPositionAllocator(enblPosition);
    mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install(enbNodContainer.Get(0));
    // Server Node
    Ptr<ListPositionAllocator> serverPosition = CreateObject<
            ListPositionAllocator>();
    serverPosition->Add(Vector(1768, 1944, 0));
    mobilityHelper.SetPositionAllocator(serverPosition);
    mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install(serverNodContainer);
    // User Equipment Lte Wlan
    mobilityHelper.SetPositionAllocator("ns3::RandomDiscPositionAllocator", "X",
            StringValue("1344.0"), "Y", StringValue("1530.0"), "Rho",
            StringValue("ns3::UniformRandomVariable[Min=1|Max=106.066]")); // 300 Meters, radius of the cell
    // here is used the side (calculated before) divided by 2, because is min and max to up and to down
    // Verificar quanto a aproximacao da torre permitida e colocar no parâmetro mínimo.
    mobilityHelper.SetMobilityModel("ns3::RandomWalk2dMobilityModel", "Mode",
            StringValue("Time"), "Time", StringValue("0.05s"), "Speed",
            StringValue("ns3::ConstantRandomVariable[Constant=25.0]"), "Bounds",
            StringValue("1238|1450|1424|1636"));
    mobilityHelper.Install(ueLteWlan80211ANodContainer); // Deu erro quando eu cloquei ueLteWlan Node .. precisei utilizar NodeContainer para que fosse completado
    // User Equipment Lte Wlan
    mobilityHelper.SetPositionAllocator("ns3::RandomDiscPositionAllocator", "X",
            StringValue("2192.0"), "Y", StringValue("1530.0"), "Rho",
            StringValue("ns3::UniformRandomVariable[Min=1|Max=106.066]")); // 300 Meters, radius of the cell
    // here is used the side (calculated before) divided by 2, because is min and max to up and to down
    // Verificar quanto a aproximacao da torre permitida e colocar no parâmetro mínimo. TODO
    mobilityHelper.SetMobilityModel("ns3::RandomWalk2dMobilityModel", "Mode",
            StringValue("Time"), "Time", StringValue("0.05s"), "Speed",
            StringValue("ns3::ConstantRandomVariable[Constant=25.0]"), "Bounds",
            StringValue("2086|2298|1424|1636"));
    mobilityHelper.Install(ueLteWlan80211GNodContainer); // Deu erro quando eu coloquei ueLteWlan Node .. precisei utilizar NodeContainer para que fosse completado
    // Access point
    Ptr<ListPositionAllocator> accessPointPosition = CreateObject<
            ListPositionAllocator>();
    accessPointPosition->Add(Vector(1344, 1530, 0));
    mobilityHelper.SetPositionAllocator(accessPointPosition);
    mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install(accessPointNode80211G);

    accessPointPosition->Add(Vector(2192, 1530, 0));
    mobilityHelper.SetPositionAllocator(accessPointPosition);
    mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install(accessPointNode80211A);

    //////////////////////////////////////////////////////////////////////////////////////////////
    //
    //		Configuring the LTE Nodes
    //
    //		Creating to each node the LTE net device and
    //	grouping these devices to a Net Device Container.
    //
    //	LINK http://www.nsnam.org/doxygen-release/classns3_1_1_net_device_container.html#details
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up Lte nodes" << std::endl;
    // eNB
    NetDeviceContainer enbDevContainer;
    enbDevContainer = lteHelper->InstallEnbDevice(enbNodContainer);
    // Ue Lte Wlan (802.11A)
    NetDeviceContainer ueLteWlanDevContainerLWA;
    ueLteWlanDevContainerLWA = lteHelper->InstallUeDevice(
            ueLteWlan80211ANodContainer);
    // Ue Lte Wlan (802.11G)
    NetDeviceContainer ueLteWlanDevContainerLWG;
    ueLteWlanDevContainerLWG = lteHelper->InstallUeDevice(
            ueLteWlan80211GNodContainer);

    ///////////////////////////////////////////////////////////////////////
    //
    //	Installing the Internet Stack Helper
    //
    //	LINK http://www.nsnam.org/doxygen/group__internet_stack_model.html
    //
    //	The internet stack provides a number of trace
    //	sources in its various protocol implementations.
    //	These trace sources can be hooked using your own
    //	custom trace code, or you can use our helper
    //	functions in some cases to arrange for tracing
    //	to be enabled.
    //
    //////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up Internet Stack Helper" << std::endl;
    InternetStackHelper internetStackHelper;
    internetStackHelper.Install(serverNodContainer);
    internetStackHelper.Install(accessPointNode80211G);
    internetStackHelper.Install(accessPointNode80211A);
    internetStackHelper.Install(ueLteWlan80211GNodContainer);
    internetStackHelper.Install(ueLteWlan80211ANodContainer);

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //		Creating the Wifi Helper
    //
    //		Helps to create WifiNetDevice objects
    //		This class can help to create a large set of similar WifiNetDevice objects and to configure a large set of their
    //		attributes during creation.
    //
    //		SetStandard ->
    //			http://www.nsnam.org/doxygen/yans-wifi-phy_8cc_source.html
    //
    //	LINK   http://www.nsnam.org/doxygen/classns3_1_1_wifi_helper.html#details
    //		   http://www.nsnam.org/docs/models/html/wifi.html
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    WifiHelper wifiHelper80211G = WifiHelper::Default();
    wifiHelper80211G.SetStandard(WIFI_PHY_STANDARD_80211g); // Define the standard 802.11G - http://standards.ieee.org/about/get/802/802.11.html
    // wifiHelper80211G.SetRemoteStationManager("ns3::ArfWifiManager");
    wifiHelper80211G.SetRemoteStationManager("ns3::ArfWifiManager",
            "RtsCtsThreshold", StringValue("0")); //Enable RTS/CTS for every packet
    WifiHelper wifiHelper80211A = WifiHelper::Default();
    wifiHelper80211A.SetStandard(WIFI_PHY_STANDARD_80211a); // Define the standard 802.11A - http://standards.ieee.org/about/get/802/802.11.html
    wifiHelper80211A.SetRemoteStationManager("ns3::ArfWifiManager");

    //////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Create a SSID (Service Set Identifier)
    //
    //	We configure the type of MAC, the SSID of the infrastructure network we want to setup and
    //	make sure that our stations don't perform active probing
    //  	The particular kind of MAC layer is specified by Attribute as being of the "ns3::StaWifiMac"
    //	type. he use of NqosWifiMacHelper will ensure that the
    //  “QosSupported” Attribute for created MAC objects is set false. The combination of these two
    //	configurations means that the MAC instance next created will
    //  be a non-QoS non-AP station (STA) in an infrastructure BSS (i.e., a BSS with an AP). Finally,
    //	the “ActiveProbing” Attribute is set to false. This means
    //  that probe requests will not be sent by MACs created by this helper.
    //  	In this case, the NqosWifiMacHelper is going to create MAC layers of the “ns3::ApWifiMac”,
    //	the latter specifying that a MAC instance configured as an
    //  AP should be created, with the helper type implying that the “QosSupported” Attribute should be
    //	set to false - disabling dlPort2.11e/WMM-style QoS support at
    //  created APs.
    //  	I set the “BeaconGeneration” Attribute to true and also set an interval between beacons of
    //	2.5 seconds.
    //  	The ActiveProbing is set up to false, because the probe request will not be sent by MACs
    //	created by this helper.
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Creating the SSID local" << std::endl;
    Ssid ssidAp80211G = Ssid("WifiLocal80211G");
    Ssid ssidAp80211A = Ssid("WifiLocal80211A");

    /////////////////////////////////////////////////////////////////
    //
    //     QoS wifi mac helper
    //
    //		- a MSDU aggregator for a particular Access Category (AC)
    //	in order to use 802.11n MSDU aggregation feature;
    //		- block ack parameters like threshold (number of packets
    //	for which block ack mechanism should be used) and inactivity
    //	timeout.
    //		The following code shows an example use of
    //	ns3::QosWifiMacHelper to create an AP with QoS enabled,
    //	aggregation on AC_VO, and Block Ack on AC_BE::
    //
    //	LINK  http://www.nsnam.org/docs/models/html/wifi.html
    //
    //  OBS.:  AC_VO for voice traffic, AC_VI for video traffic,
    // 			AC_BE for best-effort traffic and AC_BK for
    //			background traffic.
    //
    /////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up the QoS wifi mac" << std::endl;
    QosWifiMacHelper qosWifiMacHelper80211A = QosWifiMacHelper::Default();
    qosWifiMacHelper80211A.SetType("ns3::ApWifiMac", "Ssid",
            SsidValue(ssidAp80211A), "BeaconGeneration", BooleanValue(true),
            "BeaconInterval", TimeValue(Seconds(2.5)));
    qosWifiMacHelper80211A.SetMsduAggregatorForAc(AC_VO,
            "ns3::MsduStandardAggregator", "MaxAmsduSize", UintegerValue(3839));
    qosWifiMacHelper80211A.SetBlockAckThresholdForAc(AC_BE, 10);
    qosWifiMacHelper80211A.SetBlockAckInactivityTimeoutForAc(AC_BE, 5);

    //////////////////////////////////////////////////////////////////////////////////////
    //
    // 		Non QoS Wifi Mac Helper
    //
    //	 LINK http://www.nsnam.org/doxygen/classns3_1_1_nqos_wifi_mac_helper.html#details
    //
    /////////////////////////////////////////////////////////////////////////////////////
    NqosWifiMacHelper nqosWifiMacHelper80211G = NqosWifiMacHelper::Default();
    NqosWifiMacHelper accessPointMacHelper80211G = NqosWifiMacHelper::Default();
    nqosWifiMacHelper80211G.SetType("ns3::StaWifiMac", "Ssid",
            SsidValue(ssidAp80211G));
    accessPointMacHelper80211G.SetType("ns3::ApWifiMac", "Ssid",
            SsidValue(ssidAp80211G));

    ////////////////////////////////////////////////////
    //
    //  Yans Wifi Channel Helper
    //
    //   Configure the channel to the wireless hosts and
    // access point.
    //
    //	LINK: http://cutebugs.net/files/wns2-yans.pdf
    //
    ////////////////////////////////////////////////////
    // Creating channel to the wireless connection
    std::cout << "\n-----------> Setting up the Channel to Wlan network"
            << std::endl;
    YansWifiChannelHelper wifiChannelHelper = YansWifiChannelHelper::Default();
    YansWifiPhyHelper accessPointPhyHelper = YansWifiPhyHelper::Default();
    accessPointPhyHelper.SetChannel(wifiChannelHelper.Create());
    // Creating the wifi devices of these stations - to all the nodes (UE/Lte-Wlan)
    // Wlan 802.11A
    NetDeviceContainer ueLteWlanDev80211AContainer;
    // To use Qos
    ueLteWlanDev80211AContainer = wifiHelper80211A.Install(accessPointPhyHelper,
            qosWifiMacHelper80211A, ueLteWlan80211ANodContainer);
    // Wlan 802.11G
    NetDeviceContainer ueLteWlanDev80211GContainer;
    // To use Non-Qos
    ueLteWlanDev80211GContainer = wifiHelper80211G.Install(accessPointPhyHelper,
            nqosWifiMacHelper80211G, ueLteWlan80211GNodContainer);
    // Creating a single AP which shares the same set of PHY-level Attributes as the stations.
    // Access Point 802.11A
    NetDeviceContainer accessPointDevContainer80211A;
    // To use QoS
    accessPointDevContainer80211A = wifiHelper80211A.Install(
            accessPointPhyHelper, qosWifiMacHelper80211A,
            accessPointNode80211A);
    // Access Point 802.11G
    NetDeviceContainer accessPointDevContainer80211G;
    // To use QoS
    accessPointDevContainer80211G = wifiHelper80211G.Install(
            accessPointPhyHelper, accessPointMacHelper80211G,
            accessPointNode80211G);

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //
    //	Creating channels
    //
    //	Point-to-point helper
    //	  Build a set of PointToPointNetDevice objects.
    //	  Normally we eschew multiple inheritance, however, the classes PcapUserHelperForDevice and
    //    AsciiTraceUserHelperForDevice are "mixins".
    //
    //	LINK http://www.nsnam.org/doxygen-release/classns3_1_1_point_to_point_helper.html#details
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up Point-to-Point channel"
            << std::endl;
    PointToPointHelper pppHelper; // Verificar parametros do PointToPoint
    pppHelper.SetDeviceAttribute("DataRate",
            DataRateValue(DataRate("100Mb/s")));
    //pppHelper.SetDeviceAttribute("Mtu", UintegerValue(576));  // http://en.wikipedia.org/wiki/Maximum_transmission_unit
    //pppHelper.SetChannelAttribute("Delay", TimeValue(Seconds(0.01)));

    // Server to PGW - Device 1
    NetDeviceContainer serverToPgwDevice = pppHelper.Install(
            NodeContainer(pgwNode, serverNodContainer));
    // Server to AP 802.11A - Device 3
    NetDeviceContainer serverToAccessPoint80211ADevice = pppHelper.Install(
            NodeContainer(accessPointNode80211A, serverNodContainer));
    // Server to AP 802.11G - Device 2
    NetDeviceContainer serverToAccessPoint80211GDevice = pppHelper.Install(
            NodeContainer(accessPointNode80211G, serverNodContainer));

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //	Ipv4 Interface Container
    //		We've got the "hardware" in place. Now we
    //	need to add IP addresses.
    //
    //		A helper class to make life easier while doing simple IPv4 address assignment in scripts.
    //		This class is a very simple IPv4 address generator. You can think of it as a simple local number incrementer.
    //		It has no notion that IP addresses are part of a global address space. If you have a complicated address
    //		assignment situation you may want to look at the Ipv4AddressGenerator which does recognize that IP address
    //		and network number generation is part of a global problem. Ipv4AddressHelper is a simple class to make simple
    //		problems easy to handle.
    //		We do call into the global address generator to make sure that there are no duplicate addresses generated.
    //
    //	LINK http://www.nsnam.org/doxygen-release/classns3_1_1_ipv4_address_helper.html#details
    //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n-----------> Setting up Ipv4 Interface Container"
            << std::endl;
    Ipv4AddressHelper ipv4Helper;

    // server and Pgw
    ipv4Helper.SetBase("192.168.1.0", "255.255.255.0"); // setting up the IP base, this simulation needs to be Ipv4 because the NS3 have no support to IPv6 yet.
    Ipv4InterfaceContainer serverToPgwIntContainer = ipv4Helper.Assign(
            serverToPgwDevice); // Creating a simple connection between PGW and the server (HA), this method assign set the indicated IP to the nodes.

    // Server and AP 802.11G
    ipv4Helper.SetBase("192.168.2.0", "255.255.255.0");
    Ipv4InterfaceContainer serverToAccessPoint80211GIntContainer =
            ipv4Helper.Assign(serverToAccessPoint80211GDevice);

    // Server and AP 802.11A
    ipv4Helper.SetBase("192.168.3.0", "255.255.255.0");
    Ipv4InterfaceContainer serverToAccessPoint80211AIntContainer =
            ipv4Helper.Assign(serverToAccessPoint80211ADevice);

    // Access Point 802.11A
    ipv4Helper.SetBase("11.0.0.0", "255.0.0.0");
    Ipv4InterfaceContainer accessPoint80211AIntContainer = ipv4Helper.Assign(
            accessPointDevContainer80211A);

    // Ue Lte Wlan (802.11A) --- receive the IP address to connect with the Access Point
    Ipv4InterfaceContainer ueLteWlan80211AIntContainerAp = ipv4Helper.Assign(
            ueLteWlanDev80211AContainer); // LWG - Lte Wlan 802.11A

    // Access Point 802.11G
    ipv4Helper.SetBase("13.0.0.0", "255.0.0.0");
    Ipv4InterfaceContainer accessPoint80211GIntContainer = ipv4Helper.Assign(
            accessPointDevContainer80211G);

    // Ue Lte Wlan (802.11G) --- receive the IP address to connect with the Access Point
    Ipv4InterfaceContainer ueLteWlan80211GIntContainerAp = ipv4Helper.Assign(
            ueLteWlanDev80211GContainer); // LWG - Lte Wlan 802.11G

    // Ue Lte WLan 802.11A -- receive the Ip address to connect with the Lte
    Ipv4InterfaceContainer ueLteWlan80211AIntContainerLte;
    ueLteWlan80211AIntContainerLte = epcHelper->AssignUeIpv4Address(
            ueLteWlanDevContainerLWA);

    // Ue Lte WLan 802.11G -- receive the Ip address to connect with the Lte
    Ipv4InterfaceContainer ueLteWlan80211GIntContainerLte;

    // the EPC of the LTE structure helps to assign the Ipv4 to the UE nodes.
    ueLteWlan80211GIntContainerLte = epcHelper->AssignUeIpv4Address(
            ueLteWlanDevContainerLWG);

    /////////////////////////////////////////////////
    //
    //	Setting static route
    //
    //	Ipv4 Static Routing
    //	 	(*) Static routing protocol IP version
    //	 4 stacks.
    //			This class provides a basic set of
    //	 methods for inserting static unicast and
    //	 multicast routes into the ipv4 routing system.
    //			This particular protocol is designed
    //	 to be inserted into an Ipv4ListRouting
    //	 protocol but can be used also as a standalone
    //	 protocol.
    //
    /////////////////////////////////////////////////
    Ipv4StaticRoutingHelper ipv4StaticRoutingHelper;

    // Server
    std::cout
            << "\n-----------> Setting up the route to the Server access all other networks"
            << std::endl;
    Ptr<Ipv4StaticRouting> serverStaticRouting =
            ipv4StaticRoutingHelper.GetStaticRouting(
            serverNodContainer.Get(0)->GetObject<Ipv4>());
    serverStaticRouting->AddNetworkRouteTo(Ipv4Address("7.0.0.0"),
            Ipv4Mask("255.0.0.0"), 1); // This output is of the PGW to the UE - The Interface is 1 because we created the p2p node connection with the PGW first.
    serverStaticRouting->AddNetworkRouteTo(Ipv4Address("13.0.0.0"),
            Ipv4Mask("255.0.0.0"), 2); // The interface 0 is localhost. We use the 2 to connect to the Access Point 802.11G
    serverStaticRouting->AddNetworkRouteTo(Ipv4Address("11.0.0.0"),
            Ipv4Mask("255.0.0.0"), 3); // The interface 0 is localhost. We use the 3 to connect to the Access Point 802.11A
    //serverStaticRouting->SetDefaultRoute(Ipv4Address("192.168.3.1"), 3);
    //serverStaticRouting->SetDefaultRoute(Ipv4Address("192.168.2.1"), 2);
    //serverStaticRouting->SetDefaultRoute(Ipv4Address("192.168.1.1"), 1);
    // Access Point
    // The access will know the path to the LTE technology by the Server (HA)
    // Wlan 802.11A
    std::cout
            << "\n-----------> Setting up the route to the Access point 802.11A access Lte network and Server/PGW"
            << std::endl;
    Ptr<Ipv4StaticRouting> accessPoint80211AStaticRouting =
            ipv4StaticRoutingHelper.GetStaticRouting(
            accessPointNode80211A->GetObject<Ipv4>());
    // To the Lte technology
    accessPoint80211AStaticRouting->AddNetworkRouteTo(Ipv4Address("7.0.0.0"),
            Ipv4Mask("255.0.0.0"), 2); // Using the second interface because we created the interface to connect with the wireless nodes before.
    // To the PGW
    accessPoint80211AStaticRouting->AddNetworkRouteTo(
            Ipv4Address("192.168.1.0"), Ipv4Mask("255.255.255.0"), 2); // The network between the Server (HA) and the PGW node
    // To the 802.11G
    accessPoint80211AStaticRouting->AddNetworkRouteTo(
            Ipv4Address("192.168.2.0"), Ipv4Mask("255.255.255.0"), 2);
    // Wlan 802.11G
    std::cout
            << "\n-----------> Setting up the route to the Access point 802.11G access Lte network and Server/PGW"
            << std::endl;
    Ptr<Ipv4StaticRouting> accessPoint80211GStaticRouting =
            ipv4StaticRoutingHelper.GetStaticRouting(
            accessPointNode80211G->GetObject<Ipv4>());
    // To the Lte technology
    accessPoint80211GStaticRouting->AddNetworkRouteTo(Ipv4Address("7.0.0.0"),
            Ipv4Mask("255.0.0.0"), 2); // Using the second interface because we created the interface to connect with the wireless nodes before.
    // To the PGW
    accessPoint80211GStaticRouting->AddNetworkRouteTo(
            Ipv4Address("192.168.1.0"), Ipv4Mask("255.255.255.0"), 2); // The network between the Server (HA) and the PGW node
    // To the 802.11A
    accessPoint80211GStaticRouting->AddNetworkRouteTo(
            Ipv4Address("192.168.3.0"), Ipv4Mask("255.255.255.0"), 2);
    // Wlan (802.11A)
    std::cout
            << "\n-----------> Setting up the route to the Hosts LTe WLan 802.11 A access the 802.11G network, Server/PGW and Server/AP802.11G"
            << std::endl;
    for (uint16_t c = 0; c < ueLteWlan80211ANodContainer.GetN(); c++) {
        Ptr<Ipv4StaticRouting> ueLteWlan80211AStaticRouting =
                ipv4StaticRoutingHelper.GetStaticRouting(
                ueLteWlan80211ANodContainer.Get(c)->GetObject<Ipv4>());
        // Default route WLAN
        ueLteWlan80211AStaticRouting->SetDefaultRoute(Ipv4Address("11.0.0.1"),
                2);
        // Default route LTE
        ueLteWlan80211AStaticRouting->SetDefaultRoute(
                epcHelper->GetUeDefaultGatewayAddress(), 1);
        // To the server
        /*
         *  OBS: Eu havia configurado a rota de rede de uma outra maneira e nao funcionou, necessariamente
         *  	deve ser assim. Precisa-se informar o próximo host e a interface de saida para ele,
         *  	caso contrario, não havera resposta, o servidor ficara tentando conexao com o UE e ele nao
         *  	respondera.
         * */
        // Route to Server (HA)
        ueLteWlan80211AStaticRouting->AddNetworkRouteTo(
                Ipv4Address("192.168.2.0"), Ipv4Mask("255.255.255.0"),
                Ipv4Address("11.0.0.1"), 2);
        /*			// Route to Access Point 802.11G
         ueLteWlan80211AStaticRouting->AddNetworkRouteTo(Ipv4Address("192.168.3.0"), // verificar Ip do access point 802.11G
         Ipv4Mask("255.255.255.0"), Ipv4Address("11.0.0.1"), 2);*/
        // Route to Hosts on Access Point 802.11G
        ueLteWlan80211AStaticRouting->AddNetworkRouteTo(Ipv4Address("13.0.0.1"), // verificar Ip do access point 802.11G
                Ipv4Mask("255.0.0.0"), Ipv4Address("11.0.0.1"), 2);
    }
    // Ue Lte Wlan (802.11G)
    std::cout
            << "\n-----------> Setting up the route to the Hosts LTe WLan 802.11 G access the 802.11A network, Server/PGW and Server/AP802.11A"
            << std::endl;
    for (uint16_t c = 0; c < ueLteWlan80211GNodContainer.GetN(); c++) {
        Ptr<Ipv4StaticRouting> ueLteWlan80211GStaticRouting =
                ipv4StaticRoutingHelper.GetStaticRouting(
                ueLteWlan80211GNodContainer.Get(c)->GetObject<Ipv4>());
        // Default route WLAN
        ueLteWlan80211GStaticRouting->SetDefaultRoute(Ipv4Address("13.0.0.1"),
                2);
        // Default route LTE
        ueLteWlan80211GStaticRouting->SetDefaultRoute(
                epcHelper->GetUeDefaultGatewayAddress(), 1);
        // To the server
        /*
         *  OBS: Eu havia configurado a rota de rede de uma outra maneira e nao funcionou, necessariamente
         *  	deve ser assim. Precisa-se informar o próximo host e a interface de saida para ele,
         *  	caso contrario, não havera resposta, o servidor ficara tentando conexao com o UE e ele nao
         *  	respondera.
         * */
        /*
         * 	OBS.: Caminho feito pelo WLAN (2)
         */
        // Route to Server (HA)
        ueLteWlan80211GStaticRouting->AddNetworkRouteTo(
                Ipv4Address("192.168.2.0"), Ipv4Mask("255.255.255.0"),
                Ipv4Address("13.0.0.1"), 2);
        /*          // Route to Access Point 802.11G
         ueLteWlan80211GStaticRouting->AddNetworkRouteTo(Ipv4Address("192.168.2.0"),
         Ipv4Mask("255.255.255.0"), Ipv4Address("13.0.0.1"), 2);*/
        // Route to Hosts on Access Point 802.11A
        ueLteWlan80211GStaticRouting->AddNetworkRouteTo(Ipv4Address("11.0.0.1"),
                Ipv4Mask("255.0.0.0"), Ipv4Address("13.0.0.1"), 2);
        /*
         * 	OBS.: Caminho feito pelo LTE (1)

         // Route to Server (HA)
         ueLteWlan80211GStaticRouting->AddNetworkRouteTo(Ipv4Address("192.168.2.0"),
         Ipv4Mask("255.255.255.0"), epcHelper->GetUeDefaultGatewayAddress(), 1);
         // Route to Access Point 802.11G
         ueLteWlan80211GStaticRouting->AddNetworkRouteTo(Ipv4Address("192.168.2.0"),
         Ipv4Mask("255.255.255.0"), epcHelper->GetUeDefaultGatewayAddress(), 1);
         // Route to Access Point 802.11A
         ueLteWlan80211GStaticRouting->AddNetworkRouteTo(Ipv4Address("192.168.3.0"),
         Ipv4Mask("255.255.255.0"), epcHelper->GetUeDefaultGatewayAddress(), 1);
         // Route to Hosts on Access Point 802.11A
         ueLteWlan80211GStaticRouting->AddNetworkRouteTo(Ipv4Address("11.0.0.1"),
         Ipv4Mask("255.0.0.0"), epcHelper->GetUeDefaultGatewayAddress(), 1);*/
    }
    // and setup ip routing tables to get total ip-level connectivity.
    // Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    ///////////////////////////////////////////////////////
    //
    //	Atach UE to eNB
    //			(*) Attach the UEs to an eNB. THIs will
    //	 configure each UE according to the eNB configuration,
    //	 and create an RRC connection between them.
    //
    //		(0) - left
    //		(1) - right
    //
    //////////////////////////////////////////////////////
    std::cout << "\n-----------> Ataching the UEs to the Enb" << std::endl;
    // ue lte wlan (802.11A)
    std::cout << "==============> 802.11A" << std::endl;
    lteHelper->Attach(ueLteWlanDevContainerLWA, enbDevContainer.Get(0));
    // ue lte wlan (802.11G)
    std::cout << "==============> 802.11G" << std::endl;
    lteHelper->Attach(ueLteWlanDevContainerLWG, enbDevContainer.Get(0));

    /////////////////////////////////////////////////////////////////
    //
    //	EPS Bearer
    //		(*) Active an EPS Bearer including the setup of the
    //  Radio BEarer between an eNB and its attached UE.
    // 		In the current version of the ns-3 LTE model, the
    //	activation of an EPS Bearer will alse active two saturation
    // 	traffic generators for that bearer, one in uplink and
    //  one in downlink.
    //	EPS is effectively a connection-oriented transmission network and, as such,
    //	it requires the establishment of a “virtual” connection between two endpoints
    //	(e.g. a UE and a PDN-GW) before any traffic can be sent between them. In EPS
    //	terminology, this virtual connection is called an “EPS Bearer”; a term that
    //	emphasizes the fact that the virtual connection provides a “bearer service”,
    //  i.e. a transport service with specific QoS attributes. As a concept, the EPS Bearer
    //	corresponds to the “PDP Context” used in GPRS
    //
    //  @link http://3gpp.wikispaces.com/What+is+the+EPS+Bearer%3F
    //
    //  ENUM -----
    //  @link http://www.nsnam.org/doxygen/structns3_1_1_eps_bearer.html#aecf0c67109c5eb4ec0b07226fff5885ea0e7232f1a6148d754be3a3d9e425d452
    //    GBR_CONV_VOICE
    //    GBR_CONV_VIDEO
    //    GBR_GAMING
    //    GBR_NON_CONV_VIDEO
    //    NGBR_IMS
    //    NGBR_VIDEO_TCP_OPERATOR
    //    NGBR_VOICE_VIDEO_GAMING
    //    NGBR_VIDEO_TCP_PREMIUM
    //    NGBR_VIDEO_TCP_DEFAULT
    //
    ////////////////////////////////////////////////////////////////
    Ptr<EpcTft> tft = Create<EpcTft>();
    EpcTft::PacketFilter dlpf;
    dlpf.localPortStart = dlPort;
    dlpf.localPortEnd = dlPort;
    tft->Add(dlpf);
    EpcTft::PacketFilter ulpf;
    ulpf.remotePortStart = ulPort;
    ulpf.remotePortEnd = ulPort;
    tft->Add(ulpf);
    std::cout << "\n-----------> Creating the EPS Bearer - LteWlan 802.11A"
            << std::endl;
    EpsBearer bearerNgbrVideoTcpDefault(EpsBearer::NGBR_VIDEO_TCP_DEFAULT);
    lteHelper->ActivateDedicatedEpsBearer(ueLteWlanDevContainerLWA,
            bearerNgbrVideoTcpDefault, tft); // Verifcar EPC nos nós lteWlan

    std::cout << "\n-----------> Creating the EPS Bearer - LteWlan 802.11G"
            << std::endl;
    EpsBearer bearerGbrConvVoice(EpsBearer::GBR_CONV_VOICE);
    lteHelper->ActivateDedicatedEpsBearer(ueLteWlanDevContainerLWG,
            bearerGbrConvVoice, tft); // Verifcar EPC nos nós lteWlan

    //////////////////////////////////////////
    //
    //	Enabling components of the LTE
    //
    //////////////////////////////////////////
    lteHelper->EnableTraces();

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //
    //	X2 Interface
    //			Used to handover eNB to eNB.
    //
    //  	          ------
    //		          |eNB |
    //		          -----
    //		            |
    //					| ***** X2 interface
    //		            |
    //		          ------
    //		          |eNB |
    //		          ------
    //
    //  The Intra-LTE handover function supports mobility for UEs in LTE_ACTIVE and comprises
    //	the preparation, execution and completion of handover via the X2 and S1 interfaces.
    //  The Inter-3GPP-RAT handover function supports mobility to and from other
    //  3GPP-RATs for UEs in LTE_ACTIVE and comprises the preparation, execution and completion
    //  of handover via the S1 interface.
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////
    lteHelper->AddX2Interface(enbNodContainer);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //		Applications
    //
    //	- The minimum packet size is 12 bytes which is the size of the header carrying the sequence number and the time stamp.
    //    see:  http://www.nsnam.org/docs/release/3.15/doxygen/epc-test-s1u-uplink_8cc_source.html  (code line 136)
    //
    //	- These constants define our upper and our lower bounds. The random numbers will always be between 0 and 3, inclusive.
    //		LOW = 0;
    //		HIGH = 3;
    //			rand() % (HIGH - LOW + 1) + LOW
    //
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    std::cout << "Home agent configurations " << std::endl;
    HomeAgent *homeAgent = new HomeAgent();
    // We need to define the ENB, ACCESSPOINT and UE
    std::cout << "Enb ---------" << std::endl;
    homeAgent->insertEnb(enbNodContainer.Get(0));
    std::cout << "Access Point A" << std::endl;
    homeAgent->insertAccessPoint(accessPointNode80211A);
    //	std::cout << "Access Point G" << std::endl;
    //	homeAgent->insertAccessPoint(accessPointNode80211G);
    std::cout << "Ues configuration" << std::endl;
    for (uint16_t c = 0; c < ueLteWlan80211ANodContainer.GetN(); ++c) {
        homeAgent->insertUserEquipment(ueLteWlan80211ANodContainer.Get(c), 0);
    }
    //	for (uint16_t c = 0; c < ueLteWlan80211GNodContainer.GetN(); ++c) {
    //		homeAgent->insertUserEquipment(ueLteWlan80211GNodContainer.Get(c), 1);
    //	}
    std::cout << "Home agent configuration" << std::endl;
    homeAgent->insertHa(serverNodContainer.Get(0));

    // we will define the app per hour
    std::cout << "App per hour" << std::endl;
    homeAgent->setAppPerHour(10000);
    // define the amount of app
    std::cout << "Number apps" << std::endl;
    homeAgent->setNumberApps(10000);

    //////////////////////////////////////////////////////////////////
    //
    //	Pcap and trace files
    //
    /////////////////////////////////////////////////////////////////
    std::ostringstream filePcap;
    filePcap << "scratch/" << date << "/asciiPcap/" << date << "-" << file;
    accessPointPhyHelper.EnablePcapAll(filePcap.str(), true);
    /*std::ostringstream fileAscii;
     filePcap << "scratch/" << date << "/asciiTrace/" << date << "-" << file << ".tr";
     accessPointPhyHelper.EnableAsciiAll(fileAscii.str());*/

    //////////////////////////////////////////////////////////////////
    //
    //	Flow Monitor
    //
    //		Monitoring the packet traffic.
    //
    /////////////////////////////////////////////////////////////////
    /*		FlowMonitorHelper flowMonitorHelper;
     Ptr<FlowMonitor> monitorPtr;
     monitorPtr = flowMonitorHelper.Install(ueLteWlan80211ANodContainer);
     monitorPtr = flowMonitorHelper.Install(serverNodContainer);
     monitorPtr = flowMonitorHelper.Install(ueLteWlan80211GNodContainer);*/

    //////////////////////////////////////////////////////////////
    //
    // 	Run Simulation
    //
    //	Simulator STOP
    //		(*) This is needed otherwise the simulation will last
    //   forever, because (among others) the start-of-subframe
    // 	 event is scheduled repeatedly, and the ns-3 simulator
    //   scheduler will hence never run out of events.
    //
    //	Simulator Schedule
    //		(*) Execute a defined function.
    //
    //	Simulator Run
    //		(*) Run the simulation.
    //
    //	Simulator Destroy
    //		(*) Cleanup and exit.
    //
    ////////////////////////////////////////////////////////////
    Simulator::Stop(Seconds(120.0));
    //Simulator::Schedule(Seconds(0.001), linkDescription);
    std::cout << "\n\n\nStarting the simulation...\n\n" << std::endl;
    Simulator::Run();

    // Flow configurations
    /*		monitorPtr->CheckForLostPackets();
     Ptr<Ipv4FlowClassifier> classifiedPtr = DynamicCast<Ipv4FlowClassifier>(flowMonitorHelper.GetClassifier());
     std::map<FlowId, FlowMonitor::FlowStats> statsMap = monitorPtr->GetFlowStats();

     for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = statsMap.begin(); i != statsMap.end(); ++i) {
     Ipv4FlowClassifier::FiveTuple t = classifiedPtr->FindFlow(i->first);
     if ((t.destinationAddress == "192.168.2.2") || (t.destinationAddress == "192.168.1.2")) {
     std::cout << "Flow number " << i->first  << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
     std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
     std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
     std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / (i->second.timeLastRxPacket.GetSeconds() - i->second.timeFirstTxPacket.GetSeconds())/1024/1024  << " Mbps\n";
     if (i->second.rxPackets > 0)
     {
     std::cout <<"Delay = " << i->second.delaySum.GetSeconds() / i->second.rxPackets <<"\n";
     }
     }
     }*/
    Simulator::Destroy();
    return 0;
} /// MAIN

