#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/simple-device-energy-model.h"
#include "ns3/li-ion-energy-source.h"
#include "ns3/energy-source-container.h"
#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/energy-module.h"
#include "ns3/random-variable.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/random-variable-stream.h"
#include "ns3/netanim-module.h"
#include "ns3/propagation-loss-model.h"
#include <iostream>
#include <string>
#include "ns3/ptr.h"
#include "ns3/okumura-hata-propagation-loss-model.h"
#include <stdio.h>
#include "ns3/cost231-propagation-loss-model.h"

using namespace ns3;
using namespace std;

//################################### Declarações de objetos e variáveis. ###################################
// Variáveis:
double TotalTime = 10*60.0;
//#################### Number of connections established on the macro and femto cell ####################
int vozMacro = 0, dadosMacro = 0, vozFemto = 0, dadosFemto = 0;
//#######################################################################################################

// Objetos:
NodeContainer usuario;
NodeContainer redeMacro;
NodeContainer redeFemto;
NodeContainer usuariosMacroFemto;
NodeContainer antenasMacroFemto;
Ipv4InterfaceContainer wifiInterfacesFemto;
Ipv4InterfaceContainer wifiInterfacesMacro;
Ipv4InterfaceContainer wifiInterfacesMacroFemto;
Ipv4AddressHelper address;

//###########################################################################################################

Ptr<ExponentialRandomVariable> chegadaChamadaVozMacroExterna;
Ptr<ExponentialRandomVariable> tempoChamadaVozMacroExterna;
Ptr<ExponentialRandomVariable> chegadaChamadaDadosMacroExterna;
Ptr<ExponentialRandomVariable> tempoChamadaDadosMacroExterna;

Ptr<ExponentialRandomVariable> chegadaChamadaVozFemtoExterna;
Ptr<ExponentialRandomVariable> tempoChamadaVozFemtoExterna;
Ptr<ExponentialRandomVariable> chegadaChamadaDadosFemtoExterna;
Ptr<ExponentialRandomVariable> tempoChamadaDadosFemtoExterna;

Ptr<ExponentialRandomVariable> chegadaUsuarioVoz;
Ptr<ExponentialRandomVariable> tempoUsuarioVoz;
Ptr<ExponentialRandomVariable> chegadaUsuarioDados;
Ptr<ExponentialRandomVariable> tempoUsuarioDados;

//Voz

void setVozMacro (int v){
        vozMacro = v;
}

int getVozMacro (){
        return vozMacro;
}

void setVozFemto (int f){
        vozFemto = f;
}

int getVozFemto (){
        return vozFemto;
}

//Dados

void setDadosMacro (int d){
        dadosMacro = d;
}

int getDadosMacro (){
        return dadosMacro;
}

void setDadosFemto (int f){
        dadosFemto = f;
}

int getDadosFemto (){
        return dadosFemto;
}

void RemainingEnergy (double oldValue, double remainingEnergy)
{
        NS_LOG_UNCOND (Simulator::Now ().GetSeconds () << "\t "<< remainingEnergy << "\t" << getVozMacro() << "\t" << getDadosMacro() << "\t"<< getVozFemto() << "\t" << getDadosFemto());
}

void TotalEnergy (double oldValue, double totalEnergy)
{
        NS_LOG_UNCOND (Simulator::Now () << " Total energy consumed by radio = "
        << totalEnergy << "J");
}

void logaMacroFemto (double fim, int mf, int vd){
        if (vd == 0 && mf == 0){
		DataRate taxa("12.2Kbps");
		OnOffHelper onoff ("ns3::UdpSocketFactory", InetSocketAddress(wifiInterfacesMacroFemto.GetAddress (0), 9));
		onoff.SetAttribute("PacketSize", UintegerValue(244));
		onoff.SetAttribute("DataRate",DataRateValue(taxa));
		onoff.SetAttribute("OffTime",StringValue("ns3::ConstantRandomVariable[Constant=0]"));
		ApplicationContainer apps = onoff.Install(usuariosMacroFemto.Get(0));
		apps.Start (Seconds (Simulator::Now().GetSeconds()));
	       	apps.Stop  (Seconds (Simulator::Now().GetSeconds()+ fim));

		PacketSinkHelper sink ("ns3::UdpSocketFactory", InetSocketAddress(wifiInterfacesMacroFemto.GetAddress (0), 9));
		ApplicationContainer apps2 = sink.Install(antenasMacroFemto.Get(0));
		apps2.Start(Seconds(Simulator::Now().GetSeconds()));
	      	apps2.Stop(Seconds(Simulator::Now().GetSeconds()+ fim));
        }
        else if (vd == 1 && mf == 0){
		DataRate taxa("144Kbps");
              	OnOffHelper onoff ("ns3::TcpSocketFactory", InetSocketAddress(wifiInterfacesMacroFemto.GetAddress (0), 9));
		onoff.SetAttribute("PacketSize", UintegerValue(1500));
		onoff.SetAttribute("DataRate",DataRateValue(taxa));
		onoff.SetAttribute("OffTime",StringValue("ns3::ConstantRandomVariable[Constant=0]"));
		ApplicationContainer apps = onoff.Install(usuariosMacroFemto.Get(0));
		apps.Start (Seconds (Simulator::Now().GetSeconds()));
	       	apps.Stop  (Seconds (Simulator::Now().GetSeconds()+ fim));

		PacketSinkHelper sink ("ns3::TcpSocketFactory", InetSocketAddress(wifiInterfacesMacroFemto.GetAddress (0), 9));
		ApplicationContainer apps2 = sink.Install(antenasMacroFemto.Get(0));
		apps2.Start(Seconds(Simulator::Now().GetSeconds()));
	      	apps2.Stop(Seconds(Simulator::Now().GetSeconds()+ fim));
        }
        else if (vd == 0 && mf == 1){
		DataRate taxa("12.2Kbps");
              	OnOffHelper onoff ("ns3::UdpSocketFactory", InetSocketAddress(wifiInterfacesMacroFemto.GetAddress (1), 9));
		onoff.SetAttribute("PacketSize", UintegerValue(244));
		onoff.SetAttribute("DataRate",DataRateValue(taxa));
		onoff.SetAttribute("OffTime",StringValue("ns3::ConstantRandomVariable[Constant=0]"));
		ApplicationContainer apps = onoff.Install(usuariosMacroFemto.Get(1));
		apps.Start (Seconds (Simulator::Now().GetSeconds()));
	       	apps.Stop  (Seconds (Simulator::Now().GetSeconds()+ fim));

		PacketSinkHelper sink ("ns3::UdpSocketFactory", InetSocketAddress(wifiInterfacesMacroFemto.GetAddress (1), 9));
		ApplicationContainer apps2 = sink.Install(antenasMacroFemto.Get(1));
		apps2.Start(Seconds(Simulator::Now().GetSeconds()));
	      	apps2.Stop(Seconds(Simulator::Now().GetSeconds()+ fim));
        }
        else if (vd == 1 && mf == 1){
		DataRate taxa("144Kbps");
              	OnOffHelper onoff ("ns3::TcpSocketFactory", InetSocketAddress(wifiInterfacesMacroFemto.GetAddress (1), 9));
		onoff.SetAttribute("PacketSize", UintegerValue(1500));
		onoff.SetAttribute("DataRate",DataRateValue(taxa));
		onoff.SetAttribute("OffTime",StringValue("ns3::ConstantRandomVariable[Constant=0]"));
		ApplicationContainer apps = onoff.Install(usuariosMacroFemto.Get(1));
		apps.Start (Seconds (Simulator::Now().GetSeconds()));
	       	apps.Stop  (Seconds (Simulator::Now().GetSeconds()+ fim));

		PacketSinkHelper sink ("ns3::TcpSocketFactory", InetSocketAddress(wifiInterfacesMacroFemto.GetAddress (1), 9));
		ApplicationContainer apps2 = sink.Install(antenasMacroFemto.Get(1));
		apps2.Start(Seconds(Simulator::Now().GetSeconds()));
	      	apps2.Stop(Seconds(Simulator::Now().GetSeconds()+ fim));
        }
}

void ChegaUsuarioDados();
void ChegaUsuarioVoz();

void fimChamadaVozMacroExterna()
{
     int n = getVozMacro() - 1;
     setVozMacro(n);

	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);

}

void fimChamadaDadosMacroExterna()
{     int n = getDadosMacro() - 1;
     setDadosMacro(n);

	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);

}

void fimChamadaVozFemtoExterna()
{
     int n = getVozFemto() - 1;
     setVozFemto(n);

	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
}

void fimChamadaDadosFemtoExterna()
{
     int n = getDadosFemto() - 1;
     setDadosFemto(n);

	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
	Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
}

void ChegaChamadaVozMacroExterna(double fim, int mf, int vd)
{
        logaMacroFemto (fim, mf, vd);
        int n = getVozMacro() + 1;        
        setVozMacro(n);
        Simulator::Schedule(Seconds(fim), &fimChamadaVozMacroExterna);
}

void ChegaChamadaDadosMacroExterna(double fim, int mf, int vd)
{
        logaMacroFemto (fim, mf, vd);
        int n = getDadosMacro() + 1;        
        setDadosMacro(n);
        Simulator::Schedule(Seconds(fim), &fimChamadaDadosMacroExterna);
}

void ChegaChamadaVozFemtoExterna(double fim, int mf, int vd)
{
        logaMacroFemto (fim, mf, vd);
        int n = getVozFemto() + 1;        
        setVozFemto(n);
        Simulator::Schedule(Seconds(fim), &fimChamadaVozFemtoExterna);
}
        
void ChegaChamadaDadosFemtoExterna(double fim, int mf, int vd)
{
        logaMacroFemto (fim, mf, vd);
        int n = getDadosFemto() + 1;        
        setDadosFemto(n);
        Simulator::Schedule(Seconds(fim), &fimChamadaDadosFemtoExterna);
}

void ChegaUsuarioVoz();

void ChegaUsuarioDados();

//Tomada de decisão Para voz.

void decisaoVoz (double fim, int vd){
	if (getVozFemto () + getDadosFemto () < 5 && (getVozMacro () + getDadosMacro () == 0 || getVozMacro () + getDadosMacro () != 0 || getVozMacro () + getDadosMacro () < 10)){
                ChegaChamadaVozFemtoExterna (fim, 1, vd);
		//cout << "11\t"<< "Femto\t"<< getVozMacro () + getDadosMacro () << "\t"<< getVozFemto () + getDadosFemto () << endl;
        }
        else if (getVozFemto () + getDadosFemto () == 5 && getVozMacro () + getDadosMacro () == 0 ){
                ChegaChamadaVozMacroExterna (fim, 0, vd);
		//cout << "12\t"<< "Femto\t"<< getVozMacro () + getDadosMacro () << "\t" << getVozFemto () + getDadosFemto () << endl;
        }

        else if(getVozFemto () + getDadosFemto () == 5 && getVozMacro () + getDadosMacro () != 0 && getVozMacro () + getDadosMacro () < 10) {
                ChegaChamadaVozMacroExterna (fim, 0, vd);
		//cout << "13\t"<< "Macro\t"<< getVozMacro () + getDadosMacro ()<< "\t"<< getVozFemto () + getDadosFemto () << endl;
        }

	else if (getVozFemto () + getDadosFemto () == 5 &&getVozMacro () + getDadosMacro () == 10){
		
        }
}

//Tomada de decisão Para dados.

void decisaoDados (double fim, int vd){
	if (getVozFemto () + getDadosFemto () < 5 && (getVozMacro () + getDadosMacro () == 0 || getVozMacro () + getDadosMacro () != 0 || getVozMacro () + getDadosMacro () < 10)){
                ChegaChamadaDadosFemtoExterna (fim, 1, 1);
		//cout << "21\t" << "Femto\t"<< getVozMacro () + getDadosMacro () << "\t" << getVozFemto () + getDadosFemto () << endl;
        }        

        else if (getVozFemto () + getDadosFemto () == 5 && getVozMacro () + getDadosMacro () == 0 ){
                ChegaChamadaDadosMacroExterna (fim, 0, 1);
		//cout << "22\t"<< "Femto\t"<< getVozMacro () + getDadosMacro () << "\t" << getVozFemto () + getDadosFemto () << endl;
        }

	else if(getVozFemto () + getDadosFemto () == 5 && getVozMacro () + getDadosMacro () != 0 && getVozMacro () + getDadosMacro () < 10){
                ChegaChamadaDadosMacroExterna (fim, 1, 1);
		//cout << "23\t"<< "Macro\t"<< getVozMacro () + getDadosMacro () << "\t" << getVozMacro () + getDadosMacro () << endl;
        }

        else if (getVozMacro () + getDadosMacro () == 10){
		
        }
}

// chegadas do usuario - implementar decisao
void ChegaUsuarioVoz()
{ 
        double variavel = chegadaUsuarioVoz -> GetValue();      //Pega valor da variável randomica.
        double tempo = tempoUsuarioVoz -> GetValue();     //Tempo de duração da chamada.
        double proximo = Simulator::Now().GetSeconds() + variavel;      //Instante de chegada da próxima chamada.
	
        decisaoVoz(tempo, 0);        

        if (proximo < TotalTime)
        {
                Simulator::Schedule(Seconds(proximo), &ChegaUsuarioVoz);
        }
}
void ChegaUsuarioDados()
{
        double variavel = chegadaUsuarioDados -> GetValue();
        double tempo = tempoUsuarioDados -> GetValue();
        double proximo =  Simulator::Now().GetSeconds() + variavel;
	
        decisaoDados (tempo, 1);

        if (proximo < TotalTime)
        {
                //std::cout << "Próxima chamada de dados:"<< Simulator::Now().GetSeconds()+ proximo << std::endl;
                Simulator::Schedule(Seconds(proximo), &ChegaUsuarioDados);
        }
}

int main (int argc, char **argv)
{
/*
        chegadaChamadaVozMacroExterna = CreateObject<ExponentialRandomVariable>();
        chegadaChamadaVozMacroExterna -> SetAttribute("Mean", DoubleValue(0.5));
        tempoChamadaVozMacroExterna = CreateObject<ExponentialRandomVariable>();
        tempoChamadaVozMacroExterna -> SetAttribute("Mean", DoubleValue(2));
        chegadaChamadaDadosMacroExterna = CreateObject<ExponentialRandomVariable>();
        chegadaChamadaDadosMacroExterna -> SetAttribute("Mean", DoubleValue(0.1));
        tempoChamadaDadosMacroExterna = CreateObject<ExponentialRandomVariable>();
        tempoChamadaDadosMacroExterna -> SetAttribute("Mean", DoubleValue(0.2));

        chegadaChamadaVozFemtoExterna = CreateObject<ExponentialRandomVariable>();
        chegadaChamadaVozFemtoExterna -> SetAttribute("Mean", DoubleValue(1));
        tempoChamadaVozFemtoExterna = CreateObject<ExponentialRandomVariable>();
        tempoChamadaVozFemtoExterna -> SetAttribute("Mean", DoubleValue(2));
        chegadaChamadaDadosFemtoExterna = CreateObject<ExponentialRandomVariable>();
        chegadaChamadaDadosFemtoExterna -> SetAttribute("Mean", DoubleValue(0.2));
        tempoChamadaDadosFemtoExterna = CreateObject<ExponentialRandomVariable>();
        tempoChamadaDadosFemtoExterna -> SetAttribute("Mean", DoubleValue(0.2));
*/
        chegadaUsuarioVoz = CreateObject<ExponentialRandomVariable>();
        chegadaUsuarioVoz -> SetAttribute("Mean", DoubleValue(0.5));
        tempoUsuarioVoz = CreateObject<ExponentialRandomVariable>();
        tempoUsuarioVoz -> SetAttribute("Mean", DoubleValue(10));

        chegadaUsuarioDados = CreateObject<ExponentialRandomVariable>();
        chegadaUsuarioDados -> SetAttribute("Mean", DoubleValue(0.5));
        tempoUsuarioDados = CreateObject<ExponentialRandomVariable>();
        tempoUsuarioDados -> SetAttribute("Mean", DoubleValue(10));

        LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
        LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);

	usuario.Create(1);

	redeMacro.Create(1);
	redeMacro.Add(usuario.Get(0));

	redeFemto.Create(1);
	redeFemto.Add(usuario.Get(0));

	usuariosMacroFemto.Add(redeMacro.Get(1));
	usuariosMacroFemto.Add(redeFemto.Get(1));

	antenasMacroFemto.Add(redeMacro.Get(0));
	antenasMacroFemto.Add(redeFemto.Get(0));

	Ptr<Cost231PropagationLossModel> modelPropagationMacro = CreateObject<Cost231PropagationLossModel> ();
        modelPropagationMacro->SetAttribute("Frequency",DoubleValue(5.753e+09));
        modelPropagationMacro->SetAttribute("BSAntennaHeight",DoubleValue(30));
        modelPropagationMacro->SetAttribute("SSAntennaHeight",DoubleValue(1));
        modelPropagationMacro->SetAttribute("MinDistance",DoubleValue(2000));

	YansWifiChannelHelper channelM = YansWifiChannelHelper::Default ();
	Ptr< YansWifiChannel > channelMacro = channelM.Create();
        channelMacro->SetPropagationLossModel(modelPropagationMacro);

	Ptr<Cost231PropagationLossModel> modelPropagationFemto = CreateObject<Cost231PropagationLossModel> ();
        modelPropagationFemto->SetAttribute("Frequency",DoubleValue(5.753e+09));
        modelPropagationFemto->SetAttribute("BSAntennaHeight",DoubleValue(2));
        modelPropagationFemto->SetAttribute("SSAntennaHeight",DoubleValue(1));
        modelPropagationFemto->SetAttribute("MinDistance",DoubleValue(20));

	YansWifiChannelHelper channelF = YansWifiChannelHelper::Default ();
        Ptr< YansWifiChannel > channelFemto = channelF.Create();
        channelFemto->SetPropagationLossModel(modelPropagationFemto);

	//Configuring the physical layer.
        YansWifiPhyHelper phyMacro = YansWifiPhyHelper::Default ();
        phyMacro.Set("TxPowerStart", DoubleValue(43));
        phyMacro.Set("TxPowerEnd", DoubleValue(60));
	phyMacro.Set ("TxPowerLevels",UintegerValue (2.0));
        phyMacro.Set ("TxGain",DoubleValue (23.0));
        phyMacro.Set ("RxGain",DoubleValue (23.0));
	phyMacro.SetErrorRateModel ("ns3::NistErrorRateModel");   
        phyMacro.SetChannel (channelMacro);

	//Configuring the physical layer.
        YansWifiPhyHelper phyFemto = YansWifiPhyHelper::Default ();
        phyFemto.Set("TxPowerStart", DoubleValue(-6));
        phyFemto.Set("TxPowerEnd", DoubleValue(24));
	phyFemto.Set ("TxPowerLevels",UintegerValue (2.0));
        phyFemto.Set ("TxGain",DoubleValue (15.0));
        phyFemto.Set ("RxGain",DoubleValue (15.0));
	phyFemto.SetErrorRateModel ("ns3::NistErrorRateModel");   
        phyFemto.SetChannel (channelFemto);
	
	WifiHelper wifi = WifiHelper::Default ();
	wifi.SetStandard (WIFI_PHY_STANDARD_80211b);
        wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

        NqosWifiMacHelper macMacro = NqosWifiMacHelper::Default ();
	NqosWifiMacHelper macFemto = NqosWifiMacHelper::Default ();

        Ssid ssid = Ssid ("ns-3-ssid");
	std::string phyModeMacro ("DsssRate1Mbps");
 	Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode",StringValue (phyModeMacro));
  	wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                     "DataMode",StringValue (phyModeMacro),
                                     "ControlMode",StringValue (phyModeMacro));
        macMacro.SetType ("ns3::AdhocWifiMac");
	macFemto.SetType ("ns3::AdhocWifiMac");

        NetDeviceContainer staDevicesMacro;
        staDevicesMacro = wifi.Install (phyMacro, macMacro, redeMacro);

	std::string phyModeFemto ("DsssRate5_5Mbps");
 	Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode",StringValue (phyModeFemto));
  	wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                     "DataMode",StringValue (phyModeFemto),
                                     "ControlMode",StringValue (phyModeFemto));

	NetDeviceContainer staDevicesFemto;
        staDevicesFemto = wifi.Install (phyFemto, macFemto, redeFemto);

	//############################## Setting the position of the nodes. ##############################
        Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();

        positionAlloc->Add (Vector(0, 0, 0));
        positionAlloc->Add (Vector(0, 800, 0));
	positionAlloc->Add (Vector(10, 800, 0));

        MobilityHelper mobility;
        mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
        mobility.SetPositionAllocator(positionAlloc);

        mobility.Install(redeMacro);
	mobility.Install(redeFemto.Get(0));
        //################################################################################################
	
	// Create and install battery model and device models
	// RV battery model
	RvBatteryModelHelper rvModelHelper;
	// Set alpha & beta values
	rvModelHelper.Set ("RvBatteryModelAlphaValue", DoubleValue (35220));
	rvModelHelper.Set ("RvBatteryModelBetaValue", DoubleValue (0.637));
	// install source
	EnergySourceContainer sources = rvModelHelper.Install (usuario);
	// device energy model
	WifiRadioEnergyModelHelper radioEnergyHelper;
	// set VariableLoadTestIDLE current, which will be the constant load
	radioEnergyHelper.Set ("IdleCurrentA", DoubleValue (0.0156));
	radioEnergyHelper.Set ("TxCurrentA", DoubleValue (3.042253521126761));
	radioEnergyHelper.Set ("RxCurrentA", DoubleValue (3.042253521126761));                
	// install on node
	DeviceEnergyModelContainer deviceModelsMacro = radioEnergyHelper.Install (staDevicesMacro.Get(1), sources);
	radioEnergyHelper.Set ("TxCurrentA", DoubleValue (2.140845070422535));
	radioEnergyHelper.Set ("RxCurrentA", DoubleValue (2.140845070422535));
	DeviceEnergyModelContainer deviceModelsFemto = radioEnergyHelper.Install (staDevicesFemto.Get(1), sources);
	
	//Basic Energy Source.
	Ptr<RvBatteryModel> rvModelPtr = DynamicCast<RvBatteryModel> (sources.Get(0));
	rvModelPtr->TraceConnectWithoutContext ("RvBatteryModelBatteryLevel", MakeCallback (&RemainingEnergy));

	InternetStackHelper stack;
	
        stack.Install (redeMacro);
	stack.Install (redeFemto.Get(0));

        address.SetBase ("10.1.3.0", "255.255.255.0");
        wifiInterfacesMacro = address.Assign (staDevicesMacro);
	address.SetBase ("10.1.4.0", "255.255.255.0");
	wifiInterfacesFemto = address.Assign (staDevicesFemto);

	wifiInterfacesMacroFemto.Add(wifiInterfacesMacro.Get(0));
	wifiInterfacesMacroFemto.Add(wifiInterfacesFemto.Get(0));
	
	Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

	Simulator::Schedule(Seconds(0.0), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(0.0), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);
	Simulator::Schedule(Seconds(chegadaUsuarioVoz -> GetValue()), &ChegaUsuarioVoz);
        Simulator::Schedule(Seconds(chegadaUsuarioDados -> GetValue()), &ChegaUsuarioDados);

	Simulator::Stop (Seconds(TotalTime));
	AnimationInterface anim ("bateria-eventos.xml");
        FlowMonitorHelper flowmon;
        Ptr<FlowMonitor> monitor = flowmon.InstallAll();

        Simulator::Run ();

        monitor->CheckForLostPackets ();
        Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
        std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
        for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
        {
  	        Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
      	        std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
      	        std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
      	        std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
        }
        //Criação de arquivo de saída .xml
        monitor->SerializeToXmlFile("bateria_even_flowmonitor", true, true);
        Simulator::Destroy ();
}
