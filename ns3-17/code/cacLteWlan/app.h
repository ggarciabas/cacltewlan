/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil			(); -*- */
#ifndef APP_H
#define APP_H

#include "ns3/random-variable.h"
#include "ns3/core-module.h"


//using namespace std;
using namespace ns3;

class App {

	public:
		App 															();
		App 															(double cVoz, double cWww, double cFtp, double cVideo, double tVoz, double tWww, double tFtp, double tVideo);
		void setChegadaUsuarioVoz										(double);
		void setChegadaUsuarioWww										(double);
		void setChegadaUsuarioFtp										(double);
		void setChegadaUsuarioVideo										(double);
		void setTempoUsuarioVoz											(double);
		void setTempoUsuarioWww											(double);
		void setTempoUsuarioFtp											(double);
		void setTempoUsuarioVideo										(double);
		//double getValueVoz												();
		//double getValueVideo											();
		//double getValueFtp												();
		//double getValueWww												();
		uint32_t getVozLteUl											();			
		uint32_t getVozLteDl											();
		uint32_t getVozWlanUl											();
		uint32_t getVozWlanDl											();
		uint32_t getVideoLteUl											();
		uint32_t getVideoLteDl											();
		uint32_t getVideoWlanUl											();
		uint32_t getVideoWlanDl											();
		uint32_t getWwwLteUl											();
		uint32_t getWwwLteDl											();
		uint32_t getWwwWlanUl											();
		uint32_t getWwwWlanDl											();
		uint32_t getFtpLteUl											();
		uint32_t getFtpLteDl											();
		uint32_t getFtpWlanUl											();
		uint32_t getFtpWlanDl											();					 
		void setVozLteUl												(uint32_t v);				
		void setVozLteDl												(uint32_t v);
		void setVozWlanUl												(uint32_t v);
		void setVozWlanDl												(uint32_t v);
		void setVideoLteUl												(uint32_t v);
		void setVideoLteDl												(uint32_t v);
		void setVideoWlanUl												(uint32_t v);
		void setVideoWlanDl												(uint32_t v);
		void setWwwLteUl												(uint32_t v);
		void setWwwLteDl												(uint32_t v);
		void setWwwWlanUl												(uint32_t v);
		void setWwwWlanDl												(uint32_t v);
		void setFtpLteUl												(uint32_t v);
		void setFtpLteDl												(uint32_t v);
		void setFtpWlanUl												(uint32_t v);
		void setFtpWlanDl												(uint32_t v);

		Ptr<ExponentialRandomVariable> getChegadaChamadaVozLteUl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaVozLteDl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaVozWlanUl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaVozWlanDl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaVideoLteUl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaVideoLteDl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaVideoWlanUl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaVideoWlanDl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaWwwLteUl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaWwwLteDl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaWwwWlanUl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaWwwWlanDl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaFtpLteUl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaFtpLteDl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaFtpWlanUl		();
		Ptr<ExponentialRandomVariable> getChegadaChamadaFtpWlanDl		();

		void setChegadaChamadaVozLteUl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaVozLteDl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaVozWlanUl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaVozWlanDl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaVideoLteUl								(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaVideoLteDl								(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaVideoWlanUl								(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaVideoWlanDl								(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaWwwLteUl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaWwwLteDl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaWwwWlanUl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaWwwWlanDl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaFtpLteUl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaFtpLteDl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaFtpWlanUl									(Ptr<ExponentialRandomVariable> v);
		void setChegadaChamadaFtpWlanDl									(Ptr<ExponentialRandomVariable> v);

	private:
		Ptr<ExponentialRandomVariable> 		chegadaUsuarioVoz;
		Ptr<ExponentialRandomVariable> 		tempoUsuarioVoz;
		Ptr<ExponentialRandomVariable> 		chegadaUsuarioWww;
		Ptr<ExponentialRandomVariable> 		tempoUsuarioWww;
		Ptr<ExponentialRandomVariable> 		chegadaUsuarioFtp;
		Ptr<ExponentialRandomVariable> 		tempoUsuarioFtp;
		Ptr<ExponentialRandomVariable> 		chegadaUsuarioVideo;
		Ptr<ExponentialRandomVariable> 		tempoUsuarioVideo;
		uint32_t	 						vozLteUl;
		uint32_t 							vozLteDl;
		uint32_t 							vozWlanUl;
		uint32_t 							vozWlanDl;
		uint32_t 							videoLteUl;
		uint32_t 							videoLteDl;
		uint32_t 							videoWlanUl;
		uint32_t 							videoWlanDl;
		uint32_t 							wwwLteUl;
		uint32_t 							wwwLteDl;
		uint32_t 							wwwWlanUl;
		uint32_t 							wwwWlanDl;
		uint32_t 							ftpLteUl;
		uint32_t 							ftpLteDl;
		uint32_t			 				ftpWlanUl;
		uint32_t 							ftpWlanDl;
		// http://www.nsnam.org/doxygen/classns3_1_1_exponential_variable.html
		Ptr<ExponentialRandomVariable> 		chegadaChamadaVozLteUl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaVozLteDl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaVozWlanUl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaVozWlanDl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaVideoLteUl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaVideoLteDl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaVideoWlanUl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaVideoWlanDl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaWwwLteUl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaWwwLteDl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaWwwWlanUl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaWwwWlanDl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaFtpLteUl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaFtpLteDl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaFtpWlanUl;
		Ptr<ExponentialRandomVariable> 		chegadaChamadaFtpWlanDl;

};

App::App () {
	this->chegadaUsuarioVoz = CreateObject<ExponentialRandomVariable>();
	this->tempoUsuarioVoz = CreateObject<ExponentialRandomVariable>();
	this->chegadaUsuarioWww = CreateObject<ExponentialRandomVariable>();
	this->tempoUsuarioWww = CreateObject<ExponentialRandomVariable>();
	this->chegadaUsuarioFtp = CreateObject<ExponentialRandomVariable>();
	this->tempoUsuarioFtp = CreateObject<ExponentialRandomVariable>();
	this->chegadaUsuarioVideo = CreateObject<ExponentialRandomVariable>();
	this->tempoUsuarioVideo = CreateObject<ExponentialRandomVariable>();
	this->vozLteUl = 0;
	this->vozLteDl = 0;
	this->vozWlanUl = 0;
	this->vozWlanDl = 0;
	this->videoLteUl = 0;
	this->videoLteDl = 0;
	this->videoWlanUl = 0;
	this->videoWlanDl = 0;
	this->wwwLteUl = 0;
	this->wwwLteDl = 0;
	this->wwwWlanUl = 0;
	this->wwwWlanDl = 0;
	this->ftpLteUl = 0;
	this->ftpLteDl = 0;
	this->ftpWlanUl = 0;
	this->ftpWlanDl = 0;
}

App::App (double cVoz, double cWww, double cFtp, double cVideo, 
			double tVoz, double tWww, double tFtp, double tVideo) {
	this->chegadaUsuarioVoz = CreateObject<ExponentialRandomVariable>();
	this->tempoUsuarioVoz = CreateObject<ExponentialRandomVariable>();
	this->chegadaUsuarioWww = CreateObject<ExponentialRandomVariable>();
	this->tempoUsuarioWww = CreateObject<ExponentialRandomVariable>();
	this->chegadaUsuarioFtp = CreateObject<ExponentialRandomVariable>();
	this->tempoUsuarioFtp = CreateObject<ExponentialRandomVariable>();
	this->chegadaUsuarioVideo = CreateObject<ExponentialRandomVariable>();
	this->tempoUsuarioVideo = CreateObject<ExponentialRandomVariable>();

	this->chegadaUsuarioVoz -> SetAttribute("Mean", DoubleValue(cVoz));
	this->tempoUsuarioVoz -> SetAttribute("Mean", DoubleValue(tVoz));
	this->chegadaUsuarioWww -> SetAttribute("Mean", DoubleValue(cWww));
	this->tempoUsuarioWww -> SetAttribute("Mean", DoubleValue(tWww));
	this->chegadaUsuarioFtp -> SetAttribute("Mean", DoubleValue(cFtp));
	this->tempoUsuarioFtp -> SetAttribute("Mean", DoubleValue(tFtp));
	this->chegadaUsuarioVideo -> SetAttribute("Mean", DoubleValue(cVideo));
	this->tempoUsuarioVideo -> SetAttribute("Mean", DoubleValue(tVideo));
	this->vozLteUl = 0;
	this->vozLteDl = 0;
	this->vozWlanUl = 0;
	this->vozWlanDl = 0;
	this->videoLteUl = 0;
	this->videoLteDl = 0;
	this->videoWlanUl = 0;
	this->videoWlanDl = 0;
	this->wwwLteUl = 0;
	this->wwwLteDl = 0;
	this->wwwWlanUl = 0;
	this->wwwWlanDl = 0;
	this->ftpLteUl = 0;
	this->ftpLteDl = 0;
	this->ftpWlanUl = 0;
	this->ftpWlanDl = 0;
}

void App::setChegadaUsuarioVoz	(double v) {
	this->chegadaUsuarioVoz -> SetAttribute("Mean", DoubleValue(v));
}

void App::setChegadaUsuarioWww	(double v) {
	this->chegadaUsuarioWww -> SetAttribute("Mean", DoubleValue(v));
}

void App::setChegadaUsuarioFtp	(double v) {
	this->chegadaUsuarioFtp -> SetAttribute("Mean", DoubleValue(v));
}

void App::setChegadaUsuarioVideo  (double v) {
	this->chegadaUsuarioVideo -> SetAttribute("Mean", DoubleValue(v));
}

void App::setTempoUsuarioVoz	(double v) {
	this->chegadaUsuarioVoz -> SetAttribute("Mean", DoubleValue(v));
}

void App::setTempoUsuarioWww	(double v) {
	this->chegadaUsuarioWww -> SetAttribute("Mean", DoubleValue(v));
}

void App::setTempoUsuarioFtp	(double v) {
	this->chegadaUsuarioFtp -> SetAttribute("Mean", DoubleValue(v));
}

void App::setTempoUsuarioVideo  (double v) {
	chegadaUsuarioVideo -> SetAttribute("Mean", DoubleValue(v));
}
/*
double App::getValueVoz				() {
	return this->chamadaUsuarioVoz->GetValue ();
}

double App::getValueVideo				() {
	return this->chamadaUsuarioVideo->GetValue ();
}

double App::getValueFtp				() {
	return this->chamadaUsuarioFtp -> GetValue ();
}

double App::getValueWww				() {
	return this->chamadaUsuarioWww -> GetValue ();
}*/

uint32_t App::getVozLteUl	() {
	return this->vozLteUl;
}	
uint32_t App::getVozLteDl	() {
	return this->vozLteDl;
}
uint32_t App::getVozWlanUl	() {
	return this->vozWlanUl;
}
uint32_t App::getVozWlanDl	() {
	return this->vozWlanDl;
}
uint32_t App::getVideoLteUl () {
	return this->videoLteUl;
}
uint32_t App::getVideoLteDl () {
	return this->videoLteDl;
}
uint32_t App::getVideoWlanUl () {
	return this->videoWlanUl;
}
uint32_t App::getVideoWlanDl () {
	return this->videoWlanDl;
}
uint32_t App::getWwwLteUl () {
	return this->wwwLteUl;
}
uint32_t App::getWwwLteDl () {
	return this->wwwLteDl;
}
uint32_t App::getWwwWlanUl	() {
	return this->wwwWlanUl;
}
uint32_t App::getWwwWlanDl	() {
	return this->wwwWlanDl;
}
uint32_t App::getFtpLteUl () {
	return this->ftpLteUl;
}
uint32_t App::getFtpLteDl () {
	return this->ftpLteDl;
}
uint32_t App::getFtpWlanUl () {
	return this->ftpWlanUl;
}
uint32_t App::getFtpWlanDl	() {
	return this->ftpWlanDl;
}					 
void App::setVozLteUl (uint32_t v) {
	this->vozLteUl = v;
}		
void App::setVozLteDl (uint32_t v) {
	this->vozLteDl = v;
}		
void App::setVozWlanUl (uint32_t v) {
	this->vozWlanUl = v;
}		
void App::setVozWlanDl (uint32_t v) {
	this->vozWlanDl = v;
}		
void App::setVideoLteUl (uint32_t v) {
	this->videoLteUl = v;
}		
void App::setVideoLteDl	(uint32_t v) {
	this->videoLteDl = v;
}		
void App::setVideoWlanUl (uint32_t v) {
	this->videoWlanUl = v;
}		
void App::setVideoWlanDl (uint32_t v) {
	this->videoWlanDl = v;
}		
void App::setWwwLteUl (uint32_t v) {
	this->wwwLteUl = v;
}		
void App::setWwwLteDl (uint32_t v) {
	this->wwwLteDl = v;
}		
void App::setWwwWlanUl (uint32_t v) {
	this->wwwWlanUl = v;
}		
void App::setWwwWlanDl (uint32_t v) {
	this->wwwWlanDl = v;
}		
void App::setFtpLteUl (uint32_t v) {
	this->ftpLteUl = v;
}		
void App::setFtpLteDl (uint32_t v) {
	this->ftpLteDl = v;
}		
void App::setFtpWlanUl (uint32_t v) {
	this->ftpWlanUl = v;
}		
void App::setFtpWlanDl (uint32_t v) {
	this->ftpWlanDl = v;
}	

Ptr<ExponentialRandomVariable> App::getChegadaChamadaVozLteUl		() {
	return this->chegadaChamadaVozLteUl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaVozLteDl		() {
	return this->chegadaChamadaVozLteDl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaVozWlanUl		() {
	return this->chegadaChamadaVozWlanUl;	
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaVozWlanDl		() {
	return this->chegadaChamadaVozWlanDl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaVideoLteUl		() {
	return this->chegadaChamadaVideoLteUl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaVideoLteDl		() {
	return this->chegadaChamadaVideoLteDl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaVideoWlanUl		() {
	return this->chegadaChamadaVideoWlanUl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaVideoWlanDl		() {
	return this->chegadaChamadaVideoWlanDl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaWwwLteUl		() {
	return this->chegadaChamadaWwwLteUl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaWwwLteDl		() {
	return this->chegadaChamadaWwwLteDl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaWwwWlanUl		() {
	return this->chegadaChamadaWwwWlanUl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaWwwWlanDl		() {
	return this->chegadaChamadaWwwWlanDl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaFtpLteUl		() {
	return this->chegadaChamadaFtpLteUl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaFtpLteDl		() {
	return this->chegadaChamadaFtpLteDl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaFtpWlanUl		() {
	return this->chegadaChamadaFtpWlanUl;
}

Ptr<ExponentialRandomVariable> App::getChegadaChamadaFtpWlanDl		() {
	return this->chegadaChamadaFtpWlanDl;
}


void App::setChegadaChamadaVozLteUl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaVozLteUl = v;
}

void App::setChegadaChamadaVozLteDl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaVozLteDl = v;
}

void App::setChegadaChamadaVozWlanUl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaVozWlanUl = v;
}

void App::setChegadaChamadaVozWlanDl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaVozWlanDl = v;
}

void App::setChegadaChamadaVideoLteUl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaVideoLteUl = v;
}

void App::setChegadaChamadaVideoLteDl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaVideoLteDl = v;
}

void App::setChegadaChamadaVideoWlanUl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaVideoWlanUl = v;
}

void App::setChegadaChamadaVideoWlanDl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaVideoWlanDl = v;
}

void App::setChegadaChamadaWwwLteUl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaWwwLteUl = v;
}

void App::setChegadaChamadaWwwLteDl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaWwwLteDl = v;
}

void App::setChegadaChamadaWwwWlanUl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaWwwWlanUl = v;
}

void App::setChegadaChamadaWwwWlanDl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaWwwWlanDl = v;
}

void App::setChegadaChamadaFtpLteUl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaFtpLteUl = v;
}

void App::setChegadaChamadaFtpLteDl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaFtpLteDl = v;
}

void App::setChegadaChamadaFtpWlanUl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaFtpWlanUl = v;
}

void App::setChegadaChamadaFtpWlanDl (Ptr<ExponentialRandomVariable> v) {
	this->chegadaChamadaFtpWlanDl = v;
}


#endif /* APP_H */