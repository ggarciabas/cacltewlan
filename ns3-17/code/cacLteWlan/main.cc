
#include "cacltewlan.h"

int 
	main (int argc, char **argv)
{
	CacLteWlan test;
	if (!test.Configure (argc, argv))
		NS_FATAL_ERROR ("Configuration failed. Aborted.");
	
	test.Run();
	//test.Report (std::cout);
	return 0;
}
