/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * /author Giovanna Garcia
 */

// This struct describe the eNB connected to the HA
typedef struct enbNode {
	// pointer to the eNB
	ns3::Ptr<ns3::Node> m_enb;
	// Maximum bandwidth to downlink bits
	long double m_maxBandwidthDl;
	// Maximum bandwidth to uplink bits
	long double m_maxBandwidthUl;
	// To calculate the available bandwidth we are using
	//		m_maxBandwidthDl - ( (m_amountVoipDl * 25165) + ( (m_amountVideoDl + m_amountWwwDl + m_amountFtpDl) * 134217.728 ) )
	//      m_maxBandwidthUl - ( (m_amountVoipUl * 25165) + ( (m_amountVideoUl + m_amountWwwUl + m_amountFtpUl) * 134217.728 ) )
} ENB;

/*
 *   Now we will no use the WWW ad FTP here,
 *   hereafter we will use to send the packets that was not acceptable to
 *   the WLAN.
 */
