/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011, 2012 CTTC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * /author Giovanna Garcia
 */
#include <string.h>

// This struct describes an application
typedef struct applicationSending {
	// Type of the application
	App m_app;
	// Packet size to the application
	double m_packetSize;
	// Data rate to the application (Mbps)
	std::string m_dataRate;
	// Type of the socket - TCP or UDP
	std::string m_socketFactory;
	// Port of the app
	uint16_t m_port;
	// On time
	std::string m_onTime;
	// Off Time
	std::string m_offTime;
	// Start time to this app
	ns3::Time m_timeStart;
	// Stop time to this app
	ns3::Time m_timeStop;
        // Flag - accepted or not
        bool m_acceptedNot;
        // Uplink = 0 and Downlink = 1
        int m_ulDl;
} APPSENDING;
