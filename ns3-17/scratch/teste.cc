#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/netanim-module.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/netanim-module.h"
#include "ns3/ptr.h"
#include "ns3/cost231-propagation-loss-model.h"

using namespace ns3;
using namespace std;

NS_LOG_COMPONENT_DEFINE ("ThirdScriptExample");

NodeContainer p2pNodes, p2pNodes2, p2pNodes3, p2pNodes4, wifiApNode, wifiApNode2, wifiApNode3;
NetDeviceContainer p2pDevices, p2pDevices2, p2pDevices3, p2pDevices4;
NodeContainer wifiStaNodes, wifiStaNodes2, wifiStaNodes3;
Ipv4InterfaceContainer p2pInterfaces, p2pInterfaces2, p2pInterfaces3, p2pInterfaces4, wifiStaInterfaces, wifiStaInterfaces2, wifiStaInterfaces3;

Ptr<ExponentialRandomVariable> chegadaUsuarioVoz;
Ptr<ExponentialRandomVariable> tempoUsuarioVoz;
Ptr<ExponentialRandomVariable> chegadaUsuarioDados;
Ptr<ExponentialRandomVariable> tempoUsuarioDados;

void logaMacroFemto (double fim, double inicio, int vd, int user, Ipv4InterfaceContainer interfaceAux, NodeContainer nodesAux){
        if (vd == 0){
                DataRate taxa("12.2Kbps");
              	OnOffHelper onoff ("ns3::UdpSocketFactory", InetSocketAddress(interfaceAux.GetAddress (user), 9));
		onoff.SetAttribute("PacketSize", UintegerValue(244));
		onoff.SetAttribute("DataRate",DataRateValue(taxa));
		onoff.SetAttribute("OffTime",StringValue("ns3::ConstantRandomVariable[Constant=0]"));
		ApplicationContainer apps = onoff.Install(p2pNodes.Get(0));
		apps.Start (Seconds(inicio));
	       	apps.Stop  (Seconds(Simulator::Now().GetSeconds()+fim));

		PacketSinkHelper sink ("ns3::UdpSocketFactory", InetSocketAddress(interfaceAux.GetAddress (user), 9));
		ApplicationContainer apps2 = sink.Install(nodesAux.Get(user));
		apps2.Start(Seconds(Simulator::Now().GetSeconds()));
	      	apps2.Stop(Seconds(Simulator::Now().GetSeconds()+fim));
        }
        else if (vd == 1){
                DataRate taxa("144Kbps");
              	OnOffHelper onoff ("ns3::TcpSocketFactory", InetSocketAddress(interfaceAux.GetAddress (user), 9));
		onoff.SetAttribute("PacketSize", UintegerValue(1500));
		onoff.SetAttribute("DataRate",DataRateValue(taxa));
		onoff.SetAttribute("OffTime",StringValue("ns3::ConstantRandomVariable[Constant=0]"));
		ApplicationContainer apps = onoff.Install(p2pNodes.Get(0));
		apps.Start (Seconds(inicio));
	       	apps.Stop  (Seconds(Simulator::Now().GetSeconds()+fim));

		PacketSinkHelper sink ("ns3::TcpSocketFactory", InetSocketAddress(interfaceAux.GetAddress (user), 9));
		ApplicationContainer apps2 = sink.Install(nodesAux.Get(user));
		apps2.Start(Seconds(Simulator::Now().GetSeconds()));
	      	apps2.Stop(Seconds(Simulator::Now().GetSeconds()+fim));

        }
}

int 
main (int argc, char *argv[])
{
  chegadaUsuarioVoz = CreateObject<ExponentialRandomVariable>();
  chegadaUsuarioVoz -> SetAttribute("Mean", DoubleValue(0.5));
  tempoUsuarioVoz = CreateObject<ExponentialRandomVariable>();
  tempoUsuarioVoz -> SetAttribute("Mean", DoubleValue(10));

  chegadaUsuarioDados = CreateObject<ExponentialRandomVariable>();
  chegadaUsuarioDados -> SetAttribute("Mean", DoubleValue(0.5));
  tempoUsuarioDados = CreateObject<ExponentialRandomVariable>();
  tempoUsuarioDados -> SetAttribute("Mean", DoubleValue(10));

  Ptr<Node> server = CreateObject<Node> ();
  Ptr<Node> dns = CreateObject<Node> ();
  Ptr<Node> ap1 = CreateObject<Node> ();
  Ptr<Node> ap2 = CreateObject<Node> ();
  Ptr<Node> ap3 = CreateObject<Node> ();
  Ptr<Node> w11 = CreateObject<Node> ();
  Ptr<Node> w12 = CreateObject<Node> ();
  Ptr<Node> w13 = CreateObject<Node> ();
  Ptr<Node> w14 = CreateObject<Node> ();
  Ptr<Node> w21 = CreateObject<Node> ();
  Ptr<Node> w22 = CreateObject<Node> ();
  Ptr<Node> w23 = CreateObject<Node> ();
  Ptr<Node> w24 = CreateObject<Node> ();
  Ptr<Node> w31 = CreateObject<Node> ();
  Ptr<Node> w32 = CreateObject<Node> ();
  Ptr<Node> w33 = CreateObject<Node> ();
  Ptr<Node> w34 = CreateObject<Node> ();

  p2pNodes = NodeContainer (server, dns);
  p2pNodes2 = NodeContainer (dns, ap1);
  p2pNodes3 = NodeContainer (dns, ap2);
  p2pNodes4 = NodeContainer (dns, ap3);

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("1Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

  p2pDevices = pointToPoint.Install (p2pNodes);
  p2pDevices2 = pointToPoint.Install (p2pNodes2);
  p2pDevices3 = pointToPoint.Install (p2pNodes3);
  p2pDevices4 = pointToPoint.Install (p2pNodes4);

  wifiApNode = p2pNodes2.Get(1);
  wifiApNode2 = p2pNodes3.Get(1);
  wifiApNode3 = p2pNodes4.Get(1);

  wifiStaNodes = NodeContainer(w11, w12, w13, w14);
  wifiStaNodes2 = NodeContainer (w21, w22, w23, w24);
  wifiStaNodes3 = NodeContainer (w31, w32, w33, w34);

  Ptr<Cost231PropagationLossModel> modelPropagationFemto = CreateObject<Cost231PropagationLossModel> ();
  modelPropagationFemto->SetAttribute("Frequency",DoubleValue(5.753e+09));
  modelPropagationFemto->SetAttribute("BSAntennaHeight",DoubleValue(2));
  modelPropagationFemto->SetAttribute("SSAntennaHeight",DoubleValue(1));
  modelPropagationFemto->SetAttribute("MinDistance",DoubleValue(20));

  YansWifiChannelHelper channelF = YansWifiChannelHelper::Default ();
  Ptr< YansWifiChannel > channelFemto = channelF.Create();
  channelFemto->SetPropagationLossModel(modelPropagationFemto);

  YansWifiPhyHelper phyFemto = YansWifiPhyHelper::Default ();
  phyFemto.Set("TxPowerStart", DoubleValue(-6));
  phyFemto.Set("TxPowerEnd", DoubleValue(24));
  phyFemto.Set ("TxPowerLevels",UintegerValue (2.0));
  phyFemto.Set ("TxGain",DoubleValue (15.0));
  phyFemto.Set ("RxGain",DoubleValue (15.0));
  phyFemto.SetErrorRateModel ("ns3::NistErrorRateModel");   
  phyFemto.SetChannel (channelFemto);
	
  WifiHelper wifi = WifiHelper::Default ();
  wifi.SetStandard (WIFI_PHY_STANDARD_80211b);
  wifi.SetRemoteStationManager ("ns3::AarfWifiManager");

  NqosWifiMacHelper mac = NqosWifiMacHelper::Default ();

  std::string phyModeFemto ("DsssRate5_5Mbps");
 	Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode",StringValue (phyModeFemto));
  	wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                     "DataMode",StringValue (phyModeFemto),
                                     "ControlMode",StringValue (phyModeFemto));

  Ssid ssid = Ssid ("ns-3-ssid");
  mac.SetType ("ns3::StaWifiMac",
               "Ssid", SsidValue (ssid),
               "ActiveProbing", BooleanValue (false));

  NetDeviceContainer staDevices;
  staDevices = wifi.Install (phyFemto, mac, wifiStaNodes);
  NetDeviceContainer staDevices2;
  staDevices2 = wifi.Install (phyFemto, mac, wifiStaNodes2);
  NetDeviceContainer staDevices3;
  staDevices3 = wifi.Install (phyFemto, mac, wifiStaNodes3);


  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (ssid));

  NetDeviceContainer apDevices;
  apDevices = wifi.Install (phyFemto, mac, wifiApNode);  
  NetDeviceContainer apDevices2;
  apDevices2 = wifi.Install (phyFemto, mac, wifiApNode2);
  NetDeviceContainer apDevices3;
  apDevices3 = wifi.Install (phyFemto, mac, wifiApNode3);

  //############################## Setting the position of the nodes. ##############################
 Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();

 positionAlloc->Add (Vector(200, 400, 0));
 positionAlloc->Add (Vector(230, 400, 0));
 positionAlloc->Add (Vector(400, 600, 0));
 positionAlloc->Add (Vector(400, 400, 0));
 positionAlloc->Add (Vector(400, 200, 0));
 positionAlloc->Add (Vector(400, 605, 0));
 positionAlloc->Add (Vector(400, 595, 0));
 positionAlloc->Add (Vector(405, 600, 0));
 positionAlloc->Add (Vector(395, 600, 0));
 positionAlloc->Add (Vector(400, 405, 0));
 positionAlloc->Add (Vector(400, 395, 0));
 positionAlloc->Add (Vector(390, 400, 0));
 positionAlloc->Add (Vector(405, 400, 0));
 positionAlloc->Add (Vector(400, 5, 0));
 positionAlloc->Add (Vector(400, -5, 0));
 positionAlloc->Add (Vector(395, 0, 0));
 positionAlloc->Add (Vector(405, 0, 0));

 MobilityHelper mobility;
 mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
 mobility.SetPositionAllocator(positionAlloc);

 mobility.Install(p2pNodes);
 mobility.Install(p2pNodes2.Get(1));
 mobility.Install(p2pNodes3.Get(1));
 mobility.Install(p2pNodes4.Get(1));
 mobility.Install(wifiStaNodes);
 mobility.Install(wifiStaNodes2);
 mobility.Install(wifiStaNodes3);
 

//################################################################################################

  InternetStackHelper stack;
  stack.Install (p2pNodes);
  stack.Install (p2pNodes2.Get(1));
  stack.Install (p2pNodes3.Get(1));
  stack.Install (p2pNodes4.Get(1));
  stack.Install (wifiStaNodes);
  stack.Install (wifiStaNodes2);
  stack.Install (wifiStaNodes3);

  Ipv4AddressHelper address;

  std::cout << "here";

  address.SetBase ("10.1.1.0", "255.255.255.0");
  p2pInterfaces = address.Assign (p2pDevices);

  address.SetBase ("10.1.2.0", "255.255.255.0");
  address.Assign (p2pDevices.Get(1));
  p2pInterfaces2 = address.Assign (p2pDevices2.Get(1));
  p2pInterfaces3 = address.Assign (p2pDevices3.Get(1));
  p2pInterfaces4 = address.Assign (p2pDevices4.Get(1));

  address.SetBase ("10.1.3.0", "255.255.255.0");
  address.Assign (apDevices);
  wifiStaInterfaces = address.Assign (staDevices);

  address.SetBase ("10.1.4.0", "255.255.255.0");
  address.Assign (apDevices2);
  wifiStaInterfaces2 = address.Assign (staDevices2);

  address.SetBase ("10.1.5.0", "255.255.255.0");
  address.Assign (apDevices3);
  wifiStaInterfaces3 = address.Assign (staDevices3);

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  logaMacroFemto (tempoUsuarioVoz -> GetValue(), chegadaUsuarioVoz -> GetValue(), 0, 1, wifiStaInterfaces, wifiStaNodes);
  //logaMacroFemto (tempoUsuarioVoz -> GetValue(), chegadaUsuarioVoz -> GetValue(), 0, 1, wifiStaInterfaces2, wifiStaNodes2);
/*
  Ptr<Ipv4> ipv4Node = p2pNodes3 .Get(0)->GetObject<Ipv4>();
  cout << ipv4Node->GetAddress(2,0).GetLocal() << endl;
*/
  Simulator::Stop (Seconds (10.0));
  AnimationInterface anim ("teste-animation.xml");
  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();
  pointToPoint.EnablePcapAll ("teste");
  
  Simulator::Run ();
  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
  {
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
    std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
    std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
    std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
    }
    //Criação de arquivo de saída .xml
  monitor->SerializeToXmlFile("teste_flowmonitor", true, true);
  Simulator::Destroy ();
  return 0;
}
