/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

///////////////////////////////////////////////////////////////
//
// ENUM Status
//		Determina o STATUS da aplicacao
//			- REQUEST - primeira requisicao
//			- RENEGOTIATION_1 - primeira renegociacao
//			- RENEGOTIATION_2 - segunda renegociacao
//			- REALOCATION - realocacao para a outra interface
//
//////////////////////////////////////////////////////////////

enum AppStatus
{

    REQUEST                     = 0,
    RENEGOTIATION_1             = 1,
    RENEGOTIATION_2             = 2,
    REALOCATION_REQUEST         = 3,
	REALOCATION_RENEGOTIATION_1 = 4,
	REALOCATION_RENEGOTIATION_2 = 5,
    DENIED                      = 6
} ;