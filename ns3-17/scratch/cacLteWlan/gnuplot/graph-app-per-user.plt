set terminal png
set output 'graph-app-per-user.png'
set title 'Calls per user'
set xlabel 'User'
set ylabel 'Application'

set xrange [0:+31]
plot '-'  title 'calls' with linespoints
1 360
2 304
3 327
4 362
5 340
6 326
7 330
8 340
9 306
10 309
11 309
12 313
13 323
14 342
15 331
16 359
17 356
18 349
19 359
20 356
21 304
22 316
23 347
24 339
25 317
26 332
27 332
28 325
29 340
30 347
e
