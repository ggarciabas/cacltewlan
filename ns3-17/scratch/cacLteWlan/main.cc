/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */
#include "cacltewlanpublic.h"

int
main(int argc, char **argv) {
    CacLteWlan test;
    if (!test.Configure(argc, argv))
        NS_FATAL_ERROR("Configuration failed. Aborted.");
		
	cout << "Maximum value for uint32_t: " << numeric_limits<uint32_t>::max() << endl;

    test.Run();
    //test.Report (std::cout);
    return 0;
}


/*
        Error: assert failed. cond="cur->tid != tag.GetInstanceTypeId ()", file=../src/network/model/packet-tag-list.cc, line=139
                terminate called without an active exception
	 
        Solution: http://comments.gmane.org/gmane.network.simulator.ns3.user/18229
 */