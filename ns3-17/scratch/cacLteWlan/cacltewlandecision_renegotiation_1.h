/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 * 
 * Bug: 
 * 		assert failed. cond="tAbsolute.IsPositive ()", file=../src/core/model/default-simulator-impl.cc, line=226
 *                       terminate called without an active exception
 *                       Program received signal SIGABRT, Aborted.
 *                       0x00007fffecb611c9 in raise () from /usr/lib/gcc/x86_64-unknown-linux-gnu/4.8.0/../../../../lib/libc.so.6
 *		---> Erro descoberto: eu estava crindo um Simulator::Schedule(Seconds(Simulator::Now()), ....);
 *							isso fez com que erros fossem gerados. Não é possível executar um Schedule com um tempo que já passou.
 *							 Tentei executar um metodo exatamente no tempo atual, isso não foi possivel, pois, existe um delay para 
 *							as verificacoes de Schedule e execucao, isso fez com que o tempo de execucao do schedule fosse superior
 *							ao início que eu havia programado.
 */
 
#ifndef CACLTEWLANDECISION_RENEGOTIATION_1_H
#    define CACLTEWLANDECISION_RENEGOTIATION_1_H

#    include "cacltewlanheader.h"
#    include "cacltewlanapp.h"

// RENEGOTIATION_1
void
CacLteWlan::renegotiation_1_UserVozUl			(int position)
{

    cout << cRed;
    cout << "Method: renegotiation 1 User Voz Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteUl () +
         this->aplicacao->getVideoLteUl () +
         this->aplicacao->getFtpLteUl () +
         this->aplicacao->getWwwLteUl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbUl
        /* se o remetente ou o destinatario não estiver fazendo uma chamda VOIP*/
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).sender) != (uint32_t)this->appVector.at (position).sender
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).receiver) != (uint32_t) this->appVector.at (position).receiver )
    {

        cout << cYellow;
        cout << "-- RENEGOTIATION 1 Chamada de voz aceita Ul LTE." << endl;
        this->debugInformation << "---- RENEGOTIATION 1 (" << position << endl
				<< "\t\t-- Chamada de voz aceita Ul LTE." << endl
                << "\t\tMethod: renegotiation 1 User Voz Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;

        this->acceptedVoipLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteUl, this, position); // Voz

        int tot = this->aplicacao->getVozLteUl () + this->appVector.at (position).consApp;
        this->aplicacao->setVozLteUl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteUl, this, position);

        // Avisa que os UEs estão enviando aplicacoes VOIP
        this->ueVoipApp[this->appVector.at (position).sender] = this->appVector.at (position).sender;
        this->ueVoipApp[this->appVector.at (position).receiver] = this->appVector.at (position).receiver;

		cout << "F --- RENEGOTIATION 1 Chamada de voz aceita Ul LTE." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 2
        this->appVector.at (position).status = RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
				this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation_2_UserVozUl, this, position);
    }
}

void
CacLteWlan::renegotiation_1_UserVideoUl			(int position)
{
    cout << cRed;
    cout << "Method: renegotiation 1 User Video Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteUl () +
         this->aplicacao->getVideoLteUl () +
         this->aplicacao->getFtpLteUl () +
         this->aplicacao->getWwwLteUl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbUl)
    {
        cout << cYellow;
        cout << "-- RENEGOTIATION 1 Chamada de Video aceita Ul LTE." << endl;
        this->debugInformation << "---- RENEGOTIATION 1 (" << position << endl
				<< "\t\t-- Chamada de Video aceita Ul LTE." << endl
                << "\t\tMethod: renegotiation 1 User Video Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVideoLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteUl, this, position); // Video

        int tot = this->aplicacao->getVideoLteUl () + this->appVector.at (position).consApp;
        this->aplicacao->setVideoLteUl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteUl, this, position);

		cout << "F --- RENEGOTIATION 1 Chamada de Video aceita Ul LTE." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 2
        this->appVector.at (position).status = RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
				this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation_2_UserVideoUl, this, position);
    }
}

void
CacLteWlan::renegotiation_1_UserFtpUl			(int position)
{
    cout << cRed;
    cout << "Method: renegotiation 1 User Ftp Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if (this->appVector.at (position).consApp
        <= (this->averagePacketUl * (this->maxApUl - this->appVector.at (position).consApp)) / this->averageTimeUl)
    {
        cout << cYellow;
        cout << "-- RENEGOTIATION 1 Chamada de Ftp aceita Ul WLAN." << endl;
        this->debugInformation << "---- RENEGOTIATION 1 (" << position << endl
				<< "\t\t-- Chamada de Ftp aceita Ul WLAN." << endl
                << "\t\tMethod: renegotiation 1 User Ftp Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedFtpWlan++;
		this->appBwWlanUl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanUl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanUl->qtAppWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanUl, this, position); // Ftp

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanUl, this, position);

		cout << "F --- RENEGOTIATION 1 Chamada de Ftp aceita Ul WLAN." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 2
        this->appVector.at (position).status = RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
				this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation_2_UserFtpUl, this, position);
    }
}

void
CacLteWlan::renegotiation_1_UserWwwUl			(int position)
{
    cout << cRed;
    cout << "Method: renegotiation 1 User Www Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ( this->appVector.at (position).consApp
        <= (this->averagePacketUl * (this->maxApUl - this->appVector.at (position).consApp)) / this->averageTimeUl)
    {
        cout << cYellow;
        cout << "-- RENEGOTIATION 1 Chamada de Www aceita Ul WLAN." << endl;
        this->debugInformation << "---- RENEGOTIATION 1 (" << position << endl
				<< "\t\t-- Chamada de Www aceita Ul WLAN." << endl
                << "\t\tMethod: renegotiation 1 User Www Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedWwwWlan++;
		this->appBwWlanUl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanUl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanUl->qtAppWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanUl, this, position); // Www

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanUl, this, position);

		cout << "F --- RENEGOTIATION 1 Chamada de Www aceita Ul WLAN." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 2
        this->appVector.at (position).status = RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
				this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation_2_UserWwwUl, this, position);
    }
}

void
CacLteWlan::renegotiation_1_UserVozDl			(int position)
{
    cout << cRed;
    cout << "Method: renegotiation 1 User Voz Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteDl () +
         this->aplicacao->getVideoLteDl () +
         this->aplicacao->getFtpLteDl () +
         this->aplicacao->getWwwLteDl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbDl
        /* se o remetente ou o destinatario não estiver fazendo uma chamda VOIP*/
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).sender) != (uint32_t)this->appVector.at (position).sender
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).receiver) != (uint32_t) this->appVector.at (position).receiver )
    {

        cout << cYellow;
        cout << "-- RENEGOTIATION 1 Chamada de voz aceita Dl LTE." << endl;
        this->debugInformation << "---- RENEGOTIATION 1 (" << position << endl
				<< "\t\t-- Chamada de voz aceita Dl LTE." << endl
                << "\t\tMethod: renegotiation 1 User Voz Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVoipLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteDl, this, position); // Voz

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteDl, this, position);

        // Avisa que os UEs estão enviando aplicacoes VOIP
        this->ueVoipApp[this->appVector.at (position).sender] = this->appVector.at (position).sender;
        this->ueVoipApp[this->appVector.at (position).receiver] = this->appVector.at (position).receiver;

		cout << "F --- RENEGOTIATION 1 Chamada de voz aceita Dl LTE." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 2
        this->appVector.at (position).status = RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
				this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation_2_UserVozDl, this, position);
    }
}

void
CacLteWlan::renegotiation_1_UserVideoDl			(int position)
{
    cout << cRed;
    cout << "Method: renegotiation 1 User Video Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteDl () +
         this->aplicacao->getVideoLteDl () +
         this->aplicacao->getFtpLteDl () +
         this->aplicacao->getWwwLteDl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbDl)
    {
        cout << cYellow;
        cout << "-- RENEGOTIATION 1 Chamada de Video aceita Dl LTE." << endl;
        this->debugInformation << "---- RENEGOTIATION 1 (" << position << endl
				<< "\t\t-- Chamada de Video aceita Dl LTE." << endl
                << "\t\tMethod: renegotiation 1 User Video Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVideoLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteDl, this, position); // Video

        int tot = this->aplicacao->getVideoLteDl () + this->appVector.at (position).consApp;
        this->aplicacao->setVideoLteDl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteDl, this, position);

		cout << "F --- RENEGOTIATION 1 Chamada de Video aceita Dl LTE." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 2
        this->appVector.at (position).status = RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
				this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation_2_UserVideoDl, this, position);
    }
}

void
CacLteWlan::renegotiation_1_UserFtpDl			(int position)
{
    cout << cRed;
    cout << "Method: renegotiation 1 User Ftp Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ( this->appVector.at (position).consApp
        <= (this->averagePacketDl * (this->maxApDl - this->appVector.at (position).consApp)) / this->averageTimeDl)
    {
        cout << cYellow;
        cout << "-- RENEGOTIATION 1 Chamada de Ftp aceita Dl WLAN." << endl;
        this->debugInformation << "---- RENEGOTIATION 1 (" << position << endl
				<< "\t\t-- Chamada de Ftp aceita Dl WLAN." << endl
                << "\t\tMethod: renegotiation 1 User Ftp Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedFtpWlan++;
		this->appBwWlanDl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanDl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanDl->qtAppWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanDl, this, position); // Ftp

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanDl, this, position);

		cout << "F --- RENEGOTIATION 1 Chamada de Ftp aceita Dl WLAN." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 2
        this->appVector.at (position).status = RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
				this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation_2_UserFtpDl, this, position);
    }
}

void
CacLteWlan::renegotiation_1_UserWwwDl			(int position)
{
    cout << cRed;
    cout << "Method: renegotiation 1 User Www Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ( this->appVector.at (position).consApp
        <= (this->averagePacketDl * (this->maxApDl - this->appVector.at (position).consApp)) / this->averageTimeDl)
    {
        cout << cYellow;
        cout << "-- RENEGOTIATION 1 Chamada de Www aceita Dl WLAN." << endl;
        this->debugInformation << "---- RENEGOTIATION 1 (" << position << endl
				<< "\t\t-- Chamada de Www aceita Dl WLAN." << endl
                << "\t\tMethod: renegotiation 1 User Www Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedWwwWlan++;
		this->appBwWlanDl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanDl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanDl->qtAppWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanDl, this, position); // Www

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanDl, this, position);

		cout << "F --- RENEGOTIATION 1 Chamada de Www aceita Dl WLAN." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 2
        this->appVector.at (position).status = RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
				this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
				this->appVector.at (position).consApp = 32*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation_2_UserWwwDl, this, position);
    }
}




// --------- REALOCATION_RENEGOTIATION_1 ********************************************************************************************************



void
CacLteWlan::realocationRenegotiation_1_UserVozUl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_RENEGOTIATION_1 User Voz Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if (this->appVector.at (position).consApp
        <= (this->averagePacketUl * (this->maxApUl - this->appVector.at (position).consApp)) / this->averageTimeUl
        /* se o remetente ou o destinatario não estiver fazendo uma chamda VOIP*/
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).sender) != (uint32_t)this->appVector.at (position).sender
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).receiver) != (uint32_t) this->appVector.at (position).receiver )
    {

        cout << cYellow;
        cout << "-- REALOCATION_RENEGOTIATION_1 Chamada de voz aceita Ul WLAN." << endl;
        this->debugInformation << "---- REALOCATION_RENEGOTIATION_1 (" << position << endl
				<< "\t\t-- Chamada de voz aceita Ul WLAN." << endl
                << "\t\tMethod: relocation User Voz Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVoipWlan++;
		this->handoverLteWlan++;
		this->appBwWlanUl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanUl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanUl->qtAppWlan++;
		
		this->deniedVoipWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanUl, this, position); // Voz

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanUl, this, position);

        // Avisa que os UEs estão enviando aplicacoes VOIP
        this->ueVoipApp[this->appVector.at (position).sender] = this->appVector.at (position).sender;
        this->ueVoipApp[this->appVector.at (position).receiver] = this->appVector.at (position).receiver;

		cout << "F --- REALOCATION_RENEGOTIATION_1 Chamada de voz aceita Ul WLAN." << endl;

    }
    else
    {
		// Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
                this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_2_UserVozUl, this, position);
    }
}

void
CacLteWlan::realocationRenegotiation_1_UserVideoUl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_RENEGOTIATION_1 User Video Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if (this->appVector.at (position).consApp
        <= (this->averagePacketUl * (this->maxApUl - this->appVector.at (position).consApp)) / this->averageTimeUl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_RENEGOTIATION_1 Chamada de Video aceita Ul WLAN." << endl;
        this->debugInformation << "---- REALOCATION_RENEGOTIATION_1 (" << position << endl
				<< "\t\t-- Chamada de Video aceita Ul WLAN." << endl
                << "\t\tMethod: relocation User Video Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVideoWlan++;
		this->handoverLteWlan++;
		this->appBwWlanUl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanUl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanUl->qtAppWlan++;
		
		this->deniedVideoLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanUl, this, position); // Video

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanUl, this, position);

		cout << "F --- REALOCATION_RENEGOTIATION_1 Chamada de Video aceita Ul WLAN." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
                this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_2_UserVideoUl, this, position);
    }
}

void
CacLteWlan::realocationRenegotiation_1_UserFtpUl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_RENEGOTIATION_1 User Ftp Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteUl () +
         this->aplicacao->getVideoLteUl () +
         this->aplicacao->getFtpLteUl () +
         this->aplicacao->getWwwLteUl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbUl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_RENEGOTIATION_1 Chamada de Ftp aceita Ul LTE." << endl;
        this->debugInformation << "---- REALOCATION_RENEGOTIATION_1 (" << position << endl
				<< "\t\t-- Chamada de Ftp aceita Ul LTE." << endl
                << "\t\tMethod: relocation User Ftp Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedFtpLte++;
		this->handoverWlanLte++;
		
		this->deniedFtpWlan++;
		
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteUl, this, position); // Ftp

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteUl, this, position);

		cout << "F --- REALOCATION_RENEGOTIATION_1 Chamada de Ftp aceita Ul LTE." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
                this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_2_UserFtpUl, this, position);
    }
}

void
CacLteWlan::realocationRenegotiation_1_UserWwwUl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_RENEGOTIATION_1 User Www Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteUl () +
         this->aplicacao->getVideoLteUl () +
         this->aplicacao->getFtpLteUl () +
         this->aplicacao->getWwwLteUl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbUl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_RENEGOTIATION_1 Chamada de Www aceita Ul LTE." << endl;
        this->debugInformation << "---- REALOCATION_RENEGOTIATION_1 (" << position << endl
				<< "\t\t-- Chamada de Www aceita Ul LTE." << endl
                << "\t\tMethod: relocation User Www Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedWwwLte++;
		this->handoverWlanLte++;
		
		this->deniedWwwWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteUl, this, position); // Www

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteUl, this, position);

		cout << "F --- REALOCATION_RENEGOTIATION_1 Chamada de Www aceita Ul LTE." << endl;
		
    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
                this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_2_UserWwwUl, this, position);
    }
}

void
CacLteWlan::realocationRenegotiation_1_UserVozDl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_RENEGOTIATION_1 User Voz Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ( this->appVector.at (position).consApp
        <= (this->averagePacketDl * (this->maxApDl - this->appVector.at (position).consApp)) / this->averageTimeDl
        /* se o remetente ou o destinatario não estiver fazendo uma chamada VOIP*/
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).sender) != (uint32_t)this->appVector.at (position).sender
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).receiver) != (uint32_t) this->appVector.at (position).receiver )
    {
        cout << cYellow;
        cout << "-- REALOCATION_RENEGOTIATION_1 Chamada de voz aceita Dl WLAN." << endl;
        this->debugInformation << "---- REALOCATION_RENEGOTIATION_1 (" << position << endl
				<< "\t\t-- Chamada de voz aceita Dl WLAN." << endl
                << "\t\tMethod: relocation User Voz Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVoipWlan++;
		this->handoverLteWlan++;
		this->appBwWlanDl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanDl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanDl->qtAppWlan++;
		
		this->deniedVoipLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanDl, this, position); // Voz

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanDl, this, position);

        // Avisa que os UEs estão enviando aplicacoes VOIP
        this->ueVoipApp[this->appVector.at (position).sender] = this->appVector.at (position).sender;
        this->ueVoipApp[this->appVector.at (position).receiver] = this->appVector.at (position).receiver;

		cout << "F --- REALOCATION_RENEGOTIATION_1 Chamada de voz aceita Dl WLAN." << endl;
		
    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
                this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_2_UserVozDl, this, position);
	}
}

void
CacLteWlan::realocationRenegotiation_1_UserVideoDl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_RENEGOTIATION_1 User Video Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if (this->appVector.at (position).consApp <=
        (this->averagePacketDl * (this->maxApDl - this->appVector.at (position).consApp)) / this->averageTimeDl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_RENEGOTIATION_1 Chamada de Video aceita Dl WLAN." << endl;
        this->debugInformation << "---- REALOCATION_RENEGOTIATION_1 (" << position << endl
				<< "\t\t-- Chamada de Video aceita Dl WLAN." << endl
                << "\t\tMethod: relocation User Video Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVideoWlan++;
		this->handoverLteWlan++;
		this->appBwWlanDl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanDl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanDl->qtAppWlan++;
		
		this->deniedVideoLte++;
		
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanDl, this, position); // Video

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanDl, this, position);
	
		cout << "F --- REALOCATION_RENEGOTIATION_1 Chamada de Video aceita Dl WLAN." << endl;

	}
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
                this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_2_UserVideoDl, this, position);
    }
}

void
CacLteWlan::realocationRenegotiation_1_UserFtpDl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_RENEGOTIATION_1 User Ftp Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteDl () +
         this->aplicacao->getVideoLteDl () +
         this->aplicacao->getFtpLteDl () +
         this->aplicacao->getWwwLteDl () +
		 this->appVector.at (position).consApp)
        <= this->maxEnbDl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_RENEGOTIATION_1 Chamada de Ftp aceita Dl LTE." << endl;
        this->debugInformation << "---- REALOCATION_RENEGOTIATION_1 (" << position << endl
				<< "\t\t-- Chamada de Ftp aceita Dl LTE." << endl
                << "\t\tMethod: relocation User Ftp Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedFtpLte++;
		this->handoverWlanLte++;
		this->deniedFtpWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteDl, this, position); // Ftp

        int tot = this->aplicacao->getFtpLteDl () + this->appVector.at (position).consApp;
        this->aplicacao->setFtpLteDl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteDl, this, position);

		cout << "F --- REALOCATION_RENEGOTIATION_1 Chamada de Ftp aceita Dl LTE." << endl;
    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
                this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_2_UserFtpDl, this, position);
    }
}

void
CacLteWlan::realocationRenegotiation_1_UserWwwDl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_RENEGOTIATION_1 User Www Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteDl () +
         this->aplicacao->getVideoLteDl () +
         this->aplicacao->getFtpLteDl () +
         this->aplicacao->getWwwLteDl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbDl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_RENEGOTIATION_1 Chamada de Www aceita Dl LTE." << endl;
        this->debugInformation << "---- REALOCATION_RENEGOTIATION_1 (" << position << endl
				<< "\t\t-- Chamada de Www aceita Dl LTE." << endl
                << "\t\tMethod: relocation User Www Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedWwwLte++;
		this->handoverWlanLte++;
		this->deniedWwwWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteDl, this, position); // Www

        int tot = this->aplicacao->getWwwLteDl () + this->appVector.at (position).consApp;
        this->aplicacao->setWwwLteDl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteDl, this, position);

		cout << "F ----- REALOCATION_RENEGOTIATION_1 Chamada de Www aceita Dl LTE." << endl;
    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_2;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 2...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "8Kbps";
                this->appVector.at (position).consApp = 8*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "32Kbps";
                this->appVector.at (position).consApp = 32*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_2_UserWwwDl, this, position);

    }
}

# endif // CACLTEWLANDECISION_RENEGOTIATION_1_H