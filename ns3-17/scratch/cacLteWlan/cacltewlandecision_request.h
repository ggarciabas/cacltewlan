/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 * 
 * Bug: 
 * 		assert failed. cond="tAbsolute.IsPositive ()", file=../src/core/model/default-simulator-impl.cc, line=226
 *                       terminate called without an active exception
 *                       Program received signal SIGABRT, Aborted.
 *                       0x00007fffecb611c9 in raise () from /usr/lib/gcc/x86_64-unknown-linux-gnu/4.8.0/../../../../lib/libc.so.6
 *		---> Erro descoberto: eu estava crindo um Simulator::Schedule(Seconds(Simulator::Now()), ....);
 *							isso fez com que erros fossem gerados. Não é possível executar um Schedule com um tempo que já passou.
 *							 Tentei executar um metodo exatamente no tempo atual, isso não foi possivel, pois, existe um delay para 
 *							as verificacoes de Schedule e execucao, isso fez com que o tempo de execucao do schedule fosse superior
 *							ao início que eu havia programado.
 */
#ifndef CACLTEWLANDECISION_REQUEST_H
#    define CACLTEWLANDECISION_REQUEST_H

#    include "cacltewlanheader.h"
#    include "cacltewlanapp.h"


// REQUEST
void
CacLteWlan::requestUserVozUl	(int position)  // Por padrão voz é enviado pelo LTE, porém pode ser solicitado ao WLAN disponibilidade de envio.
{
    cout << cRed;
    cout << "Method: request User Voz Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    // Mais uma chamada sendo analisada!
    this->arrivedCalls++;

    if ((this->aplicacao->getVozLteUl () +
         this->aplicacao->getVideoLteUl () +
         this->aplicacao->getFtpLteUl () +
         this->aplicacao->getWwwLteUl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbUl
        /* se o remetente ou o destinatario não estiver fazendo uma chamda VOIP*/
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).sender) != (uint32_t)this->appVector.at (position).sender
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).receiver) != (uint32_t) this->appVector.at (position).receiver )
    {

        cout << cYellow;
        cout << "-- Chamada de voz aceita Ul LTE." << endl;
        this->debugInformation << "---- REQUEST (" << position << endl
				<< "\t\t-- Chamada de voz aceita Ul LTE." << endl
                << "\t\tMethod: request User Voz Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;

        this->acceptedVoipLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteUl, this, position); // Voz

        int tot = this->aplicacao->getVozLteUl () + this->appVector.at (position).consApp;
        this->aplicacao->setVozLteUl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteUl, this, position);

        // Avisa que os UEs estão enviando aplicacoes VOIP
        this->ueVoipApp[this->appVector.at (position).sender] = this->appVector.at (position).sender;
        this->ueVoipApp[this->appVector.at (position).receiver] = this->appVector.at (position).receiver;

		cout << "F --- Chamada de voz aceita Ul LTE." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Renegociando 1.... " << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
				this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation1UserVozUl, this, position);
    }

    cout << cRed;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::requestUserVozDl	(int position) // Por padrão voz é enviado pelo LTE, porém pode ser solicitado ao WLAN disponibilidade de envio pelo mesmo
{
    cout << cRed;
    cout << "Method: request User Voz Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    // Mais uma chamada sendo analisada!
    this->arrivedCalls++;

    if ((this->aplicacao->getVozLteDl () +
         this->aplicacao->getVideoLteDl () +
         this->aplicacao->getFtpLteDl () +
         this->aplicacao->getWwwLteDl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbDl
        /* se o remetente ou o destinatario não estiver fazendo uma chamda VOIP*/
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).sender) != (uint32_t)this->appVector.at (position).sender
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).receiver) != (uint32_t) this->appVector.at (position).receiver )
    {

        cout << cYellow;
        cout << "-- Chamada de voz aceita Dl LTE." << endl;
        this->debugInformation << "---- REQUEST (" << position << endl
				<< "\t\t-- Chamada de voz aceita Dl LTE." << endl
                << "\t\tMethod: request User Voz Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVoipLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteDl, this, position); // Voz

        int tot = this->aplicacao->getVozLteDl () + this->appVector.at (position).consApp;
        this->aplicacao->setVozLteDl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteDl, this, position);

        // Avisa que os UEs estão enviando aplicacoes VOIP
        this->ueVoipApp[this->appVector.at (position).sender] = this->appVector.at (position).sender;
        this->ueVoipApp[this->appVector.at (position).receiver] = this->appVector.at (position).receiver;

		cout << "F --- Chamada de voz aceita Dl LTE." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
				this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation1UserVozDl, this, position);
    }

    cout << cRed;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::requestUserVideoUl	(int position)
{ // Por padrão video é enviado pelo LTE, porém pode ser solicitado ao WLAN disponibilidade de envio pelo mesmo
    cout << cRed;
    cout << "Method: request User Video Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    // Mais uma chamada sendo analisada!
    this->arrivedCalls++;

    if ((this->aplicacao->getVozLteUl () +
         this->aplicacao->getVideoLteUl () +
         this->aplicacao->getFtpLteUl () +
         this->aplicacao->getWwwLteUl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbUl)
    {
        cout << cYellow;
        cout << "-- Chamada de Video aceita Ul LTE." << endl;
        this->debugInformation << "---- REQUEST (" << position << endl
				<< "\t\t-- Chamada de Video aceita Ul LTE." << endl
                << "\t\tMethod: request User Video Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVideoLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteUl, this, position); // Video

        int tot = this->aplicacao->getVideoLteUl () + this->appVector.at (position).consApp;
        this->aplicacao->setVideoLteUl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteUl, this, position);

		cout << "F --- Chamada de Video aceita Ul LTE." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
				this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation1UserVideoUl, this, position);
    }

    cout << cRed;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::requestUserVideoDl	(int position)
{ // Por padrão video é enviado pelo LTE, porém pode ser solicitado ao WLAN disponibilidade de envio pelo mesmo
    cout << cRed;
    cout << "Method: request User Video Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    // Mais uma chamada sendo analisada!
    this->arrivedCalls++;

    if ((this->aplicacao->getVozLteDl () +
         this->aplicacao->getVideoLteDl () +
         this->aplicacao->getFtpLteDl () +
         this->aplicacao->getWwwLteDl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbDl)
    {
        cout << cYellow;
        cout << "-- Chamada de Video aceita Dl LTE." << endl;
        this->debugInformation << "---- REQUEST (" << position << endl
				<< "\t\t-- Chamada de Video aceita Dl LTE." << endl
                << "\t\tMethod: request User Video Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVideoLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteDl, this, position); // Video

        int tot = this->aplicacao->getVideoLteDl () + this->appVector.at (position).consApp;
        this->aplicacao->setVideoLteDl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteDl, this, position);

		cout << "F --- Chamada de Video aceita Dl LTE." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
				this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation1UserVideoDl, this, position);
    }

    cout << cRed;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::requestUserFtpUl	(int position)
{
    cout << cRed;
    cout << "Method: request User ftp Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    // Mais uma chamada sendo analisada!
    this->arrivedCalls++;

    if (this->appVector.at (position).consApp
        <= (this->averagePacketUl * (this->maxApUl - this->appVector.at (position).consApp)) / this->averageTimeUl)
    {
        cout << cYellow;
        cout << "-- Chamada de Ftp aceita Ul WLAN." << endl;
        this->debugInformation << "---- REQUEST (" << position << endl
				<< "\t\t-- Chamada de Ftp aceita Ul WLAN." << endl
                << "\t\tMethod: request User ftp Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedFtpWlan++;
		this->appBwWlanUl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanUl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanUl->qtAppWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanUl, this, position); // Ftp

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanUl, this, position);

		cout << "F --- Chamada de Ftp aceita Ul WLAN." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
				this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation1UserFtpUl, this, position);
    }

    cout << cRed;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::requestUserFtpDl	(int position)
{
    cout << cRed;
    cout << "Method: request User Ftp Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    // Mais uma chamada sendo analisada!
    this->arrivedCalls++;

    if ( this->appVector.at (position).consApp
			<= (this->averagePacketDl * (this->maxApDl - this->appVector.at (position).consApp)) / this->averageTimeDl)
    {
        cout << cYellow;
        cout << "-- Chamada de Ftp aceita Dl WLAN." << endl;
        this->debugInformation << "---- REQUEST (" << position << endl
				<< "\t\t-- Chamada de Ftp aceita Dl WLAN." << endl
                << "\t\tMethod: request User Ftp Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedFtpWlan++;
		this->appBwWlanDl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanDl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanDl->qtAppWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanDl, this, position); // Ftp

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanDl, this, position);

		cout << "F --- Chamada de Ftp aceita Dl WLAN." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
				this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation1UserFtpDl, this, position);
    }

    cout << cRed;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::requestUserWwwUl	(int position)
{
    cout << cRed;
    cout << "Method: request User Www Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    // Mais uma chamada sendo analisada!
    this->arrivedCalls++;

    if ( this->appVector.at (position).consApp
        <= (this->averagePacketUl * (this->maxApUl - this->appVector.at (position).consApp)) / this->averageTimeUl)
    {
        cout << cYellow;
        cout << "-- Chamada de Www aceita Ul WLAN." << endl;
        this->debugInformation << "---- REQUEST (" << position << endl
				<< "\t\t-- Chamada de Www aceita Ul WLAN." << endl
                << "\t\tMethod: request User Www Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedWwwWlan++;
		this->appBwWlanUl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanUl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanUl->qtAppWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanUl, this, position); // Www

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanUl, this, position);

		cout << "F --- Chamada de Www aceita Ul WLAN." << endl;
		
    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
				this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation1UserWwwUl, this, position);
    }

    cout << cRed;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::requestUserWwwDl	(int position)
{
    cout << cRed;
    cout << "Method: request User Www Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    // Mais uma chamada sendo analisada!
    this->arrivedCalls++;

    if ( this->appVector.at (position).consApp
        <= (this->averagePacketDl * (this->maxApDl - this->appVector.at (position).consApp)) / this->averageTimeDl)
    {
        cout << cYellow;
        cout << "-- Chamada de Www aceita Dl WLAN." << endl;
        this->debugInformation << "---- REQUEST (" << position << endl
				<< "\t\t-- Chamada de Www aceita Dl WLAN." << endl
                << "\t\tMethod: request User Www Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedWwwWlan++;
		this->appBwWlanDl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanDl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanDl->qtAppWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanDl, this, position); // Www

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanDl, this, position);

		cout << "F --- Chamada de Www aceita Dl WLAN." << endl;

    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
				this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
				this->appVector.at (position).consApp = 64*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::renegotiation1UserWwwDl, this, position);
    }


    cout << cRed;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}




// ------------- REALOCATION_REQUEST *******************************************************************************************************************




void
CacLteWlan::realocationRequestUserVozUl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_REQUEST User Voz Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if (this->appVector.at (position).consApp
        <= (this->averagePacketUl * (this->maxApUl - this->appVector.at (position).consApp)) / this->averageTimeUl
        /* se o remetente ou o destinatario não estiver fazendo uma chamda VOIP*/
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).sender) != (uint32_t)this->appVector.at (position).sender
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).receiver) != (uint32_t) this->appVector.at (position).receiver )
    {

        cout << cYellow;
        cout << "-- REALOCATION_REQUEST Chamada de voz aceita Ul WLAN." << endl;
        this->debugInformation << "---- REALOCATION_REQUEST (" << position << endl
				<< "\t\t-- Chamada de voz aceita Ul WLAN." << endl
                << "\t\tMethod: relocation User Voz Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVoipWlan++;
		this->handoverLteWlan++;
		this->appBwWlanUl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanUl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanUl->qtAppWlan++;
		
		this->deniedVoipWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanUl, this, position); // Voz

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanUl, this, position);

        // Avisa que os UEs estão enviando aplicacoes VOIP
        this->ueVoipApp[this->appVector.at (position).sender] = this->appVector.at (position).sender;
        this->ueVoipApp[this->appVector.at (position).receiver] = this->appVector.at (position).receiver;

		cout << "F --- REALOCATION_REQUEST Chamada de voz aceita Ul WLAN." << endl;

    }
    else
    {
		// Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
                this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_1_UserVozUl, this, position);
       
    }
}

void
CacLteWlan::realocationRequestUserVideoUl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_REQUEST User Video Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if (this->appVector.at (position).consApp
        <= (this->averagePacketUl * (this->maxApUl - this->appVector.at (position).consApp)) / this->averageTimeUl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_REQUEST Chamada de Video aceita Ul WLAN." << endl;
        this->debugInformation << "---- REALOCATION_REQUEST (" << position << endl
				<< "\t\t-- Chamada de Video aceita Ul WLAN." << endl
                << "\t\tMethod: relocation User Video Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVideoWlan++;
		this->handoverLteWlan++;
		this->appBwWlanUl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanUl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanUl->qtAppWlan++;
		
		this->deniedVideoLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanUl, this, position); // Video

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanUl, this, position);

		cout << "F --- REALOCATION_REQUEST Chamada de Video aceita Ul WLAN." << endl;

    }
    else
    {
		// Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
                this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
        }
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_1_UserVideoUl, this, position);
    }
}

void
CacLteWlan::realocationRequestUserFtpUl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_REQUEST User Ftp Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteUl () +
         this->aplicacao->getVideoLteUl () +
         this->aplicacao->getFtpLteUl () +
         this->aplicacao->getWwwLteUl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbUl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_REQUEST Chamada de Ftp aceita Ul LTE." << endl;
        this->debugInformation << "---- REALOCATION_REQUEST (" << position << endl
				<< "\t\t-- Chamada de Ftp aceita Ul LTE." << endl
                << "\t\tMethod: relocation User Ftp Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedFtpLte++;
		this->handoverWlanLte++;
		
		this->deniedFtpWlan++;
		
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteUl, this, position); // Ftp

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteUl, this, position);

		cout << "F --- REALOCATION_REQUEST Chamada de Ftp aceita Ul LTE." << endl;

    }
    else
    {
       // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
                this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_1_UserFtpUl, this, position);
    }
}

void
CacLteWlan::realocationRequestUserWwwUl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_REQUEST User Www Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteUl () +
         this->aplicacao->getVideoLteUl () +
         this->aplicacao->getFtpLteUl () +
         this->aplicacao->getWwwLteUl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbUl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_REQUEST Chamada de Www aceita Ul LTE." << endl;
        this->debugInformation << "---- REALOCATION_REQUEST (" << position << endl
				<< "\t\t-- Chamada de Www aceita Ul LTE." << endl
                << "\t\tMethod: relocation User Www Ul: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedWwwLte++;
		this->handoverWlanLte++;
		
		this->deniedWwwWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteUl, this, position); // Www

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteUl, this, position);

		cout << "F --- REALOCATION_REQUEST Chamada de Www aceita Ul LTE." << endl;
		
    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
                this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_1_UserWwwUl, this, position);
    }
}

void
CacLteWlan::realocationRequestUserVozDl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_REQUEST User Voz Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ( this->appVector.at (position).consApp
        <= (this->averagePacketDl * (this->maxApDl - this->appVector.at (position).consApp)) / this->averageTimeDl
        /* se o remetente ou o destinatario não estiver fazendo uma chamada VOIP*/
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).sender) != (uint32_t)this->appVector.at (position).sender
        && (uint32_t) this->ueVoipApp.at (this->appVector.at (position).receiver) != (uint32_t) this->appVector.at (position).receiver )
    {
        cout << cYellow;
        cout << "-- REALOCATION_REQUEST Chamada de voz aceita Dl WLAN." << endl;
        this->debugInformation << "---- REALOCATION_REQUEST (" << position << endl
				<< "\t\t-- Chamada de voz aceita Dl WLAN." << endl
                << "\t\tMethod: relocation User Voz Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVoipWlan++;
		this->handoverLteWlan++;
		this->appBwWlanDl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanDl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanDl->qtAppWlan++;
		
		this->deniedVoipLte++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanDl, this, position); // Voz

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanDl, this, position);

        // Avisa que os UEs estão enviando aplicacoes VOIP
        this->ueVoipApp[this->appVector.at (position).sender] = this->appVector.at (position).sender;
        this->ueVoipApp[this->appVector.at (position).receiver] = this->appVector.at (position).receiver;

		cout << "F --- REALOCATION_REQUEST Chamada de voz aceita Dl WLAN." << endl;
		
    }
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
                this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_1_UserVozDl, this, position);
	}
}

void
CacLteWlan::realocationRequestUserVideoDl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_REQUEST User Video Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if (this->appVector.at (position).consApp <=
        (this->averagePacketDl * (this->maxApDl - this->appVector.at (position).consApp)) / this->averageTimeDl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_REQUEST Chamada de Video aceita Dl WLAN." << endl;
        this->debugInformation << "---- REALOCATION_REQUEST (" << position << endl
				<< "\t\t-- Chamada de Video aceita Dl WLAN." << endl
                << "\t\tMethod: relocation User Video Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedVideoWlan++;
		this->handoverLteWlan++;
		this->appBwWlanDl->totTimeAppWlan += this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
		this->appBwWlanDl->totPackageAppWlan += this->appVector[position].packetSize;
		this->appBwWlanDl->qtAppWlan++;
		
		this->deniedVideoLte++;
		
        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationWlanDl, this, position); // Video

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserWlanDl, this, position);
	
		cout << "F --- REALOCATION_REQUEST Chamada de Video aceita Dl WLAN." << endl;

	}
    else
    {
        // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
                this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_1_UserVideoDl, this, position);
    }
}

void
CacLteWlan::realocationRequestUserFtpDl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_REQUEST User Ftp Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteDl () +
         this->aplicacao->getVideoLteDl () +
         this->aplicacao->getFtpLteDl () +
         this->aplicacao->getWwwLteDl () +
		 this->appVector.at (position).consApp)
        <= this->maxEnbDl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_REQUEST Chamada de Ftp aceita Dl LTE." << endl;
        this->debugInformation << "---- REALOCATION_REQUEST (" << position << endl
				<< "\t\t-- Chamada de Ftp aceita Dl LTE." << endl
                << "\t\tMethod: relocation User Ftp Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedFtpLte++;
		this->handoverWlanLte++;
		this->deniedFtpWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteDl, this, position); // Ftp

        int tot = this->aplicacao->getFtpLteDl () + this->appVector.at (position).consApp;
        this->aplicacao->setFtpLteDl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteDl, this, position);

		cout << "F --- REALOCATION_REQUEST Chamada de Ftp aceita Dl LTE." << endl;
    }
    else
    {
       // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
                this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_1_UserFtpDl, this, position);
    }
}

void
CacLteWlan::realocationRequestUserWwwDl			(int position)
{
    cout << cRed;
    cout << "Method: REALOCATION_REQUEST User Www Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
    cout << cNormal;

    if ((this->aplicacao->getVozLteDl () +
         this->aplicacao->getVideoLteDl () +
         this->aplicacao->getFtpLteDl () +
         this->aplicacao->getWwwLteDl () +
         this->appVector.at (position).consApp)
        <= this->maxEnbDl)
    {
        cout << cYellow;
        cout << "-- REALOCATION_REQUEST Chamada de Www aceita Dl LTE." << endl;
        this->debugInformation << "---- REALOCATION_REQUEST (" << position << endl
				<< "\t\t-- Chamada de Www aceita Dl LTE." << endl
                << "\t\tMethod: relocation User Www Dl: time(" << Simulator::Now ().GetSeconds() << ") position (" << position << ")" << endl;
        cout << cNormal;

        this->acceptedWwwLte++;
		this->handoverWlanLte++;
		this->deniedWwwWlan++;

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds() + 0.0005), &CacLteWlan::createApplicationLteDl, this, position); // Www

        int tot = this->aplicacao->getWwwLteDl () + this->appVector.at (position).consApp;
        this->aplicacao->setWwwLteDl (tot);

        Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + time120), &CacLteWlan::endUserLteDl, this, position);

		cout << "F ----- REALOCATION_REQUEST Chamada de Www aceita Dl LTE." << endl;
    }
    else
    {
       // Tentando realizar renegociacao 1
        this->appVector.at (position).status = REALOCATION_RENEGOTIATION_1;
		this->debugInformation << "---> (" << position << ")  Realocacao Renegociando 1...." << endl;
        switch (this->appVector.at (position).app)
        {
            case 0:
                this->appVector.at (position).dataRate = "12Kbps";
                this->appVector.at (position).consApp = 12*1024;
                break;
            case 1:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 3:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
            case 2:
                this->appVector.at (position).dataRate = "64Kbps";
                this->appVector.at (position).consApp = 64*1024;
                break;
        }
		Simulator::Schedule (Seconds (Simulator::Now ().GetSeconds () + 0.0005), &CacLteWlan::realocationRenegotiation_1_UserWwwDl, this, position);

    }
}

# endif // CACLTEWLANDECISION_REQUEST_H