/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */
#ifndef CACLTEWLANPUBLIC_H
#    define CACLTEWLANPUBLIC_H

#    include "cacltewlanheader.h"
//#include "enumStatus.h"
#    include "gnuplot.h"

/*
// FlowMonitor Prints {	
void
        CacLteWlan::MacTxDrop(Ptr<const Packet> p) 
{
        TypeHeader typeHeader;
        p->PeekHeader(typeHeader);;
	
        NS_LOG_INFO("Packet Drop " << typeHeader.GetId());
        MacTxDropCount++;
}

void 
        CacLteWlan::PhyTxDrop(Ptr<const Packet> p) 
{
        TypeHeader typeHeader;
        p->PeekHeader(typeHeader);
        NS_LOG_INFO("Packet Drop " << typeHeader.GetId());
        PhyTxDropCount++;
}

void
        CacLteWlan::PhyRxDrop(Ptr<const Packet> p)
{
        TypeHeader typeHeader;
        p->PeekHeader(typeHeader);
        NS_LOG_INFO("Packet Drop " << typeHeader.GetId());
        PhyRxDropCount++;
}
// FlowMonitor Prints  }
 */

// Public Methods {

CacLteWlan::CacLteWlan ()
{
    cout << cMagenta;
    cout << "Method: Constructor () " << endl;
    cout << cNormal;

    this->nApNode = 1;
    this->nHaNode = 1;
    this->f = 3;
    this->pcap = false;
    //this->mobile				 (true),
    //this->grid				 (true),
    this->drop = false;
    this->nEnbNode = 1;
    this->nUeNode = 30;
    this->phyMode = "DsssRate11Mbps";
    this->nodeSpeed = 1; // in m/s
    this->nodePause = 2; // in s
    this->maxEnbUl = this->maxEnbDl = 52428800;
    this->maxApUl = this->maxApDl = 26214400;
    this->totApplications = 8000;
    this->totalTime = 3600;
    //widthScenario = 12; 
    //heightScenario = 12;

    this->ueVoipApp.resize (this->nUeNode); // Aloca uma position para cada node

    this->averagePacketUl = 0;
    this->averagePacketDl = 0;
    this->averageTimeUl = 0;
    this->averageTimeDl = 0;

    // Iniciando Usuarios
    this->users = 100;

    // Chamadas chegadas no Servidor para análise de BW no WLAN
    this->arrivedCalls = 0;
	
    // Quantidade de chamadas por tempo de execução; Utilizada para a análise de BW no WLAN
    //this->appPerTime = 0;
	
	// Para cálculo de BW no WLAN
	this->appBwWlanUl->totTimeAppWlan = 0;
	this->appBwWlanUl->totPackageAppWlan = 0;
	this->appBwWlanUl->qtAppWlan = 0;
	this->appBwWlanDl->totTimeAppWlan = 0;
	this->appBwWlanDl->totPackageAppWlan = 0;
	this->appBwWlanDl->qtAppWlan = 0;
	
    this->averageTimeUl = 1;
    this->averageTimeDl = 1;
    this->averagePacketDl = 1;
    this->averagePacketUl = 1;

    this->acceptedVoipLte = this->acceptedVoipWlan = this->acceptedVideoLte = this->acceptedVideoWlan
            = this->acceptedWwwLte = this->acceptedWwwWlan = this->acceptedFtpLte
            = this->acceptedFtpWlan = this->deniedVoipLte = this->deniedVoipWlan = this->deniedVideoLte = this->deniedVideoWlan
            = this->deniedWwwLte = this->deniedWwwWlan = this->deniedFtpLte
            = this->deniedFtpWlan =	this->handoverLteWlan =	this->handoverWlanLte = 0;

    cout << cMagenta;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

bool
CacLteWlan::Configure (int argc, char **argv)
{
    cout << cMagenta;
    cout << "Method: Configure () " << endl;
    cout << cNormal;

    // SeedManager::SetSeed (12345);
    CommandLine cmd;
    cmd.AddValue ("a", "Number of applications.", totApplications);
    //cmd.AddValue ("p",  "Write PCAP traces.", pcap);
    //cmd.AddValue ("m",  "Enable mobility", mobile);
    //cmd.AddValue ("g",  "Enable grid", grid);
    //cmd.AddValue ("n",  "Number of nodes.", nUeNode);
    //cmd.AddValue ("f",  "Number of bizantine faults.", f);
    //cmd.AddValue ("t",  "BftTest time, s.", totalTime);
    //cmd.AddValue ("d",  "Flowmonitor", drop);
    //cmd.AddValue ("usuarios",  "Total de usuários", users);

    cmd.Parse (argc, argv);

    // Instancia Aplicacoes -- APP class
    this->aplicacao = new App (); // Configurando aplicacoes - Tempo e entrada

    cout << cMagenta;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
    return true;
}

void
CacLteWlan::Run ()
{
    cout << cMagenta;
    cout << "Method: Run () " << endl;
    cout << cNormal;

    // Limpando RAND()
    srand (time (0));

    // LogComponentEnable("ReachableBroadcast", LOG_INFO);
    // Config::SetDefault("ns3::TcpL4Protocol::SocketType", ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("2200"));
    /*
     *   Srs Periodicity
     *          m_nUes < 25 = 40
     *          m_nUes < 60 = 80
     *          m_nUes < 120 = 160
     *          m_nUes >= 120 = 320
     * 
     *   http://www.nsnam.org/doxygen-release/test-lte-rrc_8cc_source.html
     */
    Config::SetDefault ("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue (320));
    Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("2200"));
    Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue (phyMode));

    // To Debug
    //LogComponentEnable("TcpL4Protocol", LOG_LEVEL_ALL);
    LogComponentEnable ("PacketSink", LOG_LEVEL_ALL);
    LogComponentEnable ("OnOffApplication",
                        LogLevel (LOG_LEVEL_ALL | LOG_PREFIX_FUNC | LOG_PREFIX_TIME));

    //LogComponentEnable("UdpSocketImpl", LOG_DEBUG);
    //RTS = request to send && CTS = clear to send

    //Packet::EnablePrinting ();
    //Packet::EnableChecking ();
    //PacketMetadata::Enable ();

    // Infromation printing 
    cout << cYellow;
    cout << "Creating: \n\t\t" << this->nUeNode <<
            " User Equipments.\n\t\t" << this->nEnbNode <<
            " Evolved Node B.\n\t\t" << this->nApNode <<
            " Access Point. \n\t\t" << this->nHaNode <<
            " Home Agent.\n";
    cout << cNormal;

    CreateNodes				();
    SetMobility				();
    InstallInternetStack	();
    CreateDevices			();
    AttachUeNode			();
    EnableTraces 			();

    // Dumping interface
    headerDumping ();
    DumpingInterfaces (haNode.Get (0), "HOME AGENT");
    DumpingInterfaces (apNode, "ACCESS POINT");
    for (uint32_t c = 0; c < ueNode.GetN (); ++c)
    {
        DumpingInterfaces (ueNode.Get (c), "USER EQUIPMENT");
        std::cout << cYellow;
        separatorDumping ();
    }

    // Gerenciador de tempo
    this->managerTimeUl.open ("./scratch/cacLteWlan/log/managerTimeUl.txt", ios::app);
    this->managerTimeDl.open ("./scratch/cacLteWlan/log/managerTimeDl.txt", ios::app);
    this->managerPackageUl.open ("./scratch/cacLteWlan/log/managerPackageUl.txt", ios::app);
    this->managerPackageDl.open ("./scratch/cacLteWlan/log/managerPackageDl.txt", ios::app);

    // Open all files -- application writing
    //this->callAcceptedLte.open ("./scratch/cacLteWlan/log/callAcceptedLte.txt", ios::app);
    //this->callAcceptedWlan.open ("./scratch/cacLteWlan/log/callAcceptedWlan.txt", ios::app);
    //this->callDeniedLte.open ("./scratch/cacLteWlan/log/callDeniedLte.txt", ios::app);
    //this->callDeniedWlan.open ("./scratch/cacLteWlan/log/callDeniedWlan.txt", ios::app);

    // Debug
    this->debugInformation.open ("./scratch/cacLteWlan/log/debugInformation.txt", ios::app);
    this->appDesc.open ("./scratch/cacLteWlan/log/appDesc.txt", ios::app);

    // Flow Monitor	
    NodeContainer allWithFlow; // Todos os nos que podem receber flowmonitor
    allWithFlow.Add (this->ueNode);
    allWithFlow.Add (this->haNode);
    allWithFlow.Add (this->apNode);
    this->monitor = this->flowmon.Install (allWithFlow);
    this->flowMonitor.open ("./scratch/cacLteWlan/log/flowMonitor.txt", ios::app);

    Simulator::Schedule (Seconds (0), &CacLteWlan::FlowTraffic, this);

    RunApplications		();

    cout << cRed;
    cout << "Starting simulation for " << this->totalTime << " s ..." << endl;
    cout << cNormal;

    Simulator::Schedule (Seconds (this->totalTime - 1), &CacLteWlan::SerializeFlowMonitor, this);

    Simulator::Stop (Seconds (this->totApplications / (this->appAveragePerUser * this->users) * this->totalTime));
    //AnimationInterface anim ("./scratch/cacLteWlan/log/cacLteWlanAnim.xml");

    Simulator::Run ();
	
	// Graficos -- dados
    this->callAdmLte.open ("./scratch/cacLteWlan/gnuplot/call_admission_lte/call_admission_lte.txt", ios::app);
    this->callAdmWlan.open ("./scratch/cacLteWlan/gnuplot/call_admission_wlan/call_admission_wlan.txt", ios::app);
    this->callHand.open ("./scratch/cacLteWlan/gnuplot/call_handover/call_handover.txt", ios::app);

	
	this->callAdmLte << "<Begin>\n\t <Users>" <<  this->users << 
						"</Users>\n\t<Accepted>\n\t\t<Voip>" << this->acceptedVoipLte / this->totApplications << 
						"</Voip>\n\t\t<Video>" <<  this->acceptedVideoLte / this->totApplications << 
						"</Video>\n\t\t<Www>" << this->acceptedWwwLte / this->totApplications << 
						"</Www>\n\t\t<Ftp>" << this->acceptedFtpLte / this->totApplications << 
						"</Ftp>\n\t</Accepted>\n\t<Denied>\n\t\t<Voip>" << this->deniedVoipLte / this->totApplications << 
						"</Voip>\n\t\t<Video>" << this->deniedVideoLte / this->totApplications <<
						"</Video>\n\t\t<Www>" << this->deniedWwwLte / this->totApplications << 
						"</Www>\n\t\t<Ftp>" << this->deniedFtpLte / this->totApplications <<
						"</Ftp>\n\t</Denied>\n</End>" << endl;
						
    this->callAdmWlan << "<Begin>\n\t <Users>" <<  this->users << 
						 "</Users>\n\t<Accepted>\n\t\t<Voip>" << this->acceptedVoipWlan / this->totApplications << 
						 "</Voip>\n\t\t<Video>" <<  this->acceptedVideoWlan / this->totApplications << 
						 "</Video>\n\t\t<Www>" << this->acceptedWwwWlan / this->totApplications << 
						 "</Www>\n\t\t<Ftp>" << this->acceptedFtpWlan / this->totApplications << 
						 "</Ftp>\n\t</Accepted>\n\t<Denied>\n\t\t<Voip>" << this->deniedVoipWlan / this->totApplications << 
						 "</Voip>\n\t\t<Video>" << this->deniedVideoWlan / this->totApplications <<
						 "</Video>\n\t\t<Www>" << this->deniedWwwWlan / this->totApplications << 
						 "</Www>\n\t\t<Ftp>" << this->deniedFtpWlan / this->totApplications <<
						 "</Ftp>\n\t</Denied>\n</End>" << endl;						
	
    this->callHand << "User: " << this->users
					<< " LTE: " << this->handoverLteWlan
					<< " WLAN: " << this->handoverWlanLte << endl;

    Simulator::Destroy ();

    cout << cMagenta;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}
/*
void
        CacLteWlan::Report (std::ostream &)
{
	
}*/
// Public Methods }

// Private Methods {

void
CacLteWlan::FlowTraffic ()
{
    this->monitor->CheckForLostPackets ();
    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (this->flowmon.GetClassifier ());
    map<FlowId, FlowMonitor::FlowStats> stats = this->monitor->GetFlowStats ();
    cout << cBlue;
    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {
        Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
        this->flowMonitor << "------------------------------------------------------------------------\n";
        cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
        this->flowMonitor << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
        cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
        this->flowMonitor << "  Tx Bytes:   " << i->second.txBytes << "\n";
        cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
        this->flowMonitor << "  Rx Bytes:   " << i->second.rxBytes << "\n";
    }
    cout << cNormal;
}

void
CacLteWlan::SerializeFlowMonitor ()
{
    //Criação de arquivo de saída .xml
    this->monitor->SerializeToXmlFile ("./scratch/cacLteWlan/log/cac_lte_wlan_flowmonitor", true, true);
}

/*
 *  Este método ira criar os nós necessários para a simulação.
 *  This method will create all the nodes to the simulation.
 */
void
CacLteWlan::CreateNodes ()
{
    cout << cMagenta;
    cout << "Method: Create Nodes () " << endl;
    cout << cNormal;

    this->lteHelper = CreateObject<LteHelper>(); // LTE
    cout << cYellow;
    cout << "--- criado lteHelper" << endl;
    cout << cNormal;
    this->lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");
    // lteHelper->setPathlossModelType("ns3::Cost231PropagationLossModel");
    this->lteHelper->SetAttribute ("PathlossModel",
                                   StringValue ("ns3::FriisPropagationLossModel"));

    this->epcHelper = CreateObject<EpcHelper>(); // EPC
    cout << cYellow;
    cout << "--- criado epcHelper" << endl;
    cout << cNormal;

    this->lteHelper->SetEpcHelper (this->epcHelper);
    this->pgwNode = epcHelper->GetPgwNode (); // PGW
    cout << cYellow;
    cout << "--- criado pgwNode" << endl;
    cout << cNormal;

    this->enbNode.Create (1); // Evolved Node B
    cout << cYellow;
    cout << "--- criado enbNode" << endl;
    cout << cNormal;

    this->ueNode.Create (this->nUeNode);
    cout << cYellow;
    cout << "--- criado ueNode" << endl;
    cout << cNormal;

    this->haNode.Create (1); // Home Agent
    cout << cYellow;
    cout << "--- criado heNode" << endl;
    cout << cNormal;

    this->apNode = CreateObject<Node>(); // Access Point node
    cout << cYellow;
    cout << "--- criado apNode" << endl;
    cout << cNormal;

    cout << cMagenta;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

/*
 *  Este método irá configurar a mobilidade dos nós.
 * 	This method will configure the nodes' mobility.
 */
void
CacLteWlan::SetMobility ()
{
    cout << cMagenta;
    cout << "Method: SetMobility () " << endl;
    cout << cNormal;

    MobilityHelper mobilityHelper;

    Ptr<ListPositionAllocator> pgwPosition =
            CreateObject<ListPositionAllocator>();
    pgwPosition->Add (Vector (1768, 1200, 0));
    mobilityHelper.SetPositionAllocator (pgwPosition);
    mobilityHelper.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install (this->pgwNode); // Packet Data Network Gateway
    cout << cYellow;
    cout << "--- mobilidade para pgwNode" << endl;
    cout << cNormal;


    Ptr<ListPositionAllocator> enblPosition =
            CreateObject<ListPositionAllocator>();
    enblPosition->Add (Vector (1100, 1346, 0));
    mobilityHelper.SetPositionAllocator (enblPosition);
    mobilityHelper.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install (this->enbNode.Get (0)); // Enhanced node B
    cout << cYellow;
    cout << "--- mobilidade para ebnNode" << endl;
    cout << cNormal;

    Ptr<ListPositionAllocator> serverPosition = CreateObject<
            ListPositionAllocator>();
    serverPosition->Add (Vector (1768, 1944, 0));
    mobilityHelper.SetPositionAllocator (serverPosition);
    mobilityHelper.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install (this->haNode); // HA Node
    cout << cYellow;
    cout << "--- mobilidade para haNode" << endl;
    cout << cNormal;

    mobilityHelper.SetPositionAllocator ("ns3::RandomDiscPositionAllocator", "X",
                                         StringValue ("1344.0"), "Y", StringValue ("1530.0"), "Rho",
                                         StringValue ("ns3::UniformRandomVariable[Min=1|Max=106.066]"));
    mobilityHelper.SetMobilityModel ("ns3::RandomWalk2dMobilityModel", "Mode",
                                     StringValue ("Time"), "Time", StringValue ("0.05s"), "Speed",
                                     StringValue ("ns3::ConstantRandomVariable[Constant=25.0]"), "Bounds",
                                     StringValue ("1238|1450|1424|1636"));
    mobilityHelper.Install (this->ueNode); // User Equipment Lte Wlan
    cout << cYellow;
    cout << "--- mobilidade para ueNode" << endl;
    cout << cNormal;

    Ptr<ListPositionAllocator> accessPointPosition = CreateObject<
            ListPositionAllocator>();
    accessPointPosition->Add (Vector (1344, 1530, 0));
    mobilityHelper.SetPositionAllocator (accessPointPosition);
    mobilityHelper.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobilityHelper.Install (this->apNode); // Access point
    cout << cYellow;
    cout << "--- mobilidade para apNode" << endl;
    cout << cNormal;

    cout << cMagenta;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

/*
 *  Este método irá configurar Internet Stack.
 * 	This method will configure the Internet Stack.
 */
void
CacLteWlan::InstallInternetStack ()
{
    cout << cMagenta;
    cout << "Method: Install Internet Stack () " << endl;
    cout << cNormal;

    InternetStackHelper internetStackHelper;
    internetStackHelper.Install (this->haNode);
    cout << cYellow;
    cout << "--- stack para haNode" << endl;
    cout << cNormal;
    internetStackHelper.Install (this->apNode);
    cout << cYellow;
    cout << "--- stack para apNode" << endl;
    cout << cNormal;
    internetStackHelper.Install (this->ueNode);
    cout << cYellow;
    cout << "--- stack para ueNode" << endl;
    cout << cNormal;

    cout << cMagenta;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::CreateDevices ()
{
    cout << cMagenta;
    cout << "Method: SetMobility () " << endl;
    cout << cNormal;

    // Lte {
    this->enbDevice.Add (this->lteHelper->InstallEnbDevice (this->enbNode)); // eNB
    cout << cYellow;
    cout << "--- criado dispositivo enbDevice" << endl;
    cout << cNormal;
    this->ueDeviceLte.Add (this->lteHelper->InstallUeDevice (this->ueNode)); // UE
    cout << cYellow;
    cout << "--- criado dispositivo ueDeviceLte" << endl;
    cout << cNormal;
    // Lte }

    // Wifi {
    /*
            RxGain (Receive Gain) is used to raise or lower the gain (volume) of signals that come in on the IP interface.
            RTS/CTS (Request to Send / Clear to Send)
            RTS/CTS packet size threshold is 0–2347 octets. Typically, sending RTS/CTS frames does not occur unless the packet 
            size exceeds this threshold. If the packet size that the node wants to transmit is larger than the threshold, the RTS/CTS handshake gets 				triggered. Otherwise, the data frame gets sent immediately.
            The Friis transmission equation is used in telecommunications engineering, and gives the power received by one antenna under idealized 				conditions given another antenna some distance away transmitting a known amount of power.
     */

    WifiHelper wifi = WifiHelper::Default ();
    wifi.SetStandard (WIFI_PHY_STANDARD_80211g);
    wifi.SetRemoteStationManager ("ns3::ArfWifiManager");

    Ssid ssidAp = Ssid ("WifiLocal");

    //PHY Layer
    //YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
    // This is one parameter that matters when using FixedRssLossModel set it to zero; otherwise, gain will be added
    //wifiPhy.Set ("RxGain", DoubleValue (0) ); 
    //wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO); 

    YansWifiChannelHelper wifiChannelHelper = YansWifiChannelHelper::Default ();
    YansWifiPhyHelper accessPointPhyHelper = YansWifiPhyHelper::Default ();
    accessPointPhyHelper.SetChannel (wifiChannelHelper.Create ());

    //MAC Layer
    NqosWifiMacHelper nqosWifiMacHelper = NqosWifiMacHelper::Default ();
    NqosWifiMacHelper accessPointMacHelper = NqosWifiMacHelper::Default ();
    nqosWifiMacHelper.SetType ("ns3::StaWifiMac", "Ssid", SsidValue (ssidAp));
    accessPointMacHelper.SetType ("ns3::ApWifiMac", "Ssid", SsidValue (ssidAp));

    this->ueDeviceWlan.Add (wifi.Install (accessPointPhyHelper, nqosWifiMacHelper, this->ueNode));
    this->apDevice.Add (wifi.Install (accessPointPhyHelper, nqosWifiMacHelper, this->apNode));

    // Wifi }

    // P2P {

    PointToPointHelper pppHelper;
    pppHelper.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("1000Mb/s")));

    // Server to PGW 
    NetDeviceContainer haToPgw = pppHelper.Install (this->pgwNode, this->haNode.Get (0));
    this->haDevice.Add (haToPgw.Get (1)); // Verificar metodo Install point-to-point-helper.cc.
    cout << cYellow;
    cout << "--- criado dispositivo haDevice - pgw" << endl;
    cout << cNormal;
    // Server to AP 
    NetDeviceContainer haToAp = pppHelper.Install (this->apNode, this->haNode.Get (0));
    this->haDevice.Add (haToAp.Get (1)); // Verificar metodo Install point-to-point-helper.cc.
    cout << cYellow;
    cout << "--- criado dispositivo haDevice - ap" << endl;
    cout << cNormal;
    this->apDevice.Add (haToAp.Get (0));
    cout << cYellow;
    cout << "--- criado dispositivo apDevice - ha" << endl;
    cout << cNormal;

    // P2P }

    // Ips {
    Ipv4AddressHelper ipv4Helper;

    // server and Pgw
    ipv4Helper.SetBase ("192.168.1.0", "255.255.255.0"); // setting up the IP base, this simulation needs to be Ipv4 because the NS3 have no support to IPv6 yet.
    Ipv4InterfaceContainer haToPgwInterface = ipv4Helper.Assign (haToPgw); // Creating a simple connection between PGW and the server (HA), this method assign set the indicated IP to the nodes.
    this->haInterface.Add (haToPgwInterface.Get (1));
    cout << cYellow;
    cout << "--- criado interface haInterface - pgw" << endl;
    cout << cNormal;

    // Server and AP
    ipv4Helper.SetBase ("192.168.2.0", "255.255.255.0");
    Ipv4InterfaceContainer haToApInterface = ipv4Helper.Assign (haToAp);
    this->haInterface.Add (haToApInterface.Get (1));
    cout << cYellow;
    cout << "--- criado interface haInterface - ap" << endl;
    cout << cNormal;
    this->apInterface.Add (haToApInterface.Get (0));
    cout << cYellow;
    cout << "--- criado interface apInterface - ha" << endl;
    cout << cNormal;

    // Access Point
    ipv4Helper.SetBase ("11.0.0.0", "255.0.0.0");
    this->apInterface.Add (ipv4Helper.Assign (this->apDevice));
    cout << cYellow;
    cout << "--- criado interface apInterface - wifi" << endl;
    cout << cNormal;

    // Ue Wifi
    this->ueInterface.Add (ipv4Helper.Assign (this->ueDeviceWlan));
    cout << cYellow;
    cout << "--- criado interface ueInterface - ap" << endl;
    cout << cNormal;

    // Ue Lte 
    this->ueInterface.Add (this->epcHelper->AssignUeIpv4Address (this->ueDeviceLte));
    cout << cYellow;
    cout << "--- criado interface ueInterface - lte" << endl;
    cout << cNormal;
    // Ips }	

    // Routing Table
    //Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

    StaticRouting ();

    cout << cMagenta;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::StaticRouting ()
{
    cout << cMagenta;
    cout << "Method: Static Routing () " << endl;
    cout << cNormal;

    Ipv4StaticRoutingHelper ipv4StaticRoutingHelper;

    // Home Agent
    Ptr<Ipv4StaticRouting> haStaticRouting = ipv4StaticRoutingHelper.GetStaticRouting (haNode.Get (0)->GetObject<Ipv4>());
    haStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);
    haStaticRouting->AddNetworkRouteTo (Ipv4Address ("11.0.0.0"), Ipv4Mask ("255.0.0.0"), 2);
    cout << cYellow;
    cout << "--- roteamento estatico haNode" << endl;
    cout << cNormal;

    Ptr<Ipv4StaticRouting> apStaticRouting = ipv4StaticRoutingHelper.GetStaticRouting (apNode->GetObject<Ipv4>());
    apStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 2); // To Lte
    apStaticRouting->AddNetworkRouteTo (Ipv4Address ("192.168.1.0"), Ipv4Mask ("255.255.255.0"), 2); // The network between the Server (HA) and the PGW node
    cout << cYellow;
    cout << "--- roteamento estatico apNoed" << endl;
    cout << cNormal;

    for (uint16_t c = 0; c < ueNode.GetN (); c++)
    {
        Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4StaticRoutingHelper.GetStaticRouting (ueNode.Get (c)->GetObject<Ipv4>());
        // Default route WLAN
        ueStaticRouting->SetDefaultRoute (Ipv4Address ("11.0.0.1"), 2);
        // Default route LTE
        ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
        // Ap to Server (HA)
        ueStaticRouting->AddNetworkRouteTo (
                                            Ipv4Address ("192.168.2.0"), Ipv4Mask ("255.255.255.0"),
                                            Ipv4Address ("11.0.0.1"), 2);
    }

    cout << cYellow;
    cout << "--- roteamento estatico ueNode" << endl;
    cout << cNormal;

    cout << cMagenta;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

/*
 * Attach the UEs to an eNB. THIs will
 * configure each UE according to the eNB configuration,
 * and create an RRC connection between them.
 */
void
CacLteWlan::AttachUeNode ()
{
    cout << cMagenta;
    cout << "Method: Attach Ue Node () " << endl;
    cout << cNormal;

    lteHelper->Attach (this->ueDeviceLte, this->enbDevice.Get (0));
    cout << cYellow;
    cout << "--- ueDeviceLte attached to enbDevice" << endl;
    cout << cNormal;

    cout << cMagenta;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

//////////////////////////////////////////
//
//	Enabling components of the LTE
//
//////////////////////////////////////////

void
CacLteWlan::EnableTraces ()
{
    this->lteHelper->EnableTraces ();
}

///////////////////////////////////////////////////////////////////////////////
//
// 	Dumping interface
//		Will create all the interfaces to the specific node.
//  - node - the specific node to create the interfaces.
//
//  OBS.: ++contador é melhor do que contador++, pois a implementação da sobrecarga
//    do operador ++contador é mais ágil.
//
///////////////////////////////////////////////////////////////////////////////

void
CacLteWlan::DumpingInterfaces (Ptr<ns3::Node> node, std::string descriptionNode)
{
    Ptr<Ipv4> ipv4Node = node->GetObject<Ipv4>(); // get the IPV4 node of the object

    // Node identification
    std::cout << cYellow;
    std::cout.width (10);
    std::cout.fill (' ');
    std::cout << "";
    std::cout.width (10);
    std::cout.fill ('.');
    std::cout << "";
    std::cout.width (16);
    std::cout << descriptionNode;
    std::cout.fill ();
    std::cout.width ();
    std::cout << " - NODE ID :: ";
    std::cout.width (2);
    std::cout.fill (' ');
    std::cout << node->GetId ();
    std::cout.width (30);
    std::cout.fill ('.');
    std::cout << " " << std::endl;
    // Data Interface
    std::cout << cCyan;
    std::cout.width (10);
    std::cout.fill (' ');
    std::cout << "";
    std::cout.fill ();
    std::cout.width ();
    std::cout << "| INT|" << "   ADDRESS  |" << "      MASK     |";
    std::cout.width (16);
    std::cout.fill (' ');
    std::cout << " ";
    std::cout.fill ();
    std::cout.width ();
    std::cout << "NAME";
    std::cout.width (15);
    std::cout.fill (' ');
    std::cout << " ";
    std::cout.fill ();
    std::cout.width ();
    std::cout << "|" << std::endl;
    uint32_t numberInterfaces = ipv4Node->GetNInterfaces (); // Get the number of interfaces of the node.

    std::cout << cRed;
    for (uint32_t interfaces = 0; interfaces < numberInterfaces; ++interfaces)
    {
        Ptr<NetDevice> nodeDevice = ipv4Node->GetNetDevice (interfaces); // Get the specific device to the interface.
        // Data Interface
        std::cout.width (10);
        std::cout.fill (' ');
        std::cout << "";
        std::cout.width ();
        std::cout.fill ();
        std::cout << "|";
        std::cout.width (4);
        std::cout.fill (' ');
        std::cout << interfaces;
        std::cout.width ();
        std::cout.fill ();
        std::cout << "|";
        std::cout.width (12);
        std::cout.fill (' ');
        std::ostringstream ipAddress;
        ipAddress << ipv4Node->GetAddress (interfaces, 0).GetLocal ();
        std::cout << ipAddress.str ();
        std::cout.width ();
        std::cout.fill ();
        std::ostringstream ipMask;
        ipMask << ipv4Node->GetAddress (interfaces, 0).GetMask ();
        std::cout << "|";
        std::cout.width (15);
        std::cout.fill (' ');
        std::cout << ipMask.str ();
        std::cout.width ();
        std::cout.fill ();
        std::cout << "|";
        std::cout.width (34);
        std::cout.fill (' ');
        std::cout << (nodeDevice ? typeid (*nodeDevice).name () : "loopback");
        std::cout.width ();
        std::cout.fill ();
        std::cout << " ";
        std::cout << "|" << std::endl;
    }
}

///////////////////////////////////////////////////////////////////////////////
//
//  Header DUMPING
//
///////////////////////////////////////////////////////////////////////////////

void
CacLteWlan::headerDumping ()
{
    // Header Table --- DUMPING INTERFACE
    std::cout.width (10);
    std::cout.fill (' ');
    std::cout << "";
    std::cout.width (27);
    std::cout.fill ('#');
    std::cout << " ";
    std::cout.fill ();
    std::cout.width ();
    std::cout << "DUMPING INTERFACE ";
    std::cout.width (27);
    std::cout.fill ('#');
    std::cout << " " << std::endl;
}


/////////////////////////////////////////////////////////////////////////////
//
// Separator DUMPING
//
/////////////////////////////////////////////////////////////////////////////

void
CacLteWlan::separatorDumping ()
{
    // Separator
    std::cout.width (10);
    std::cout.fill (' ');
    std::cout << "";
    std::cout.width (72);
    std::cout.fill ('-');
    std::cout << " " << std::endl;
}

void
CacLteWlan::RunApplications ()
{
    cout << cMagenta;
    cout << "Method: Run Applications." << endl;
    cout << cNormal;

    int sender;
    int receiver;
    int direction;
    int device;
    int app;
    double packetSize;
    double timeOn;
    double timeOff;
    double consApp;
    double startApp;
    string dataRate;
    vector<int> qtAppUsers (30);

    // Zerando o vetor para calcular a media de pacotes por UE
    for (std::vector<int>::iterator ite = qtAppUsers.begin (); ite != qtAppUsers.end (); ++ite)
    {
        *ite = 0;
    }

    // Gera aplicacoes
    for (int i = 0; i < this->totApplications; ++i)
    {
        sender = rand () % (nUeNode); // 0 ate o numero de ue
        do
        {
            receiver = rand () % (nUeNode);
        }
        while (receiver == sender);

        // Utilizaremos o SENDER para efetuar o calculo
        qtAppUsers.at (sender)++;

        direction = rand () % (1 + 1); // 0- ul 1- dl

        app = rand () % (3 + 1); // 0- voice 1- video 2- www 3- ftp

        switch (app)
        {
            case 0: // VOICE
                device = 0; // LTE
                packetSize = 50;
                timeOn = time120;
                timeOff = 0;
                consApp = 24*1024;
                dataRate = "24Kbps";
                break;
            case 1: // VIDEO
                device = 0; // LTE
                packetSize = 429;
                timeOn = time120;
                timeOff = 0;
                consApp = 128*1024;
                dataRate = "128Kbps";
                break;
            case 2: // WWW
                device = 1; // WLAN
                packetSize = 429;
                timeOn = time120;
                timeOff = 0.04;
                consApp = 128*1024;
                dataRate = "128Kbps";
                break;
            case 3: // FTP
                device = 1; // WLAN
                packetSize = 429;
                timeOn = time120;
                timeOff = 0;
                consApp = 128*1024;
                dataRate = "128Kbps";
                break;
        }

        APPLICATION application;
        application.sender = sender;
        application.receiver = receiver;
        application.direction = direction;
        application.device = device;
        application.app = app;
        application.packetSize = packetSize;
        application.timeOn = timeOn;
        application.timeOff = timeOff;
        application.consApp = consApp;
        application.posWlanVector = -1;
        application.status = REQUEST;
        application.dataRate = dataRate;
	
        this->appDesc << "----- POSITION (" << i << ")" << endl
				<< "--> Aplicação:  " << app << "\n--> Direção:  " << direction  << "\n--> Sender (" << sender << ") Ip: \n------> LTE ( " << this->ueNode.Get (sender)->GetObject<ns3::Ipv4>()->GetAddress (1, 0).GetLocal ()
                << ") \n------> WLAN( " << this->ueNode.Get (sender)->GetObject<ns3::Ipv4>()->GetAddress (2, 0).GetLocal ()
                << ")" << endl;

        this->appVector.push_back (application);

    }

    // Gerando Gráfico
    Simulator::Schedule (Seconds (Simulator::Now ()), &CacLteWlan::GraphAppPerUser, this, qtAppUsers);

    // Finalizando a Media de Chamadas
    for (std::vector<int>::iterator ite = qtAppUsers.begin (); ite != qtAppUsers.end (); ++ite)
    {
        this->appAveragePerUser += *ite; // somando as chamadas por UEs
    }

    this->appAveragePerUser = this->appAveragePerUser / 30; // divide pelo numero de UEs	

    this->debugInformation << "--> AVERAGE APP PER USER: " << this->appAveragePerUser << endl;

    this->aplicacao->setTimeApplication (appAveragePerUser, this->users);

    this->debugInformation << "--> TIME APPLICATION - INTERCALL: " << this->aplicacao->getTimeApplications () << endl;

    // Inicia aplicacoes
    for (std::vector<APPLICATION>::iterator ite = this->appVector.begin (); ite != this->appVector.end (); ++ite)
    {
        // Informando o tempo de inicio de cada chamada
        startApp = this->aplicacao->setTimeTotal (this->aplicacao->getTimeApplications ());
        (*ite).startApp = startApp;

        switch ((*ite).app)
        {
            case 0: // voice
                if ((*ite).direction == 0) // ul
                    Simulator::Schedule (Seconds ((*ite).startApp), &CacLteWlan::requestUserVozUl, this, std::distance (this->appVector.begin (), ite));
                else // dl
                    Simulator::Schedule (Seconds ((*ite).startApp), &CacLteWlan::requestUserVozDl, this, std::distance (this->appVector.begin (), ite));
                break;
            case 1: // video
                if ((*ite).direction == 0) // ul
                    Simulator::Schedule (Seconds ((*ite).startApp), &CacLteWlan::requestUserVideoUl, this, std::distance (this->appVector.begin (), ite));
                else // dl
                    Simulator::Schedule (Seconds ((*ite).startApp), &CacLteWlan::requestUserVideoDl, this, std::distance (this->appVector.begin (), ite));
                break;
            case 2: // www
                if ((*ite).direction == 0) // ul
                    Simulator::Schedule (Seconds ((*ite).startApp), &CacLteWlan::requestUserWwwUl, this, std::distance (this->appVector.begin (), ite));
                else // dl
                    Simulator::Schedule (Seconds ((*ite).startApp), &CacLteWlan::requestUserWwwDl, this, std::distance (this->appVector.begin (), ite));
                break;
            case 3: // ftp
                if ((*ite).direction == 0) // ul
                    Simulator::Schedule (Seconds ((*ite).startApp), &CacLteWlan::requestUserFtpUl, this, std::distance (this->appVector.begin (), ite));
                else  // dl
                    Simulator::Schedule (Seconds ((*ite).startApp), &CacLteWlan::requestUserFtpDl, this, std::distance (this->appVector.begin (), ite));
                break;
        }
    }

    Simulator::Schedule (Seconds (0.5), &CacLteWlan::ManagerTimeWlanUl, this);
    Simulator::Schedule (Seconds (0.5), &CacLteWlan::ManagerTimeWlanDl, this);
    Simulator::Schedule (Seconds (0.5), &CacLteWlan::ManagerPacketWlanUl, this);
    Simulator::Schedule (Seconds (0.5), &CacLteWlan::ManagerPacketWlanDl, this);

    cout << cMagenta;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::ManagerTimeWlanUl ()
{
    this->managerTimeUl << "====>   ManagerTimeWlanUl " << endl;

	this->averageTimeUl = this->appBwWlanUl->totTimeAppWlan / timeLeituraWlan;
           
    this->managerTimeUl << "---> Média do tempo em UL: " << this->averageTimeUl << endl;
	
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + timeLeituraWlan + timeEsperaWlan), &CacLteWlan::ManagerTimeWlanUl, this);

}

void
CacLteWlan::ManagerTimeWlanDl ()
{
	
    this->managerTimeDl << "====>   ManagerTimeWlanDl " << endl;

	this->averageTimeDl = this->appBwWlanDl->totTimeAppWlan / timeLeituraWlan;
           
    this->managerTimeDl << "---> Média do tempo em DL: " << this->averageTimeDl << endl;
	
	Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + timeLeituraWlan + timeEsperaWlan), &CacLteWlan::ManagerTimeWlanDl, this);

}

void
CacLteWlan::ManagerPacketWlanUl ()
{
	
    this->managerPackageUl << "====>   ManagerPacketWlanUl " << endl;
    
	this->averagePacketUl = this->appBwWlanUl->totPackageAppWlan / this->appBwWlanUl->qtAppWlan;

    this->managerPackageUl << "\t\t---> Média de pacotes em UL: " << this->averagePacketUl << endl;

    Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + timeLeituraWlan + timeEsperaWlan), &CacLteWlan::ManagerPacketWlanUl, this);

}

void
CacLteWlan::ManagerPacketWlanDl ()
{
    this->managerPackageDl << "====>   ManagerPacketWlanDl " << endl;
			
    this->averagePacketDl = this->appBwWlanDl->totPackageAppWlan / this->appBwWlanDl->qtAppWlan;

    this->managerPackageDl << "\t\t---> Média de pacotes em DL: " << this->averagePacketDl << endl;

    Simulator::Schedule (Seconds (Simulator::Now().GetSeconds() + timeLeituraWlan + timeEsperaWlan), &CacLteWlan::ManagerPacketWlanDl, this);

}
// Private Methods }

#endif

