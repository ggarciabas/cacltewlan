/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil			(); -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */
#ifndef APPHEADER_H
#    define APPHEADER_H

#    include "ns3/random-variable.h"
#    include "ns3/core-module.h"
#	 include "enumStatus.h" 


//using namespace std;
using namespace ns3;

class App
{

  public:
    App 															();

    void setTimeApplication                                         (double, double); // tempo de intercalacao entre as chamadas, vide formula em app.h
    /*void setQtVozLteUl												(int);
    void setQtVozLteDl												(int);
    void setQtVozWlanUl												(int);
    void setQtVozWlanDl												(int);
    void setQtVideoLteUl											(int);
    void setQtVideoLteDl											(int);
    void setQtVideoWlanUl											(int);
    void setQtVideoWlanDl											(int);
    void setQtWwwLteUl												(int);
    void setQtWwwLteDl												(int);
    void setQtWwwWlanUl												(int);
    void setQtWwwWlanDl												(int);
    void setQtFtpLteUl												(int);
    void setQtFtpLteDl												(int);
    void setQtFtpWlanUl												(int);
    void setQtFtpWlanDl												(int);*/

    int getQtVozLteUl												();
    int getQtVozLteDl												();
    int getQtVideoLteUl												();
    int getQtVideoLteDl												();
    int getQtWwwLteUl												();
    int getQtWwwLteDl												();
    int getQtFtpLteUl												();
    int getQtFtpLteDl												();

    uint32_t getVozLteUl											();
    uint32_t getVozLteDl											();
    uint32_t getVideoLteUl											();
    uint32_t getVideoLteDl											();
    uint32_t getWwwLteUl											();
    uint32_t getWwwLteDl											();
    uint32_t getFtpLteUl											();
    uint32_t getFtpLteDl											();

    void setVozLteUl												(uint32_t v);
    void setVozLteDl												(uint32_t v);
    void setVozWlanUl												(uint32_t v);
    void setVozWlanDl												(uint32_t v);
    void setVideoLteUl												(uint32_t v);
    void setVideoLteDl												(uint32_t v);
    void setVideoWlanUl												(uint32_t v);
    void setVideoWlanDl												(uint32_t v);
    void setWwwLteUl												(uint32_t v);
    void setWwwLteDl												(uint32_t v);
    void setWwwWlanUl												(uint32_t v);
    void setWwwWlanDl												(uint32_t v);
    void setFtpLteUl												(uint32_t v);
    void setFtpLteDl												(uint32_t v);
    void setFtpWlanUl												(uint32_t v);
    void setFtpWlanDl												(uint32_t v);

    double setTimeTotal												(double);
    double getTimeApplications                                      ();


  private:
    // Quantidade de chamada aceitas
    int			 													qtVozLteUl;
    int 															qtVozLteDl;
    int 															qtVozWlanUl;
    int 															qtVozWlanDl;
    int					 							 				qtVideoLteUl;
    int 															qtVideoLteDl;
    int		 														qtVideoWlanUl;
    int 															qtVideoWlanDl;
    int 															qtWwwLteUl;
    int 															qtWwwLteDl;
    int 															qtWwwWlanUl;
    int 															qtWwwWlanDl;
    int	 															qtFtpLteUl;
    int 															qtFtpLteDl;
    int																qtFtpWlanUl;
    int 															qtFtpWlanDl;
    // Banda utilizada
    uint32_t		 												vozLteUl;
    uint32_t	 													vozLteDl;
    uint32_t 														vozWlanUl;
    uint32_t 														vozWlanDl;
    uint32_t 														videoLteUl;
    uint32_t 														videoLteDl;
    uint32_t 														videoWlanUl;
    uint32_t 														videoWlanDl;
    uint32_t 														wwwLteUl;
    uint32_t 														wwwLteDl;
    uint32_t 														wwwWlanUl;
    uint32_t 														wwwWlanDl;
    uint32_t 														ftpLteUl;
    uint32_t 														ftpLteDl;
    uint32_t			 											ftpWlanUl;
    uint32_t 														ftpWlanDl;
    // http://www.nsnam.org/doxygen/classns3_1_1_exponential_variable.html

    double															timeTotal;
    double															timeApplications;

} ;

#endif