/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */
#ifndef CACLTEWLANHEADER_H
#    define CACLTEWLANHEADER_H

#    include "app.h"
#    include "structapp.h"
#	 include "structBwWlan.h"
// Decision
#	 include "cacltedecision_request.h"
#	 include "cacltewlan_renegotiation_1.h"
#	 include "cacltewlan_renegotiation_2.h"
// ------------------
#    include "ns3/core-module.h"
#    include "ns3/internet-module.h"
#    include "ns3/random-variable.h"
#    include "ns3/simulator.h"
//#		include "ns3/log.h"
#    include "structGraphs.h"
#    include "ns3/point-to-point-module.h"
#    include "ns3/applications-module.h"
#    include "ns3/flow-monitor-module.h"
#    include "ns3/ptr.h"
#    include "ns3/network-module.h"
#    include "ns3/mobility-module.h"
#    include "ns3/lte-module.h"
#    include "ns3/config-store.h"
#    include "ns3/epc-helper.h"
#    include "ns3/ipv4-global-routing-helper.h"
#    include "ns3/ipv4.h"
#    include "ns3/wifi-module.h"
#    include "ns3/trace-helper.h"
#    include "ns3/netanim-module.h" 
#    include <iostream> 
#    include <stdio.h>
#    include <vector>
#    include <string>
#    include <algorithm>
#    include <cstdlib>
#    include <ctime>
#    include <fstream>

// https://groups.google.com/forum/#!topic/ns-3-users/XQcgBmfNCRI -- Erro 
/*
   PacketSink:StartApplication(0x8597be0)
	[0massert failed. cond="m_count < std::numeric_limits<uint32_t>::max()", file=./ns3/simple-ref-count.h, line=84
	terminate called without an active exception
	Program received signal SIGABRT, Aborted.
	0x00007fffecb611c9 in raise () from /usr/lib/gcc/x86_64-unknown-linux-gnu/4.8.0/../../../../lib/libc.so.6
 */ 
#	 include <limits>

// ----------------------------------------------------------------------

#    define cNormal  				"\x1B[0m" 	// Normal 
#    define cRed  					"\x1B[31m" 	// Red -- Drop Printing
#    define cGreen  				"\x1B[32m" 	// Green 
#    define cYellow  				"\x1B[33m"	// Yeallow -- Infromation printing 
#    define cBlue  					"\x1B[34m"	// Blue -- Flow information
#    define cMagenta  				"\x1B[35m"	// Magenta -- Method name
#    define cCyan  					"\x1B[36m"	// Cyan
#    define cWhite  				"\x1B[37m"	// White 
#    define time120					120 		// tempo total de cada aplicacao
# 	 define timeLeituraWlan  		3			// tempo para leitura do meio WLAN - tempo que firá lendo o meio para fazer a média de tempo e de pacotes
#	 define timeEsperaWlan			5			// tempo de espera para a próxima leitura do meio WLAN - utilizado para cálculo BW no WLAN	

using namespace ns3;
using namespace std;

class CacLteWlan
{

  public:
    CacLteWlan													();
    bool Configure 												(int argc, char **argv);
    void Run 													();
    //void Report												(std::ostream & os);
    void GraphLteUlTime                 						(vector<CALLACCEPTEDLTEUL>);
    void GraphAppPerUser                        				(vector<int>);

  private:

    // Args
    double														users;
    uint16_t													nUeNode;
    uint16_t													nEnbNode;
    uint16_t													nHaNode;
    uint16_t													nApNode;    
    uint16_t 													f;					// http://en.wikipedia.org/wiki/Byzantine_fault_tolerance
    bool														pcap;
    bool														wifi;
    //bool														mobile;
    //bool														grid;
    bool														drop;
    double														totalTime;

    // Constants
    string 														phyMode;
    double														nodeSpeed; 			// in ms
    double														nodePause;			// in s
    //uint16_t													widthScenario;
    //uint16_t													heightScenario;

    // Reports
    uint32_t													MacTxDropCount;
    uint32_t													PhyTxDropCount;
    uint32_t													PhyRxDropCount;

    // Auxiliar vars for Config
    Ptr<LteHelper> 												lteHelper;
    Ptr<EpcHelper> 												epcHelper;
    Ptr<Node> 													pgwNode;
    NodeContainer												ueNode;
    NodeContainer												enbNode;
    Ptr<Node>													apNode;
    NodeContainer												haNode;
    NetDeviceContainer                          				ueDeviceLte;
    NetDeviceContainer                          				ueDeviceWlan;
    NetDeviceContainer                          				enbDevice;
    NetDeviceContainer                          				apDevice;
    NetDeviceContainer                          				haDevice;
    Ipv4InterfaceContainer                      				ueInterface;
    Ipv4InterfaceContainer                      				apInterface;
    Ipv4InterfaceContainer                      				haInterface;

    // Traffic
    uint32_t													maxEnbUl;
    uint32_t													maxEnbDl;
    uint32_t													maxApUl;
    uint32_t													maxApDl;
    App 														*aplicacao;
    vector<APPLICATION>                         				appVector; 			// all applications created to simlate the CAC
    double														totApplications; 	// total de aplicações que serão analisadas dentro da simulação
    double 														appAveragePerUser; 	// Media de pacotes por UEs
    Ptr<FlowMonitor>                            				monitor;
    vector<uint32_t>                            				ueVoipApp; 			// Ues que estao enviando aplicacoes voip
    double														averagePacketUl; 	// Average Packet used to calculate the BW on WLAN
    double														averagePacketDl; 	// Average Packet used to calculate the BW on WLAN
    double														averageTimeUl; 		// Average time used to calculate the BW on WLAN
    double														averageTimeDl; 		// Average time used to calculate the BW on WLAN
    double														arrivedCalls; 		// Chamadas chegadas para o servidor. Utilizada para a análise de BW no WLAN.	
	APPBWWLAN													*appBwWlanUl; 		// para cálculo de BW no WLAN UL
	APPBWWLAN													*appBwWlanDl; 		// para cálculo de BW no WLAN DL

    // Arquivos
    FlowMonitorHelper                  				 			flowmon;
    ofstream													flowMonitor; 		// ("./scratch/cacLteWlan/log/flowMonitor.txt", ios::app); // Flow Monitor		
    ofstream													managerTimeUl; 		// ("./scratch/cacLteWlan/log/managerTimeUl.txt", ios::app); // Gerenciador de tempo
    ofstream													managerTimeDl;		// ("./scratch/cacLteWlan/log/managerTimeDl.txt", ios::app); // Gerenciador de tempo
    ofstream													managerPackageUl; 	// ("./scratch/cacLteWlan/log/managerPackageUl.txt", ios::app); // Gerenciador de pacotes
    ofstream													managerPackageDl; 	// ("./scratch/cacLteWlan/log/managerPackageDl.txt", ios::app); // Gerenciador de pacotes
    ofstream													debugInformation; 	// ("./scratch/cacLteWlan/log/debugInformation.txt", ios::app); // Informacoes de controle - debug
	ofstream													appDesc; 			// ("./scratch/cacLteWlan/log/appDesc.txt", ios::app); // DEscricao das aplicacoes - debug

    // Gráficos
    vector<CALLACCEPTEDLTEUL>             		              	callsLteUlTime; 	// dados para geração do gráfico.

    // Dados para gráficos
    int 														acceptedVoipLte; 	// chamadas VOIP aceitas no lte
    int 														acceptedVoipWlan; 	// chamadas VOIP aceitas no wlan
    int 														acceptedVideoLte; 	// chamadas VIDEO aceitas no lte
    int 														acceptedVideoWlan; 	// chamadas VIDEO aceitas no wlan
    int 														acceptedWwwLte; 	// chamadas WWW aceitas no lte
    int 														acceptedWwwWlan; 	// chamadas WWW aceitas no wlan
    int 														acceptedFtpLte; 	// chamadas FTP aceitas no lte
    int 														acceptedFtpWlan; 	// chamadas FTP aceitas no wlan
	int 														deniedVoipLte; 		// chamadas VOIP negadas no lte
    int 														deniedVoipWlan; 	// chamadas VOIP negadas no wlan
    int 														deniedVideoLte; 	// chamadas VIDEO negadas no lte
    int 														deniedVideoWlan; 	// chamadas VIDEO negadas no wlan
    int 														deniedWwwLte; 		// chamadas WWW negadas no lte
    int 														deniedWwwWlan; 		// chamadas WWW negadas no wlan
    int 														deniedFtpLte; 		// chamadas FTP negadas no lte
    int 														deniedFtpWlan; 		// chamadas FTP negadas no wlan	
    int 														handoverLteWlan; 	// chamadas com handover aceitas do lte para wlan
    int 														handoverWlanLte; 	// chamadas com handover aceitas do wlan para lte
    ofstream													callAdmLte; 		// ("./scratch/cacLteWlan/gnuplot/call_admission_lte/call_admission_lte.txt", ios::app); 
    ofstream													callAdmWlan; 		// ("./scratch/cacLteWlan/gnuplot/call_admission_wlan/call_admission_wlan.txt", ios::app); 
    ofstream													callHand; 			// ("./scratch/cacLteWlan/gnuplot/call_handover/call_handover.txt", ios::app);

  private:

    // Flowmonitor prints
    /*void PhyRxDrop                            				(Ptr<const Packet>);
    void PhyTxDrop                                      		(Ptr<const Packet>);
    void MacTxDrop              								(Ptr<const Packet>);
    void PrintDrop                      						(Ptr<const Packet>);*/
    void FlowTraffic                                    		();
    void SerializeFlowMonitor                           		();

    void CreateNodes                            				();
    void SetMobility                            				();
    void CreateDevices                          				();
    void InstallInternetStack                           		();
    void RunApplications                                		();
    void AttachUeNode                                  	 		();
    void EnableTraces                                   		();
    void StaticRouting                                          ();

    // Will be implemented all decisions
    void requestUserVozUl                                       (int);
    void requestUserVideoUl                                     (int);
    void requestUserFtpUl                                       (int);
    void requestUserWwwUl                                       (int);
    void requestUserVozDl                                       (int);
    void requestUserVideoDl                                     (int);
    void requestUserFtpDl                                       (int);
    void requestUserWwwDl                                       (int);
    void renegotiation_1_UserVozUl                              (int);
    void renegotiation_1_UserVideoUl                            (int);
    void renegotiation_1_UserFtpUl                              (int);
    void renegotiation_1_UserWwwUl                              (int);
    void renegotiation_1_UserVozDl                              (int);
    void renegotiation_1_UserVideoDl                            (int);
    void renegotiation_1_UserFtpDl                              (int);
    void renegotiation_1_UserWwwDl                              (int);
    void renegotiation_2_UserVozUl                              (int);
    void renegotiation_2_UserVideoUl                            (int);
    void renegotiation_2_UserFtpUl                              (int);
    void renegotiation_2_UserWwwUl                              (int);
    void renegotiation_2_UserVozDl                              (int);
    void renegotiation_2_UserVideoDl                            (int);
    void renegotiation_2_UserFtpDl                              (int);
    void renegotiation_2_UserWwwDl                              (int);
    void realocationRequestUserVozUl                            (int);
    void realocationRequestUserVideoUl                          (int);
    void realocationRequestUserFtpUl                            (int);
    void realocationRequestUserWwwUl                            (int);
    void realocationRequestUserVozDl     	                    (int);
    void realocationRequestUserVideoDl                          (int);
    void realocationRequestUserFtpDl                            (int);
    void realocationRequestUserWwwDl                            (int);	
	void realocationRalocation_1_UserVozUl                      (int);
    void realocationRalocation_1_UserVideoUl                    (int);
    void realocationRalocation_1_UserFtpUl                      (int);
    void realocationRalocation_1_UserWwwUl                      (int);
    void realocationRalocation_1_UserVozDl                      (int);
    void realocationRalocation_1_UserVideoDl                    (int);
    void realocationRalocation_1_UserFtpDl                      (int);
    void realocationRalocation_1_UserWwwDl                      (int);	
	void realocationRalocation_2_UserVozUl                      (int);
    void realocationRalocation_2_UserVideoUl                    (int);
    void realocationRalocation_2_UserFtpUl                      (int);
    void realocationRalocation_2_UserWwwUl                      (int);
    void realocationRalocation_2_UserVozDl                      (int);
    void realocationRalocation_2_UserVideoDl                    (int);
    void realocationRalocation_2_UserFtpDl                      (int);
    void realocationRalocation_2_UserWwwDl                      (int);

    // Application
    void createApplicationLteUl                                 (int);
    void createApplicationLteDl                                 (int);
    void createApplicationWlanUl                                (int);
    void createApplicationWlanDl                                (int);

    // End App
    void endUserLteUl                                           (int);
    void endUserLteDl                                           (int);
    void endUserWlanUl                                          (int);
    void endUserWlanDl                                          (int);

    // Dumping interfaces
    void DumpingInterfaces                                      (Ptr<ns3::Node>, std::string);
    void separatorDumping                                       ();
    void headerDumping                                          ();

    // Managing WLAN
    void ManagerPacketWlanUl                                    ();
    void ManagerTimeWlanUl                                      ();
    void ManagerPacketWlanDl                                    ();
    void ManagerTimeWlanDl                                      ();

} ;

#endif 
