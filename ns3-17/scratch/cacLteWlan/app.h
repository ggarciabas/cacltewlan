/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil			(); -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */
#ifndef APP_H
#    define APP_H

#    include "appheader.h"
#    include <iostream>

App::App ()
{
    this->vozLteUl                              = 0;
    this->vozLteDl                              = 0;
    this->vozWlanUl                             = 0;
    this->vozWlanDl                             = 0;
    this->videoLteUl                            = 0;
    this->videoLteDl                            = 0;
    this->videoWlanUl                           = 0;
    this->videoWlanDl                           = 0;
    this->wwwLteUl                              = 0;
    this->wwwLteDl                              = 0;
    this->wwwWlanUl                             = 0;
    this->wwwWlanDl                             = 0;
    this->ftpLteUl                              = 0;
    this->ftpLteDl                              = 0;
    this->ftpWlanUl                             = 0;
    this->ftpWlanDl                             = 0;
    this->timeApplications                      = 0;

    //std::cout << "====>>>>>>>> " << this->timeApplications << std::endl;

    this->timeTotal                             = 0;
}

void
App::setTimeApplication (double appAveragePerUser, double users)
{
    // l - lambida
    // Uq - media do numero de usuarios
    // Uc - media de chamadas por usuario
    // 1/l = 3600 / Uq * Uc
    this->timeApplications                      = 3600 / (users * appAveragePerUser); // Tempo de diferenca entre cada aplicacao
    this->timeTotal                             = this->timeApplications * (-1); // Tempo atual de aplicacoes -- *(-1) para que sempre uma aplicacao inicie em 0.0	
}
/*
void
App::setQtVozLteUl (int v)
{
    this->qtVozLteUl = v;
}

void
App::setQtVozLteDl (int v)
{
    this->qtVozLteDl = v;
}

void
App::setQtVozWlanUl (int v)
{
    this->qtVozWlanUl = v;
}

void
App::setQtVozWlanDl (int v)
{
    this->qtVozWlanUl = v;
}

void
App::setQtVideoLteUl (int v)
{
    this->qtVideoLteUl = v;
}

void
App::setQtVideoLteDl (int v)
{
    this->qtVideoLteDl = v;
}

void
App::setQtVideoWlanUl (int v)
{
    this->qtVideoWlanUl = v;
}

void
App::setQtVideoWlanDl (int v)
{
    this->qtVideoWlanDl = v;
}

void
App::setQtWwwLteUl (int v)
{
    this->qtWwwLteUl = v;
}

void
App::setQtWwwLteDl (int v)
{
    this->qtWwwLteDl = v;
}

void
App::setQtWwwWlanUl (int v)
{
    this->qtWwwWlanUl = v;
}

void
App::setQtWwwWlanDl (int v)
{
    this->qtWwwWlanDl = v;
}

void
App::setQtFtpLteUl (int v)
{
    this->qtFtpLteUl = v;
}

void
App::setQtFtpLteDl (int v)
{
    this->qtFtpLteDl = v;
}

void
App::setQtFtpWlanUl (int v)
{
    this->qtFtpWlanUl = v;
}

void
App::setQtFtpWlanDl (int v)
{
    this->qtFtpWlanDl = v;
}

int
App::getQtVozLteUl ()
{
    return this->qtVozLteUl;
}

int
App::getQtVozLteDl ()
{
    return this->qtVozLteDl;
}

int
App::getQtVozWlanUl ()
{
    return this->qtVozWlanUl;
}

int
App::getQtVozWlanDl ()
{
    return this->qtVozWlanDl;
}

int
App::getQtVideoLteUl ()
{
    return this->qtVideoLteUl;
}

int
App::getQtVideoLteDl ()
{
    return this->qtVideoLteDl;
}

int
App::getQtVideoWlanUl ()
{
    return this->qtVideoWlanUl;
}

int
App::getQtVideoWlanDl ()
{
    return this->qtVideoWlanDl;
}

int
App::getQtWwwLteUl ()
{
    return this->qtWwwLteUl;
}

int
App::getQtWwwLteDl ()
{
    return this->qtWwwLteDl;
}

int
App::getQtWwwWlanUl ()
{
    return this->qtWwwWlanUl;
}

int
App::getQtWwwWlanDl ()
{
    return this->qtWwwWlanDl;
}

int
App::getQtFtpLteUl ()
{
    return this->qtFtpLteUl;
}

int
App::getQtFtpLteDl ()
{
    return this->qtFtpLteDl;
}

int
App::getQtFtpWlanUl ()
{
    return this->qtFtpWlanUl;
}

int
App::getQtFtpWlanDl ()
{
    return this->qtFtpWlanDl;
}
*/
uint32_t
App::getVozLteUl	()
{
    return this->vozLteUl;
}

uint32_t
App::getVozLteDl	()
{
    return this->vozLteDl;
}

uint32_t
App::getVideoLteUl ()
{
    return this->videoLteUl;
}

uint32_t
App::getVideoLteDl ()
{
    return this->videoLteDl;
}

uint32_t
App::getWwwLteUl ()
{
    return this->wwwLteUl;
}

uint32_t
App::getWwwLteDl ()
{
    return this->wwwLteDl;
}

uint32_t
App::getFtpLteUl ()
{
    return this->ftpLteUl;
}

uint32_t
App::getFtpLteDl ()
{
    return this->ftpLteDl;
}

void
App::setVozLteUl (uint32_t v)
{
    this->vozLteUl = v;
}

void
App::setVozLteDl (uint32_t v)
{
    this->vozLteDl = v;
}

void
App::setVideoLteUl (uint32_t v)
{
    this->videoLteUl = v;
}

void
App::setVideoLteDl	(uint32_t v)
{
    this->videoLteDl = v;
}

void
App::setWwwLteUl (uint32_t v)
{
    this->wwwLteUl = v;
}

void
App::setWwwLteDl (uint32_t v)
{
    this->wwwLteDl = v;
}

void
App::setFtpLteUl (uint32_t v)
{
    this->ftpLteUl = v;
}

void
App::setFtpLteDl (uint32_t v)
{
    this->ftpLteDl = v;
}

double
App::setTimeTotal (double t)
{
    this->timeTotal += t;
    return this->timeTotal;
}

double
App::getTimeApplications ()
{
    return this->timeApplications;
}

#endif /* APP_H */