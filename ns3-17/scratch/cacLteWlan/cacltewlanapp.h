/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */
#ifndef CACLTEWLANAPPLICATION_H
#    define CACLTEWLANAPPLICATION_H

#    include "cacltewlanheader.h"


// Application {

void
CacLteWlan::createApplicationLteUl (int position)
{
    cout << cMagenta;
    cout << "Method: Create Application Lte Ul." << endl;
    cout << cNormal;

    ApplicationContainer appContainer;

    ns3::Ptr<ns3::Node> sender = this->ueNode.Get ((this->appVector.at (position)).sender);
    ns3::Ipv4Address receiverIpAddress = this->ueNode.Get ((this->appVector.at (position)).receiver)->GetObject<ns3::Ipv4>()->GetAddress (2, 0).GetLocal (); // Ipv4 Address of the receiver

    cout << "--> Ipv4 address receiver: " << receiverIpAddress << endl;

    switch ((this->appVector.at (position)).app)
    {
        case 0: // Voice
        {

            cout << cCyan;
            ns3::PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 5060));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // who will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::UdpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.024Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (50)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	5060));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de UL do LTE
            this->aplicacao->setVozLteUl (this->aplicacao->getVozLteUl () + this->aplicacao->getConsVozLte ());

            cout << cNormal;
        }
            break;
        case 1: // VIDEO
        {
            cout << cCyan;

            ns3::PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 5070));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::UdpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	5070));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de UL do LTE
            this->aplicacao->setVideoLteUl (this->aplicacao->getVideoLteUl () + this->aplicacao->getConsVideoLte ());
        }
            break;
        case 2: // WWW
        {
            cout << cCyan;
            ns3::PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 8080));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::TcpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0.04]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	8080));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de UL do LTE
            this->aplicacao->setWwwLteUl (this->aplicacao->getWwwLteUl () + this->aplicacao->getConsWwwLte ());
        }
            break;
        case 3: // FTP
        {
            cout << cCyan;
            ns3::PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 2121));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::TcpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	2121));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de UL do LTE
            this->aplicacao->setFtpLteUl (this->aplicacao->getFtpLteUl () + this->aplicacao->getConsFtpLte ());
        }
            break;
    }

    appContainer.Start (Seconds (this->appVector.at (position).startApp));
    appContainer.Stop  (Seconds (this->appVector.at (position).startApp + time120));

    cout << cYellow;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::createApplicationLteDl (int position)
{
    cout << cMagenta;
    cout << "Method: Create Application Lte Dl." << endl;
    cout << cNormal;

    ApplicationContainer appContainer;

    ns3::Ptr<ns3::Node> sender = this->ueNode.Get ((this->appVector.at (position)).sender);
    ns3::Ipv4Address receiverIpAddress = this->ueNode.Get ((this->appVector.at (position)).receiver)->GetObject<ns3::Ipv4>()->GetAddress (2, 0).GetLocal (); // Ipv4 Address of the receiver

    cout << "--> Ipv4 address receiver: " << receiverIpAddress << endl;

    switch ((this->appVector.at (position)).app)
    {
        case 0: // Voice
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 5060));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::UdpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.024Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (50)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	5060));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de DL do LTE
            this->aplicacao->setVozLteDl (this->aplicacao->getVozLteDl () + this->aplicacao->getConsVozLte ());
        }
            break;
        case 1: // VIDEO
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 5070));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::UdpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	5070));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de DL do LTE
            this->aplicacao->setVideoLteDl (this->aplicacao->getVideoLteDl () + this->aplicacao->getConsVideoLte ());

        }
            break;
        case 2: // WWW
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 8080));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet	

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::TcpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0.04]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	8080));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de DL do LTE
            this->aplicacao->setWwwLteDl (this->aplicacao->getWwwLteDl () + this->aplicacao->getConsWwwLte ());
        }
            break;
        case 3: // FTP
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 2121));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::TcpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	2121));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de DL do LTE
            this->aplicacao->setFtpLteDl (this->aplicacao->getFtpLteDl () + this->aplicacao->getConsFtpLte ());
        }
            break;
    }

    appContainer.Start (Seconds (this->appVector.at (position).startApp));
    appContainer.Stop  (Seconds (this->appVector.at (position).startApp + time120));

    cout << cYellow;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::createApplicationWlanUl (int position)
{
    cout << cMagenta;
    cout << "Method: Create Application Wlan Ul." << endl;
    cout << cNormal;

    ApplicationContainer appContainer;

    ns3::Ptr<ns3::Node> sender = this->ueNode.Get ((this->appVector.at (position)).sender);
    ns3::Ipv4Address receiverIpAddress = this->ueNode.Get ((this->appVector.at (position)).receiver)->GetObject<ns3::Ipv4>()->GetAddress (1, 0).GetLocal (); // Ipv4 Address of the receiver

    cout << "--> Ipv4 address receiver: " << receiverIpAddress << endl;

    switch ((this->appVector.at (position)).app)
    {
        case 0: // Voice
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 5060));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::UdpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.024Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (50)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	5060));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de UL do WLAN
            this->aplicacao->setVozWlanUl (this->aplicacao->getVozWlanUl () + this->aplicacao->getConsVozWlan ());
        }
            break;
        case 1: // VIDEO
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 5070));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::UdpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	5070));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de UL do WLAN
            this->aplicacao->setVideoWlanUl (this->aplicacao->getVideoWlanUl () + this->aplicacao->getConsVideoWlan ());
        }
            break;
        case 2: // WWW
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 8080));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::TcpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0.04]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	8080));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de UL do WLAN
            this->aplicacao->setWwwWlanUl (this->aplicacao->getWwwWlanUl () + this->aplicacao->getConsWwwWlan ());
        }
            break;
        case 3: // FTP
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 2121));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::TcpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	2121));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de UL do WLAN
            this->aplicacao->setFtpWlanUl (this->aplicacao->getFtpWlanUl () + this->aplicacao->getConsFtpWlan ());
        }
            break;
    }

    appContainer.Start (Seconds (this->appVector.at (position).startApp));
    appContainer.Stop  (Seconds (this->appVector.at (position).startApp + time120));

    cout << cYellow;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::createApplicationWlanDl (int position)
{
    cout << cMagenta;
    cout << "Method: Create Application Wlan Dl." << endl;
    cout << cNormal;

    ApplicationContainer appContainer;

    ns3::Ptr<ns3::Node> sender = this->ueNode.Get ((this->appVector.at (position)).sender);
    ns3::Ipv4Address receiverIpAddress = this->ueNode.Get ((this->appVector.at (position)).receiver)->GetObject<ns3::Ipv4>()->GetAddress (1, 0).GetLocal (); // Ipv4 Address of the receiver

    cout << "--> Ipv4 address receiver: " << receiverIpAddress << endl;

    switch ((this->appVector.at (position)).app)
    {
        case 0: // Voice
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 5060));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::UdpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.024Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (50)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	5060));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de DL do WLAN
            this->aplicacao->setVozWlanDl (this->aplicacao->getVozWlanDl () + this->aplicacao->getConsVozWlan ());
        }
            break;
        case 1: // VIDEO
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::UdpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 5070));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::UdpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	5070));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de DL do WLAN
            this->aplicacao->setVideoWlanDl (this->aplicacao->getVideoWlanDl () + this->aplicacao->getConsVideoWlan ());
        }
            break;
        case 2: // WWW
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 8080));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::TcpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0.04]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	8080));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de DL do WLAN
            this->aplicacao->setWwwWlanDl (this->aplicacao->getWwwWlanDl () + this->aplicacao->getConsWwwWlan ());
        }
            break;
        case 3: // FTP
        {
            ns3::PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", ns3::InetSocketAddress (ns3::Ipv4Address::GetAny (), 2121));
            appContainer = sinkHelper.Install (this->ueNode.Get ((this->appVector.at (position)).receiver));  // whi will receive the packet

            // OnOffHelper is a client that send data to the destination node : http://www.nsnam.org/docs/release/3.16/doxygen/classns3_1_1_on_off_application.html ;
            ns3::OnOffHelper onoff ("ns3::TcpSocketFactory",	ns3::Address ()); // Address() - creates an invalid address.
            onoff.SetAttribute ("OnTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=120]")); // On time
            onoff.SetAttribute ("OffTime", ns3::StringValue ("ns3::ConstantRandomVariable[Constant=0]")); // Off time
            // P.S.: offTime + DataRate/PacketSize = next packet time
            onoff.SetAttribute ("DataRate", ns3::DataRateValue (ns3::DataRate ("0.128Mbps"))); // Data Rate
            onoff.SetAttribute ("PacketSize", ns3::UintegerValue (429)); // Packet Size

            ns3::AddressValue receiverAddress (ns3::InetSocketAddress (receiverIpAddress,	2121));
            onoff.SetAttribute ("Remote", receiverAddress);

            appContainer.Add (onoff.Install (sender));

            // Consumindo banda de DL do WLAN
            this->aplicacao->setFtpWlanDl (this->aplicacao->getFtpWlanDl () + this->aplicacao->getConsFtpWlan ());
        }
            break;
    }

    appContainer.Start (Seconds (this->appVector.at (position).startApp));
    appContainer.Stop  (Seconds (this->appVector.at (position).startApp + time120));

    cout << cYellow;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

// Application }

// End App {

void
CacLteWlan::endUserLteUl (int position)
{
    cout << cMagenta;
    cout << "Method: End User Voz Ul." << endl;
    cout << cNormal;

    switch ((this->appVector.at (position)).app)
    {
        case 0: // Voice
        {
            // Retornando banda de UL do LTE
            this->aplicacao->setVozLteUl (this->aplicacao->getVozLteUl () - this->aplicacao->getConsVozLte ());

            // Autorizando os UEs a efetuarem chamadas VOIP			
            this->ueVoipApp[this->appVector.at (position).sender] = 0;
            this->ueVoipApp[this->appVector.at (position).receiver] = 0;
        }
            break;
        case 1: // VIDEO
        {
            // Retornando banda de UL do LTE
            this->aplicacao->setVideoLteUl (this->aplicacao->getVideoLteUl () - this->aplicacao->getConsVideoLte ());
        }
            break;
        case 2: // WWW
        {
            // Retornando banda de UL do LTE
            this->aplicacao->setWwwLteUl (this->aplicacao->getWwwLteUl () - this->aplicacao->getConsWwwLte ());
        }
            break;
        case 3: // FTP
        {
            // Retornando banda de UL do LTE
            this->aplicacao->setFtpLteUl (this->aplicacao->getFtpLteUl () - this->aplicacao->getConsFtpLte ());
        }
            break;
    }

    cout << cYellow;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::endUserLteDl (int position)
{
    cout << cMagenta;
    cout << "Method: End User Voz Dl." << endl;
    cout << cNormal;

    switch ((this->appVector.at (position)).app)
    {
        case 0: // Voice
        {
            // Retornando banda de DL do LTE
            this->aplicacao->setVozLteDl (this->aplicacao->getVozLteDl () - this->aplicacao->getConsVozLte ());

            // Autorizando os UEs a efetuarem chamadas VOIP			
            this->ueVoipApp[this->appVector.at (position).sender] = 0;
            this->ueVoipApp[this->appVector.at (position).receiver] = 0;
        }
            break;
        case 1: // VIDEO
        {
            // Retornando banda de Dl do LTE
            this->aplicacao->setVideoLteDl (this->aplicacao->getVideoLteDl () - this->aplicacao->getConsVideoLte ());
        }
            break;
        case 2: // WWW
        {
            // Retornando banda de Dl do LTE
            this->aplicacao->setWwwLteDl (this->aplicacao->getWwwLteDl () - this->aplicacao->getConsWwwLte ());
        }
            break;
        case 3: // FTP
        {
            // Retornando banda de Dl do LTE
            this->aplicacao->setFtpLteDl (this->aplicacao->getFtpLteDl () - this->aplicacao->getConsFtpLte ());
        }
            break;
    }

    cout << cYellow;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::endUserWlanUl (int position)
{
    cout << cMagenta;
    cout << "Method: End User Video Ul." << endl;
    cout << cNormal;

    // Remove a aplicacao que estava sendo enviada pelo WLAN
    //this->apAcceptedAppUl[position] = 0;
	
	this->appBwWlanUl->totTimeAppWlan -= this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
	this->appBwWlanUl->totPackageAppWlan -= this->appVector[position].packetSize;
	this->appBwWlanUl->qtAppWlan--;

    if ((this->appVector.at (position)).app)
    {// Voice		
        // Autorizando os UEs a efetuarem chamadas VOIP			
        this->ueVoipApp[this->appVector.at (position).sender] = 0;
        this->ueVoipApp[this->appVector.at (position).receiver] = 0;
    }

    cout << cYellow;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

void
CacLteWlan::endUserWlanDl (int position)
{
    cout << cMagenta;
    cout << "Method: End User Video Dl." << endl;
    cout << cNormal;

    // Remove a aplicacao que estava sendo enviada pelo WLAN
    //this->apAcceptedAppDl[position] = 0;
	
	this->appBwWlanDl->totTimeAppWlan -= this->appVector[position].timeOn + this->appVector[position].timeOff; // para calculo de BW no WLAN
	this->appBwWlanDl->totPackageAppWlan -= this->appVector[position].packetSize;
	this->appBwWlanDl->qtAppWlan--;

    if ((this->appVector.at (position)).app)
    {// Voice		
        // Autorizando os UEs a efetuarem chamadas VOIP			
        this->ueVoipApp[this->appVector.at (position).sender] = 0;
        this->ueVoipApp[this->appVector.at (position).receiver] = 0;
    }

    cout << cYellow;
    cout << "---------------------------------------" << endl;
    cout << cNormal;
}

// End App}


#endif