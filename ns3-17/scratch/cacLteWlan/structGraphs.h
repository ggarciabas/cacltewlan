/*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
* 
* Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
*/
typedef struct structCallAcceptedLteUl {
	int                     posicao; // posicao da aplicacao lte
	int                     tempo; // tempo de inicio da aplicacao 			
} CALLACCEPTEDLTEUL;




/*
// Dados para o gráfico - LTE UL TIME
		if (application.device == 0) {
			CALLSLTEULTIME data;
			data.posicao = i;
			data.tempo = application.startApp;
			this->callsLteUlTime.push_back(data);		
		}
	// Generate Graph with Gnuplot
	Simulator::Schedule(Seconds(Simulator::Now()), &CacLteWlan::GraphLteUlTime, this, this->callsLteUlTime);
*/