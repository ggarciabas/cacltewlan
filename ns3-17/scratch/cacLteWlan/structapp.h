/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */
#include "enumStatus.h"
#include <string.h>

typedef struct Application
{

    int                                                 sender; // remetente
    int                                                 receiver; // destinatario
    int                                                 device; // wlan ou lte
    int                                                 direction; // ul ou dl
    int                                                 app; // voice, video, ftp e www
    double                                              packetSize;
    double                                              timeOn;
    double                                              timeOff;
    double                                              consApp; // Requisicao de banda para a aplicação - bps (1024Kbps)
    uint32_t                                            posWlanVector; // show the position app on wlan vector - to calculate the bandwidth
    double                                              startApp; // Time to start the application
    int                                                 acceptedDenied; // flag -- call accepted or not
    AppStatus                                           status; // Status -- application
    std::string                                         dataRate;
} APPLICATION;