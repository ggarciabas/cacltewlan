set terminal png
set output 'graph-app-per-user.png'
set title 'Calls per user'
set xlabel 'User'
set ylabel 'Application'

set xrange [0:+31]
plot '-'  title 'calls' with lines
1 256
2 263
3 272
4 246
5 251
6 291
7 287
8 245
9 296
10 235
11 268
12 299
13 243
14 243
15 253
16 270
17 270
18 270
19 285
20 269
21 252
22 287
23 255
24 300
25 267
26 291
27 258
28 248
29 265
30 265
e
