/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "cacltewlan.h"
#include <fstream>
#include <iostream> 
#include <string>
#include <stdio.h>

#define cNormal  	"\x1B[0m" 	// Normal 
#define cRed  		"\x1B[31m" 	// Red -- Drop Printing
#define cGreen  	"\x1B[32m" 	// Green -- Infromation printing 
#define cYellow  	"\x1B[33m"	// Yeallow -- Flow information
#define cBlue  		"\x1B[34m"	// Blue
#define cMagenta  	"\x1B[35m"	// Magenta
#define cCyan  		"\x1B[36m"	// Cyan
#define cWhite  	"\x1B[37m"	// White

using namespace std;
using namespace ns3;
//namespace ns3 {

// FlowMonitor Prints {	
void
	CacLteWlan::MacTxDrop(Ptr<const Packet> p) 
{
	/*TypeHeader typeHeader;
	p->PeekHeader(typeHeader);;
	NS_LOG_INFO("Packet Drop " << typeHeader.GetId());
	MacTxDropCount++;*/
}

void 
	CacLteWlan::PhyTxDrop(Ptr<const Packet> p) 
{
	/*TypeHeader typeHeader;
	p->PeekHeader(typeHeader);
	NS_LOG_INFO("Packet Drop " << typeHeader.GetId());
	PhyTxDropCount++;*/
}

void
	CacLteWlan::PhyRxDrop(Ptr<const Packet> p)
{
	/*TypeHeader typeHeader;
	p->PeekHeader(typeHeader);
	NS_LOG_INFO("Packet Drop " << typeHeader.GetId());
	PhyRxDropCount++;*/
}
// FlowMonitor Prints  }


// Public Methods {
CacLteWlan::CacLteWlan ()
{
	this->csvFilename = "cacLteWlan.csv";
	this->nApNode = 1;
	this->nHaNode = 1;
	this->f = 3;
	this->pcap = false;
	//this->mobile				 (true),
	//this->grid				 (true),
	this->drop = false;
	this->qtApplication = 1000;
	this->nEnbNode = 1;
	this->nUeNode = 10;
	this->phyMode = "DsssRate11Mbps";
	this->nodeSpeed = 1; // in m/s
	this->nodePause = 2; // in s
	//widthScenario = 12; 
	//heightScenario = 12;
	this->aplicacao = new App(0.5, 0.5, 0.5, 0.5, 10, 10, 10, 10); // Configurando aplicacoes - Tempo e entrada
}		

bool
	CacLteWlan::Configure (int argc, char **argv)
{
	// SeedManager::SetSeed (12345);
	CommandLine cmd;
	cmd.AddValue ("csv","Save throughput information into csvfilename.", csvFilename);
	cmd.AddValue ("a", "Number of applications.", qtApplication);
	//cmd.AddValue ("p",  "Write PCAP traces.", pcap);
	//cmd.AddValue ("m",  "Enable mobility", mobile);
	//cmd.AddValue ("g",  "Enable grid", grid);
	//cmd.AddValue ("n",  "Number of nodes.", nUeNode);
	//cmd.AddValue ("f",  "Number of bizantine faults.", f);
	//cmd.AddValue ("t",  "BftTest time, s.", totalTime);
	//cmd.AddValue ("d",  "Flowmonitor", drop);
	
	cmd.Parse (argc, argv);
	return true;
}

void
	CacLteWlan::Run ()
{	
	// LogComponentEnable("ReachableBroadcast", LOG_INFO);
	// Config::SetDefault("ns3::TcpL4Protocol::SocketType", ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("2200"));
	/*
     *   Srs Periodicity
     *          m_nUes < 25 = 40
     *          m_nUes < 60 = 80
     *          m_nUes < 120 = 160
     *          m_nUes >= 120 = 320
     * 
     *   http://www.nsnam.org/doxygen-release/test-lte-rrc_8cc_source.html
     */
    Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue(160));
	Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("2200"));
	Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue (phyMode)); 
	
	// To Debug
	LogComponentEnable("TcpL4Protocol", LOG_LEVEL_ALL);
	LogComponentEnable("PacketSink", LOG_LEVEL_ALL);
	LogComponentEnable("OnOffApplication",
			LogLevel(LOG_LEVEL_ALL | LOG_PREFIX_FUNC | LOG_PREFIX_TIME));
	
	//LogComponentEnable("UdpSocketImpl", LOG_DEBUG);
	//RTS = request to send && CTS = clear to send
	
	//Packet::EnablePrinting ();
	//Packet::EnableChecking ();
	//PacketMetadata::Enable ();
	
	// Infromation printing 
	cout << cGreen;
	cout << "Creating: \n\t\t" << this->nUeNode << 
			"User Equipments.\n\t\t" << this->nEnbNode <<
			"Evolved Node B.\n\t\t" << this->nApNode <<
			"Access Point. \n\t\t" << this->nHaNode <<
			"Home Agent.\n";
	cout << cNormal;
	
	CreateNodes				();
	SetMobility				();
	CreateDevices			();
	InstallInternetStack	();
	AttachUeNode			();
	EnableTraces 			();	
	
	RunApplications		();
	
	cout << cRed;
	cout << "Starting simulation for " << this->totalTime << " s ..." << endl;
	cout << cNormal;
		
	Simulator::Stop (Seconds(this->totalTime));
	AnimationInterface anim ("cacLteWlan.xml");
        FlowMonitorHelper flowmon;
        Ptr<FlowMonitor> monitor = flowmon.InstallAll();

        Simulator::Run ();

        monitor->CheckForLostPackets ();
        Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
        std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
		std::cout << cYellow;
        for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
        {
  	        Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
      	        std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
      	        std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
      	        std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
        }
		std::cout << cNormal;
        //Criação de arquivo de saída .xml
        monitor->SerializeToXmlFile("cac_lte_wlan_flowmonitor", true, true);
        Simulator::Destroy ();
}

void
	CacLteWlan::Report (std::ostream &)
{
	
}
// Public Methods }


// Private Methods {

/*
 *  Este método ira criar os nós necessários para a simulação.
 *  This method will create all the nodes to the simulation.
 */ 
void
	CacLteWlan::CreateNodes ()
{
	this->lteHelper = CreateObject<LteHelper>(); // LTE
	this->lteHelper->SetSchedulerType("ns3::RrFfMacScheduler");
	// lteHelper->setPathlossModelType("ns3::Cost231PropagationLossModel");
	this->lteHelper->SetAttribute("PathlossModel",
			StringValue("ns3::FriisPropagationLossModel"));       
	this->epcHelper = CreateObject<EpcHelper>(); // EPC
	this->lteHelper->SetEpcHelper(this->epcHelper);
	this->pgwNode = epcHelper->GetPgwNode(); // PGW
	
	this->enbNode.Create(1); // Evolved Node B
	
	this->ueNode.Create(this->nUeNode);	

	this->haNode.Create(1); // Home Agent
	
	this->apNode = CreateObject<Node>(); // Access Point node			
	
}

/*
 *  Este método irá configurar a mobilidade dos nós.
 * 	This method will configure the nodes' mobility.
 */ 
void 
	CacLteWlan::SetMobility ()
{
	MobilityHelper mobilityHelper; 
	
	Ptr<ListPositionAllocator> pgwPosition =
			CreateObject<ListPositionAllocator>();
	pgwPosition->Add(Vector(1768, 1200, 0));
	mobilityHelper.SetPositionAllocator(pgwPosition);
	mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobilityHelper.Install(this->pgwNode);// Packet Data Network Gateway
	
	Ptr<ListPositionAllocator> enblPosition =
			CreateObject<ListPositionAllocator>();
	enblPosition->Add(Vector(1100, 1346, 0));
	mobilityHelper.SetPositionAllocator(enblPosition);
	mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobilityHelper.Install(this->enbNode.Get(0));// Enhanced node B
	
	Ptr<ListPositionAllocator> serverPosition = CreateObject<
			ListPositionAllocator>();
	serverPosition->Add(Vector(1768, 1944, 0));
	mobilityHelper.SetPositionAllocator(serverPosition);
	mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobilityHelper.Install(this->haNode);// HA Node
	
	mobilityHelper.SetPositionAllocator("ns3::RandomDiscPositionAllocator", "X",
			StringValue("1344.0"), "Y", StringValue("1530.0"), "Rho",
			StringValue("ns3::UniformRandomVariable[Min=1|Max=106.066]")); 
	mobilityHelper.SetMobilityModel("ns3::RandomWalk2dMobilityModel", "Mode",
			StringValue("Time"), "Time", StringValue("0.05s"), "Speed",
			StringValue("ns3::ConstantRandomVariable[Constant=25.0]"), "Bounds",
			StringValue("1238|1450|1424|1636"));
	mobilityHelper.Install(this->ueNode);// User Equipment Lte Wlan
	
	Ptr<ListPositionAllocator> accessPointPosition = CreateObject<
			ListPositionAllocator>();
	accessPointPosition->Add(Vector(1344, 1530, 0));
	mobilityHelper.SetPositionAllocator(accessPointPosition);
	mobilityHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobilityHelper.Install(this->apNode);// Access point
}

void
	CacLteWlan::CreateDevices ()
{
	// Lte {
	this->enbDevice.Add(this->lteHelper->InstallEnbDevice(this->enbNode)); // eNB
	this->ueDevice.Add(this->lteHelper->InstallUeDevice(this->ueNode)); // UE
	// Lte }
	
	// Wifi {
	/*
		RxGain (Receive Gain) is used to raise or lower the gain (volume) of signals that come in on the IP interface.
		RTS/CTS (Request to Send / Clear to Send)
		RTS/CTS packet size threshold is 0–2347 octets. Typically, sending RTS/CTS frames does not occur unless the packet 
		size exceeds this threshold. If the packet size that the node wants to transmit is larger than the threshold, the RTS/CTS handshake gets 				triggered. Otherwise, the data frame gets sent immediately.
		The Friis transmission equation is used in telecommunications engineering, and gives the power received by one antenna under idealized 				conditions given another antenna some distance away transmitting a known amount of power.
	*/
	
	WifiHelper wifi = WifiHelper::Default(); 
	wifi.SetStandard (WIFI_PHY_STANDARD_80211g);
	wifi.SetRemoteStationManager ("ns3::ArfWifiManager");
	
	Ssid ssidAp = Ssid("WifiLocal");
		
	//PHY Layer
	//YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
	// This is one parameter that matters when using FixedRssLossModel set it to zero; otherwise, gain will be added
	//wifiPhy.Set ("RxGain", DoubleValue (0) ); 
	//wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO); 
	
	YansWifiChannelHelper wifiChannelHelper = YansWifiChannelHelper::Default();
	YansWifiPhyHelper accessPointPhyHelper = YansWifiPhyHelper::Default();
	accessPointPhyHelper.SetChannel(wifiChannelHelper.Create());		

	//MAC Layer
	NqosWifiMacHelper nqosWifiMacHelper = NqosWifiMacHelper::Default();
	NqosWifiMacHelper accessPointMacHelper = NqosWifiMacHelper::Default();
	nqosWifiMacHelper.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssidAp));
	accessPointMacHelper.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssidAp));
			
	this->ueDevice.Add(wifi.Install (accessPointPhyHelper, nqosWifiMacHelper, this->ueNode));
	this->apDevice.Add(wifi.Install (accessPointPhyHelper, nqosWifiMacHelper, this->apNode));
	
	// Wifi }
	
	// P2P {
	
	PointToPointHelper pppHelper;
	pppHelper.SetDeviceAttribute ("DataRate", DataRateValue(DataRate("1000Mb/s")));
	
	// Server to PGW 
	NetDeviceContainer haToPgw = pppHelper.Install(this->pgwNode, this->haNode.Get(0));
	this->haDevice.Add(haToPgw.Get(1)); // Verificar metodo Install point-to-point-helper.cc.
	// Server to AP 
	NetDeviceContainer haToAp = pppHelper.Install(this->apNode, this->haNode.Get(0));
	this->haDevice.Add(haToAp.Get(1)); // Verificar metodo Install point-to-point-helper.cc.
	this->apDevice.Add(haToAp.Get(0));
	
	// P2P }
	
	// Ips {
	Ipv4AddressHelper ipv4Helper;

	// server and Pgw
	ipv4Helper.SetBase("192.168.1.0", "255.255.255.0"); // setting up the IP base, this simulation needs to be Ipv4 because the NS3 have no support to IPv6 yet.
	Ipv4InterfaceContainer haToPgwInterface = ipv4Helper.Assign(haToPgw); // Creating a simple connection between PGW and the server (HA), this method assign set the indicated IP to the nodes.
	this->haInterface.Add(haToPgwInterface.Get(1));

	// Server and AP
	ipv4Helper.SetBase("192.168.2.0", "255.255.255.0");
	Ipv4InterfaceContainer haToApInterface = ipv4Helper.Assign(haToAp);
	this->haInterface.Add(haToApInterface.Get(1));
	this->apInterface.Add(haToApInterface.Get(0));

	// Access Point
	ipv4Helper.SetBase("11.0.0.0", "255.0.0.0");
	this->apInterface.Add(ipv4Helper.Assign(this->apDevice));

	// Ue Wifi
	this->ueInterface.Add(ipv4Helper.Assign(this->ueDevice));

	// Ue Lte 
	this->ueInterface.Add(this->epcHelper->AssignUeIpv4Address(this->ueDevice));		
	// Ips }	
	
	// Routing Table
	Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
}

/*
 * Attach the UEs to an eNB. THIs will
 * configure each UE according to the eNB configuration,
 * and create an RRC connection between them.
 */ 
void
	CacLteWlan::AttachUeNode () 
{
	
	lteHelper->Attach(this->ueDevice, this->enbDevice.Get(0));
}

//////////////////////////////////////////
//
//	Enabling components of the LTE
//
//////////////////////////////////////////
void
	CacLteWlan::EnableTraces ()
{
	this->lteHelper->EnableTraces();
}


/*
 *  Este método irá configurar Internet Stack.
 * 	This method will configure the Internet Stack.
 */ 
void
	CacLteWlan::InstallInternetStack ()
{
	InternetStackHelper internetStackHelper;
	internetStackHelper.Install(this->haNode);
	internetStackHelper.Install(this->apNode);
	internetStackHelper.Install(this->ueNode);
}

void
	CacLteWlan::RunApplications ()
{
	// Edit the applications  ************
	//Simulator::Schedule(Seconds(0.0), &ChegaUsuarioVoz);
      //  Simulator::Schedule(Seconds(0.0), &ChegaUsuarioDados);
}
// Private Methods }


//}

