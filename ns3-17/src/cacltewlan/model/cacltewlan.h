/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef CACLTEWLAN_H
#define CACLTEWLAN_H

#include <string>
#include "app.cc"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/random-variable.h"
#include "ns3/simulator.h"
//#include "ns3/log.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/ptr.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/config-store.h"
#include "ns3/epc-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/ipv4.h"
#include "ns3/wifi-module.h"
#include "ns3/trace-helper.h"
#include "ns3/netanim-module.h"  

using namespace ns3;
using namespace std;

class CacLteWlan {
		
	public:
		CacLteWlan		();
		bool Configure 	(int argc, char **argv);
		void Run 		();
		void Report		(std::ostream & os);
		
	private:
	
		// Args
		string								csvFilename;
		uint16_t							nUeNode;
		uint16_t							nEnbNode;
		uint16_t							nHaNode;
		uint16_t							nApNode;
		// http://en.wikipedia.org/wiki/Byzantine_fault_tolerance
		uint16_t 							f;
		bool								pcap;
		bool								wifi;
		//bool								mobile;
		//bool								grid;
		bool								drop;
		double								totalTime;
		uint16_t 							qtApplication;		
		
		// Constants
		string 								phyMode;
		double								nodeSpeed; // in ms
		double								nodePause; // in s
		//uint16_t							widthScenario;
		//uint16_t							heightScenario;
		
		// Reports
		uint32_t							MacTxDropCount;
		uint32_t							PhyTxDropCount;
		uint32_t							PhyRxDropCount;
		
		// Auxiliar vars for Config
		Ptr<LteHelper> 						lteHelper;
		Ptr<EpcHelper> 						epcHelper;
		Ptr<Node> 							pgwNode;
		NodeContainer						ueNode;
		NodeContainer						enbNode;
		Ptr<Node>							apNode;
		NodeContainer						haNode;
		NetDeviceContainer					ueDevice;
		NetDeviceContainer 					enbDevice;
		NetDeviceContainer					apDevice;
		NetDeviceContainer					haDevice;
		Ipv4InterfaceContainer				ueInterface;
		//Ipv4InterfaceContainer				enbInterface;
		Ipv4InterfaceContainer				apInterface;
		Ipv4InterfaceContainer				haInterface;
		
		// Traffic
		App 								*aplicacao;
				
	private:
		
		// Flowmonitor prints
		void PhyRxDrop 					(Ptr<const Packet> p);
		void PhyTxDrop					(Ptr<const Packet> p);
		void MacTxDrop					(Ptr<const Packet> p);
		void PrintDrop					(Ptr<const Packet> p);
		
		void CreateNodes 				();
		void SetMobility 				();
		void CreateDevices 				();
		void InstallInternetStack 		();
		void RunApplications			();
		void AttachUeNode 				();
		void EnableTraces 				();

};


#endif /* CACLTEWLAN_H */

